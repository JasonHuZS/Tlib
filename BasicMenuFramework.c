#include "BasicMenuFramework.h"
#include <stdarg.h>
#include <stddef.h>

void BMenuFStart(){
  (*BMenuFroot->response)();
}

void BMenuFSetRoot(BasicMenuFrame * frame){
  BMenuFroot=frame;
}

void BMenuFSetResponse(BasicMenuFrame * frame,int (*resfunc)()){
  frame->response=resfunc;
}

void BMenuFFrameInit(BasicMenuFrame * frame,int (*resfunc)()){
  frame->response=resfunc;
  frame->parent=NULL;
  frame->totalSubNum=0;
  frame->subFrame[0]=NULL;
  frame->subFrame[1]=NULL;
  frame->subFrame[2]=NULL;
  frame->subFrame[3]=NULL;
}

void BMenuFAppendChild(BasicMenuFrame * par,BasicMenuFrame * child){
  uint num=par->totalSubNum;
  if (num>=4)
    systemErrorHandler(OUT_OF_BOUND);
  par->subFrame[num++]=child;
  child->parent=par;
  par->totalSubNum=num;
}

int BmenuFEjectChild(BasicMenuFrame *frame){
  if (frame->totalSubNum==0)
    return 0;
  frame->subFrame[frame->totalSubNum-1]->parent=NULL;
  frame->subFrame[frame->totalSubNum--]=NULL;
  return 1;
}

int BMenuFInsertChild(BasicMenuFrame * par,uint index,BasicMenuFrame * child){
  if (index>=4)
    return 0;
  par->subFrame[index]->parent=NULL;
  par->subFrame[index]=child;
  child->parent=par;
  return 1;
}

int BMenuFDeleteChild(BasicMenuFrame * par,uint index,BasicMenuFrame * child){
  uint i,l;
  l=par->totalSubNum;
  if (index>=l)
    return 0;
  par->subFrame[index]=NULL;
  child->parent=NULL;
  for (i=index+1;i<l;i++)
    par->subFrame[i-1]=par->subFrame[i];
  par->totalSubNum=l-1;
  return 1;
}

void BMenuFFrameInitGroup(uint frameNum,...){
  uint i;
  BasicMenuFrame * frame;
  int (*resfunc)();
  //volatile void *junk;
  va_list argp;
  va_start( argp, frameNum );
  for (i=0;i<frameNum;i++){
    frame=va_arg(argp,BasicMenuFrame *);
    resfunc=(int (*)())(va_arg(argp,void *));
    va_arg(argp,void *);
    BMenuFFrameInit(frame,resfunc);
  }
  va_end(argp);
}

int BMenuFAssociate(BasicMenuFrame * par,uint childNum,...){
  uint i;
  BasicMenuFrame * para;
  va_list argp;
  va_start( argp, childNum );
  for (i=0;i<4&&i<childNum;i++){
    para=va_arg(argp,BasicMenuFrame *);
    par->subFrame[i]=para;
    para->parent=par;
  }
  va_end(argp);
  par->totalSubNum=i;
  return i;
}
