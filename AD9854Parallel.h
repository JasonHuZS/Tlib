#ifndef _AD9854Parallel_H_
#define _AD9854Parallel_H_
#include "DDS.h"
#include "AD9854.h"

/*
 * this is a head file of AD9854 in parallel transition mode.
 * use following precompilation selection for use defined port.
 *     define __Alternative_Parallel_AD9854_
 *
 * default port usage:
 * D0~7 <- P5.0~7
 * A0~5 <- P3.0~5<- P3.6,P3.7 are useless but occupied too.
 * control port  <- P2.0~7
 * SHAPED_KEYING <- P2.0
 * FSK/BPSK/HOLD <- P2.1
 * NC            <- P2.2
 * -RD/-CS       <- P2.3
 * -WR/SCLK      <- P2.4  <- this port should exchange with A2 when using SPI mode
 * IO_UDCLK      <- P2.5
 * MASTER_RESET  <- P2.6
 * S/P_SELECT    <- P2.7
 */

/*******************PORT DEFINITIONS AND MANAGEMENT***********************/
#ifndef __Alternative_Parallel_AD9854_
//port definitions
#define AD9854_DP                       P5
#define AD9854_AP                       P3
#define AD9854_CP                       P2
//control port bit definitions
#define AD9854_SHPK                     (BIT0)
#define AD9854_FBH                      (BIT1)
#define AD9854_RD                       (BIT3)
#define AD9854_WR                       (BIT4)
#define AD9854_UDCLK                    (BIT5)
#define AD9854_RST                      (BIT6)
#define AD9854_SP                       (BIT7)
#else
#ifndef AD9854_DP
#error "please define 'AD9854_DP' explicitly!"
#endif
#ifndef AD9854_AP
#error "please define 'AD9854_AP' explicitly!"
#endif
#ifndef AD9854_CP
#error "please define 'AD9854_CP' explicitly!"
#endif
#ifndef AD9854_SHPK
#error "please define 'AD9854_SHPK' explicitly!"
#endif
#ifndef AD9854_FBH
#error "please define 'AD9854_FBH' explicitly!"
#endif
#ifndef AD9854_RD
#error "please define 'AD9854_RD' explicitly!"
#endif
#ifndef AD9854_WR
#error "please define 'AD9854_WR' explicitly!"
#endif
#ifndef AD9854_UDCLK
#error "please define 'AD9854_UDCLK' explicitly!"
#endif
#ifndef AD9854_RST
#error "please define 'AD9854_RST' explicitly!"
#endif
#ifndef AD9854_SP
#error "please define 'AD9854_SP' explicitly!"
#endif

#endif

#define AD9854_JOIN(port,func)          (port##func)

#define AD9854_DATA_DIR                 AD9854_JOIN(AD9854_DP,DIR)
#define AD9854_DATA_IN                  AD9854_JOIN(AD9854_DP,IN)
#define AD9854_DATA_OUT                 AD9854_JOIN(AD9854_DP,OUT)
#define AD9854_DATA_SEL                 AD9854_JOIN(AD9854_DP,SEL)

#define AD9854_ADDR_DIR                 AD9854_JOIN(AD9854_AP,DIR)
#define AD9854_ADDR_IN                  AD9854_JOIN(AD9854_AP,IN)
#define AD9854_ADDR_OUT                 AD9854_JOIN(AD9854_AP,OUT)
#define AD9854_ADDR_SEL                 AD9854_JOIN(AD9854_AP,SEL)

#define AD9854_CTRL_DIR                 AD9854_JOIN(AD9854_CP,DIR)
#define AD9854_CTRL_IN                  AD9854_JOIN(AD9854_CP,IN)
#define AD9854_CTRL_OUT                 AD9854_JOIN(AD9854_CP,OUT)
#define AD9854_CTRL_SEL                 AD9854_JOIN(AD9854_CP,SEL)

#undef AD9854_JOIN

/*************************REGISTER DEFINITIONS****************************/
#define AD9854_PAR1_H                   (0x00)     //Phase Adjust Register#1<13:8>
#define AD9854_PAR1_L                   (0X01)     //Phase Adjust Register#1<7:0>
#define AD9854_PAR2_H                   (0X02)     //Phase Adjust Register#2<13:8>
#define AD9854_PAR2_L                   (0X03)     //Phase Adjust Register#2<7:0>
#define AD9854_FTW1_6                   (0X04)     //Frequency Tuning Word1<47:40>
#define AD9854_FTW1_5                   (0X05)     //Frequency Tuning Word1<39:32>
#define AD9854_FTW1_4                   (0X06)     //Frequency Tuning Word1<31:24>
#define AD9854_FTW1_3                   (0X07)     //Frequency Tuning Word1<23:16>
#define AD9854_FTW1_2                   (0X08)     //Frequency Tuning Word1<15:8>
#define AD9854_FTW1_1                   (0X09)     //Frequency Tuning Word1<7:0>
#define AD9854_FTW2_6                   (0X0A)     //Frequency Tuning Word2<47:40>
#define AD9854_FTW2_5                   (0X0B)     //Frequency Tuning Word2<39:32>
#define AD9854_FTW2_4                   (0X0C)     //Frequency Tuning Word2<31:24>
#define AD9854_FTW2_3                   (0X0D)     //Frequency Tuning Word2<23:16>
#define AD9854_FTW2_2                   (0X0E)     //Frequency Tuning Word2<15:8>
#define AD9854_FTW2_1                   (0X0F)     //Frequency Tuning Word2<7:0>
#define AD9854_DFW_6                    (0X10)     //Delta Frequency Word<47:40>
#define AD9854_DFW_5                    (0X11)     //Delta Frequency Word<39:32>
#define AD9854_DFW_4                    (0X12)     //Delta Frequency Word<31:24>
#define AD9854_DFW_3                    (0X13)     //Delta Frequency Word<23:16>
#define AD9854_DFW_2                    (0X14)     //Delta Frequency Word<15:8>
#define AD9854_DFW_1                    (0X15)     //Delta Frequency Word<7:0>
#define AD9854_UDCLK_4                  (0X16)     //Update Clock<31:24>
#define AD9854_UDCLK_3                  (0X17)     //Update Clock<23:16>
#define AD9854_UDCLK_2                  (0X18)     //Update Clock<15:8>
#define AD9854_UDCLK_1                  (0X19)     //Update Clock<7:0>
#define AD9854_RRC_3                    (0X1A)     //Ramp Rate Clock<19:16>
#define AD9854_RRC_2                    (0X1B)     //Ramp Rate Clock<15:8>
#define AD9854_RRC_1                    (0X1C)     //Ramp Rate Clock<7:0>
#define AD9854_PD                       (0X1D)     //Power Down:DC,DC,DC,Comp PD,0,QDAC PD,DAC PD,DIG PD
#define AD9854_REFCLK                   (0X1E)     //REFCLK:DC,PLL Range,BypassPLL,RM4,RM3,RM2,RM1,RM0
#define AD9854_MOD                      (0X1F)     //MOD:CLR ACC1,CLR ACC2,Triangle,SRC QDAC,M2,M1,M0,Int Update Clk
#define AD9854_OSK_S                    (0X20)     //OSK&SERIAL COM:DC,Bypass Inv Sinc,OSK EN,OSK INT,DC,DC,LSB First,SDO Active
#define AD9854_OSKI_2                   (0X21)     //Output Shape Key I Mult<11:8>
#define AD9854_OSKI_1                   (0X22)     //Output Shape Key I Mult<7:0>
#define AD9854_OSKQ_2                   (0X23)     //Output Shape Key Q Mult<11:8>
#define AD9854_OSKQ_1                   (0X24)     //Output Shape Key Q Mult<7:0>
#define AD9854_OSKRR                    (0X25)     //Output Shape Key Ramp Rate<7:0>
#define AD9854_QDAC_2                   (0X26)     //QDAC<11:8>
#define AD9854_QDAC_1                   (0X27)     //QDAC<7:0>

/**************************FUNCION DEFINITIONS****************************/
/*
 * NOTICE:
 * initialization function.
 * call it before doing anything.
 */
void AD9854Init();

void AD9854SendByte(uchar regAddr,uchar data);
uchar AD9854ReadByte(uchar regAddr);

#endif

//                            Ver. 0.05, all untested
//                            Hu Zhongsheng implemented, last update in 2013/9/3