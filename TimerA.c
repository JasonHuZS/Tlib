#include "TimerA.h"
#include <stdarg.h>
#include <stddef.h>

static uint TimerAmodeControl;

void TimerAStart(){
  SET_BIT(TACTL,TimerAmodeControl);
}

void TimerAPause(){
  CLR_BIT(TACTL,TimerAmodeControl);
  ASN_BIT(TAR,0);
}

void TimerACompareInit(TimerA_ModeCtrl mcbit,uint tacctl_num,...){
  uint i;
  uint taccr,ombit;
  unsigned short volatile* reg[3]={&TACCR0,&TACCR1,&TACCR2};
  unsigned short volatile* ctlreg[3]={&TACCTL0,&TACCTL1,&TACCTL2};
  va_list argp;
  va_start(argp,tacctl_num);
  TimerAmodeControl=mcbit;
  TIMERA_TACLR;
  TACTL=TASSEL_2;
  for (i=0;i<tacctl_num&&i<3;i++){
    taccr=va_arg(argp,uint);
    ombit=va_arg(argp,TimerA_OutputM);
    ASN_BIT(*reg[i],taccr);
    ASN_BIT(*ctlreg[i],ombit);
  }
  va_end(argp);
  #ifndef __Alternative_TimerA_0_
  TIMERA_RESET_HANDLER0;
  #endif
  #ifndef __Alternative_TimerA_1_
  TIMERA_RESET_CC1_HANDLER;
  TIMERA_RESET_CC2_HANDLER;
  TIMERA_RESET_TA_HANDLER;
  #endif
}

void TimerACaptureInit(TimerA_ModeCtrl mcbit,uint tacctl_num,...){
  uint i;
  uint taccr,cmbit;
  unsigned short volatile* reg[3]={&TACCR0,&TACCR1,&TACCR2};
  unsigned short volatile* ctlreg[3]={&TACCTL0,&TACCTL1,&TACCTL2};
  va_list argp;
  va_start(argp,tacctl_num);
  TimerAmodeControl=mcbit;
  TIMERA_TACLR;
  TACTL=TASSEL_2;
  for (i=0;i<tacctl_num&&i<3;i++){
    taccr=va_arg(argp,uint);
    cmbit=va_arg(argp,TimerA_CaptureM);
    ASN_BIT(*reg[i],taccr);
    ASN_BIT(*ctlreg[i],CAP+SCS+cmbit);
  }
  va_end(argp);
  #ifndef __Alternative_TimerA_0_
  TIMERA_RESET_HANDLER0;
  #endif
  #ifndef __Alternative_TimerA_1_
  TIMERA_RESET_CC1_HANDLER;
  TIMERA_RESET_CC2_HANDLER;
  TIMERA_RESET_TA_HANDLER;
  #endif
}

#ifndef __Alternative_TimerA_0_      //0xFFF2 Timer A CC0
#pragma vector=TIMERA0_VECTOR
__interrupt void TimerA0ISR(){
  _DINT();
  uchar res=0;
  res=(*TimerAisr0Handler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif

#ifndef __Alternative_TimerA_1_      //0xFFF0 Timer A CC1-2, TA
#pragma vector=TIMERA1_VECTOR
__interrupt void TimerA1ISR(){
  _DINT();
  uchar res=0;
  uint tatmp;
  while(tatmp=TAIV&0x000E){
    if (tatmp==TIMERA_TAIV_1IFG)
      res+=(*TimerACC1Handler)();
    else if (tatmp==TIMERA_TAIV_2IFG)
      res+=(*TimerACC2Handler)();
    else if (tatmp==TIMERA_TAIV_TAIFG)
      res+=(*TimerATAHandler)();
}
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif
