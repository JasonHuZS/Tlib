#ifndef _IterativeFFT_H_
#define _IterativeFFT_H_
#include "Complex.h"

/*
 * this is a head file of iterative fft implementation.
 * IFFTCal() only work for signals with exactly 128 samples.
 */

void IFFTCal(Complex rtnarr[128],int inpsig[128]);

#include "IterativeFFT.c"

#endif

//                            Ver. 0.8, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/18
