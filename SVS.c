#include "SVS.h"

void SVSInit(SVS_VLD vld, boolean poron, boolean svsout){
  register uchar outbit;
  CLR_BIT(P6DIR,BIT7);
  SET_BIT(P6SEL,BIT7);
  if (SVSCTL&0xf0 == 0)		//if the previos VLDx == 0
    SVSCTL = vld + (poron<<3);	//write SVSCTL immediately
  else{					//else
  while(!SVSCTL&SVSON) _NOP();	//wait until SVSON = 1
    SVSCTL = 0x0;			//clear VLDx
    SVSCTL = vld + (poron<<3);
  }
  outbit=0x80&(svsout<<7);                    //P5.7 as the SVSOUT
  P5DIR |= outbit;
  P5SEL |= outbit;
  while(!SVSCTL&SVSON)	//wait until SVSON = 1
    _NOP();
  return; //return the status of SVSinit, SVSON
}
