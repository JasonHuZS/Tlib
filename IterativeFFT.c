#include "FFT.h"
#include "IterativeFFT.h"

static const Complex IFFTomega[10]={{-1.000000,-0.000000}, {0.000000,-1.000000}, 
    {0.707107,-0.707107}, {0.923880,-0.382683}, {0.980785,-0.195090}, 
    {0.995185,-0.098017}, {0.998795,-0.049068}, {0.999699,-0.024541}, 
    {0.999925,-0.012272}, {0.999981,-0.006136}};

static unsigned char IFFTRevBit(unsigned char i){
  unsigned char r=0;
  r|=((i&0x1)<<6);
  r|=((i&0x2)<<4);
  r|=((i&0x4)<<2);
  r|=(i&0x8);
  r|=((i&0x10)>>2);
  r|=((i&0x20)>>4);
  r|=((i&0x40)>>6);
  return r;
}

static void IFFTBitReverse(Complex *rtnarr,int * inpsig){
  unsigned char i;
  unsigned char revi;
  for (i=0;i<128;i++){
    revi=IFFTRevBit(i);
    rtnarr[revi].real=(float)inpsig[i];
    rtnarr[i].imag=0.0;
  }
}

void IFFTCal(Complex rtnarr[],int inpsig[]){
  Complex o;
  Complex tmp1,tmp2;
  int out,mid,in,m,j,step,itv;
  IFFTBitReverse(rtnarr,inpsig);
  for(out=0;out<7;out++){
    m=1<<out;
    j=128>>out;
    COMPLEX_SET(o,1.0000,0.0000);
    step=m<<1;
    for(mid=0;mid<m;mid++){
      itv=0;
      for(in=0;in<j;in++){
        COMPLEX_MUL(tmp1,o,rtnarr[itv+mid+m]);
        tmp2=rtnarr[itv+mid];
        COMPLEX_ADD(rtnarr[itv+mid],tmp2,tmp1);
        COMPLEX_SUB(rtnarr[itv+mid+m],tmp2,tmp1);
        itv+=step;
      }
      o=ComplexMul(o,IFFTomega[out]);
    }
  }
}
