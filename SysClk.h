#ifndef _SysClk_H_
#define _SysClk_H_
#include "General.h"

/*
 *this is head file for basic clock configuration
 *this head file provides clock settings as below:
 *	1MHz/8MHz/12MHz/16MHz DCO as MCLK (can be divided by 1/2/4/8)
 *      Max DCO
 *	8MHz XT2 as MCLK (can be divided by 1/2/4/8)
 *	DCO as SCLK (can be divided by 1/2/4/8)
 *	XT2 as SCLK (can be divided by 1/2/4/8)
 *DO NOT set the same clock more than once in the program
 */

#define SYSCLK_XT2_FREQ				(24000)
//KHZ. change it if your external crystal is not 8MHZ

//declare it before using XT2 crystal
#define SYSCLK_XT2_INIT				SysClkInitXT2()

//Y is Clk Division. it should be and only be 1,2,4,8
#define SYSCLK_MCLK_DCO_1MHZ(Y)		SysClkDCOConfig(1);  SysClkMClkConfig('D',(Y))
#define SYSCLK_MCLK_DCO_8MHZ(Y)		SysClkDCOConfig(8);  SysClkMClkConfig('D',(Y))
#define SYSCLK_MCLK_DCO_12MHZ(Y)	SysClkDCOConfig(12); SysClkMClkConfig('D',(Y))
#define SYSCLK_MCLK_DCO_16MHZ(Y)	SysClkDCOConfig(16); SysClkMClkConfig('D',(Y))
#define SYSCLK_MCLK_DCO_MAX(Y)		SysClkMaxDCO(); SysClkMClkConfig('D',(Y))
#define SYSCLK_MCLK_XT2(Y)			SysClkMClkConfig('X',(Y))
#define SYSCLK_SCLK_DCO(Y)			SysClkSClkConfig('D',(Y))
#define SYSCLK_SCLK_XT2(Y)			SysClkSClkConfig('X',(Y))

uint SysClkfreq=1000; //KHZ,  MCLK frequency.
uint SysClkfreqS=1000;//KHZ, SMCLK frequency.

/*
 * call this function before use external osc.
 */
void SysClkInitXT2();
void SysClkSysClkDCOConfig (uchar freq);
void SysClkSysClkMClkConfig (uchar source, uchar div);
void SysClkSysClkSClkConfig (uchar source, uchar div);
void SysClkMaxDCO();

#include "SysClk.c"

#endif

//					Ver 0.4
//					Yongchen Jiang coded, 1st Aug 2013
