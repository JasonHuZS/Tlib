#include "Complex.h"

Complex ComplexNew(){                           // DON'T use prosedure like this
  Complex r;                                    // unless there are special purposes
  r.real=r.imag=0;
  return r;
}

Complex ComplexGen(float realin,float imagin){  // SIMILAR
  Complex r;
  r.real=realin;
  r.imag=imagin;
  return r;
}

Complex ComplexMul(Complex a,Complex b){        // r=a*b
  Complex r;
  r.real=a.real*b.real-a.imag*b.imag;
  r.imag=a.real*b.imag+a.imag*b.real;
  return r;
}

Complex ComplexAdd(Complex a,Complex b){        // r=a+b
  Complex r;
  r.real=a.real+b.real;
  r.imag=a.imag+b.imag;
  return r;
}

Complex ComplexSub(Complex a,Complex b){        // r=a-b
  Complex r;
  r.real=a.real-b.real;
  r.imag=a.imag-b.imag;
  return r;
}

Complex ComplexDiv(Complex a,Complex b){        // r=a/b
  Complex r;
  float denom;
  denom=b.real*b.real+b.imag*b.imag;
  r.real=(a.real*b.real+a.imag*b.imag)/denom;
  r.imag=(a.real*b.imag-a.imag*b.real)/denom;
  return r;
}

Complex ComplexConjugate(Complex a){
  Complex r;
  r.real=a.real;
  r.imag=-a.imag;
  return r;
}

float ComplexSquare(Complex a){
  float r;
  r=a.real*a.real+a.imag*a.imag;
  return r;
}