#include "General.h"
#include "msp430.h"
#include "SysClk.h"

extern uint SysClkforDelay;
void delay(uint count){
  volatile uint i;
  SysClkforDelay|=(SysClkforDelay==0);
  uint l=count*SysClkforDelay;
  for(i=0;i<l;i++);
}

void NestedDelay(uint count,uint times){
  volatile uint i;
  for(i=0;i<times;i++)
    delay(count);
}

void blank(){}

uchar ucblank(){
  return 0;
}

uchar ucblank1(){
  return 1;
}

uchar bit2index(uchar bit){
  if (bit&BIT0)
    return 0;
  else if (bit&BIT1)
    return 1;
  else if (bit&BIT2)
    return 2;
  else if (bit&BIT3)
    return 3;
  else if (bit&BIT4)
    return 4;
  else if (bit&BIT5)
    return 5;
  else if (bit&BIT6)
    return 6;
  else if (bit&BIT7)
    return 7;
  return 8;
}

uchar index2bit(uchar index){
  switch(index){
  case 0: return BIT0;
  case 1: return BIT1;
  case 2: return BIT2;
  case 3: return BIT3;
  case 4: return BIT4;
  case 5: return BIT5;
  case 6: return BIT6;
  case 7: return BIT7;
  default:return 0xff;
  }
}

void systemErrorHandler(uchar errorCode){
  //need further negotiation
  boolean out=btrue;
  for (;out;);
}

void Ref2_5V(void){
  SET_BIT(ADC12CTL0,0x0060);
  DELAY_20MS;
}

void Ref1_5V(void ){
  SET_BIT(ADC12CTL0,0x0020);
  DELAY_20MS;
}
