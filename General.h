#ifndef _General_H_
#define _General_H_

/*
 * this is a General head file
 * this head file provides some functions that people likely
 *   tend to use.
 * Notice:
 * functions in this head file all base on osc. with freq. of 1MHZ.
 * make sure that and don't make trivial mistakes.
 * the depth of pipeline of CPU in MSP430 is 6.
 */

/**************************BIT OPERATION MACROS**************************/
#define SET_BIT(reg,bits)   ((reg)|=(bits))
#define CLR_BIT(reg,bits)   ((reg)&=~(bits))
#define INV_BIT(reg,bits)   ((reg)^=(bits))
#define ASN_BIT(reg,bits)   ((reg)=(bits))

/***************************CALCULATION MACROS***************************/
#define MAX(a,b)            ((a)>(b)?(a):(b))
#define MIN(a,b)            ((a)>(b)?(b):(a))

/***************************BUSY TIMING MACROS***************************/
#define DELAY_1US           _NOP()
#define DELAY_1MS           delay(107)
#define DELAY_5MS           NestedDelay(39,13)
#define DELAY_10MS          NestedDelay(39,26)
#define DELAY_20MS          NestedDelay(39,52)
#define DELAY_50MS          NestedDelay(39,130)
#define DELAY_80MS          NestedDelay(39,208)
#define DELAY_100MS         NestedDelay(39,260)
#define DELAY_200MS         NestedDelay(39,520)
#define DELAY_500MS         NestedDelay(39,1300)
#define DELAY_800MS         NestedDelay(39,2080)
#define DELAY_1S            NestedDelay(39,2600)
//#define DELAY_1S          NestedDelay(1070,52)

/*************************OTHER MACRO DEFINTIONS*************************/
#ifdef NULL
#undef NULL
#endif
#define NULL                (0x0000)
#define FUNC_NULL           (blank)
#define UCFUNC_NULL         (ucblank)

//Reference Voltage Setup, NEED EXTRA DELAY!!!!!!!
//REMEMBER TO SET AT LEAST 17MS DELAY RIGHT AFTER USING IT!!!!
#define SET_REF_2_5V        SET_BIT(ADC12CTL0,0x0060)
#define SET_REF_1_5V        SET_BIT(ADC12CTL0,0x0020)

/*************************EXCEPTION INDEX MACROS*************************/
#define OUT_OF_BOUND        (1)
#define OUT_OF_HEAP         (2)

/***********************CONVENIENT TYPE DEFINITIONS***********************/
enum Boolean_t {bfalse=0,btrue=1};
typedef enum Boolean_t boolean;

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

/*******************GENERAL FUNCTION DEFINITIONS*************************/
//Time Related
void delay(uint count);
void NestedDelay(uint count,uint times);

//Null functions
void blank();
uchar ucblank();
uchar ucblank1();

//Bithack
uchar bit2index(uchar bit);
uchar index2bit(uchar index);

//Internal Reference Initialization
void Ref2_5V();
void Ref1_5V();

//Exception Machanism
void systemErrorHandler(uchar errorCode);

#include "General.c"

#endif

//                            Ver. 0.78, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/30
