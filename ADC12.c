
#include "ADC12.h"
#include "General.h"
/*
 * default: 
 * sample-and-hold time: 4 ADC12CLK cycles
 * the sampe-input signal is not inverted
 * the ADC12 clock divider: /1
 * the pots are used as blow:
 * P6.0~P6.7 -> channel0~channel7
 */


/*
 * this is the function to select reference, all the channels select the same reference
 */
void ADC12SelectReference(uint cea,uint sref)
{
  switch(cea)
  {
  case ADC12_CHANNEL0_END:
    {
      ADC12_MCTL0_ASN((sref)+(cea));
      P6SEL |= 0x01;
      break;
    }
  case ADC12_CHANNEL1_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+(cea));
      P6SEL |= 0x03;
      break;
    }
  case ADC12_CHANNEL2_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+(cea));
      P6SEL |= 0x07;
      break;
    }
  case ADC12_CHANNEL3_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+(cea));
      P6SEL |= 0x0f;
      break;
    }
  case ADC12_CHANNEL4_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+(cea));
      P6SEL |= 0x1f;
      break;
    }
  case ADC12_CHANNEL5_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+(cea));
      P6SEL |= 0x3f;
      break;
    }
  case ADC12_CHANNEL6_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+(cea));
      P6SEL |= 0x7f;
      break;
    }
    case ADC12_CHANNEL7_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL8_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL9_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL10_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL11_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+0x0a);
      ADC12_MCTL11_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL12_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+0x0a);
      ADC12_MCTL11_ASN((sref)+0x0b);     
      ADC12_MCTL12_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
  case ADC12_CHANNEL13_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+0x0a);
      ADC12_MCTL11_ASN((sref)+0x0b);     
      ADC12_MCTL12_ASN((sref)+0x0c);
      ADC12_MCTL13_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL14_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+0x0a);
      ADC12_MCTL11_ASN((sref)+0x0b);     
      ADC12_MCTL12_ASN((sref)+0x0c);
      ADC12_MCTL13_ASN((sref)+0x0d);
      ADC12_MCTL14_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
    case ADC12_CHANNEL15_END:
    {
      ADC12_MCTL0_ASN((sref)+0x00);
      ADC12_MCTL1_ASN((sref)+0x01);
      ADC12_MCTL2_ASN((sref)+0x02);
      ADC12_MCTL3_ASN((sref)+0x03);
      ADC12_MCTL4_ASN((sref)+0x04);
      ADC12_MCTL5_ASN((sref)+0x05);
      ADC12_MCTL6_ASN((sref)+0x06);
      ADC12_MCTL7_ASN((sref)+0x07);
      ADC12_MCTL8_ASN((sref)+0x08);
      ADC12_MCTL9_ASN((sref)+0x09);
      ADC12_MCTL10_ASN((sref)+0x0a);
      ADC12_MCTL11_ASN((sref)+0x0b);     
      ADC12_MCTL12_ASN((sref)+0x0c);
      ADC12_MCTL13_ASN((sref)+0x0d);
      ADC12_MCTL14_ASN((sref)+(0x0e));
      ADC12_MCTL15_ASN((sref)+(cea));
      P6SEL |= 0xff;
      break;
    }
  }
}

  
/*
 * this is the function of the initialization of the singal-channel and singal-conversion mode  
 * csa  -> conversion start address
 * ssel -> ADC12 clock source
 * shp  -> sample-and-hold pulse-mode
 * shs  ->sample-and-hold source
 * sref -> select reference, all the channels select the same reference
 */
void ADC12SingleChannelAndSingleConversion(uint csa,uint ssel,uint shp,uint shs,uint sref)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN(0x0010+ref);
  ADC12_CTL1_ASN((csa)+(ssel)+(shp)+(shs));
  switch(csa)
  {
  case ADC12_CHANNEL0_START: { P6DIR&=~0x01;P6SEL |= 0x01;ADC12_MCTL0_ASN((sref)+0x0000);break;}
  case ADC12_CHANNEL1_START: { P6DIR&=~0x02;P6SEL |= 0x02;ADC12_MCTL1_ASN((sref)+0x0001);break;}
  case ADC12_CHANNEL2_START: { P6DIR&=~0x04;P6SEL |= 0x04;ADC12_MCTL2_ASN((sref)+0x0002);break;}
  case ADC12_CHANNEL3_START: { P6DIR&=~0x08;P6SEL |= 0x08;ADC12_MCTL3_ASN((sref)+0x0003);break;}
  case ADC12_CHANNEL4_START: { P6DIR&=~0x10;P6SEL |= 0x10;ADC12_MCTL4_ASN((sref)+0x0004);break;}
  case ADC12_CHANNEL5_START: { P6DIR&=~0x20;P6SEL |= 0x20;ADC12_MCTL5_ASN((sref)+0x0005);break;}
  case ADC12_CHANNEL6_START: { P6DIR&=~0x40;P6SEL |= 0x40;ADC12_MCTL6_ASN((sref)+0x0006);break;}
  case ADC12_CHANNEL7_START: { P6DIR&=~0x80;P6SEL |= 0x80;ADC12_MCTL7_ASN((sref)+0x0007);break;}
  case ADC12_CHANNEL8_START: { ADC12_MCTL8_ASN((sref)+0x0008);break;}
  case ADC12_CHANNEL9_START: { ADC12_MCTL9_ASN((sref)+0x0009);break;}
  case ADC12_CHANNEL10_START: { ADC12_MCTL10_ASN((sref)+0x000a);break;}
  case ADC12_CHANNEL11_START: { ADC12_MCTL11_ASN((sref)+0x000b);break;}
  case ADC12_CHANNEL12_START: { ADC12_MCTL12_ASN((sref)+0x000c);break;}
  case ADC12_CHANNEL13_START: { ADC12_MCTL11_ASN((sref)+0x000d);break;}
  case ADC12_CHANNEL14_START: { ADC12_MCTL11_ASN((sref)+0x000e);break;}
  case ADC12_CHANNEL15_START: { ADC12_MCTL11_ASN((sref)+0x000f);break;}
  }
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}

void ADC12SCSCSimple(uint csa)
{ 
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN(0x0010+ref);
  ADC12_CTL1_ASN((csa)+ADC12_SSEL_OSC+ADC12_SHP_PULSE+ADC12_SHS_SC);
  //ADC12SelectReference(ADC12_VREF_AVSS);
    switch(csa)
  {
  case ADC12_CHANNEL0_START: { P6DIR&=~0x01;P6SEL |= 0x01;ADC12_MCTL0_ASN((ADC12_VREF_AVSS)+0x0000);break;}
  case ADC12_CHANNEL1_START: { P6DIR&=~0x02;P6SEL |= 0x02;ADC12_MCTL1_ASN((ADC12_VREF_AVSS)+0x0001);break;}
  case ADC12_CHANNEL2_START: { P6DIR&=~0x04;P6SEL |= 0x04;ADC12_MCTL2_ASN((ADC12_VREF_AVSS)+0x0002);break;}
  case ADC12_CHANNEL3_START: { P6DIR&=~0x08;P6SEL |= 0x08;ADC12_MCTL3_ASN((ADC12_VREF_AVSS)+0x0003);break;}
  case ADC12_CHANNEL4_START: { P6DIR&=~0x10;P6SEL |= 0x10;ADC12_MCTL4_ASN((ADC12_VREF_AVSS)+0x0004);break;}
  case ADC12_CHANNEL5_START: { P6DIR&=~0x20;P6SEL |= 0x20;ADC12_MCTL5_ASN((ADC12_VREF_AVSS)+0x0005);break;}
  case ADC12_CHANNEL6_START: { P6DIR&=~0x40;P6SEL |= 0x40;ADC12_MCTL6_ASN((ADC12_VREF_AVSS)+0x0006);break;}
  case ADC12_CHANNEL7_START: { P6DIR&=~0x80;P6SEL |= 0x80;ADC12_MCTL7_ASN((ADC12_VREF_AVSS)+0x0007);break;}
  case ADC12_CHANNEL8_START: { ADC12_MCTL8_ASN((ADC12_VREF_AVSS)+0x0008);break;}
  case ADC12_CHANNEL9_START: { ADC12_MCTL9_ASN((ADC12_VREF_AVSS)+0x0009);break;}
  case ADC12_CHANNEL10_START: { ADC12_MCTL10_ASN((ADC12_VREF_AVSS)+0x000a);break;}
  case ADC12_CHANNEL11_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000b);break;}
  case ADC12_CHANNEL12_START: { ADC12_MCTL12_ASN((ADC12_VREF_AVSS)+0x000c);break;}
  case ADC12_CHANNEL13_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000d);break;}
  case ADC12_CHANNEL14_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000e);break;}
  case ADC12_CHANNEL15_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000f);break;}
  }
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}
  
  
/*
 * this is the function of the initialization of the sequence-of-channels mode 
 * msc  -> multiple sample and conversion
 * csa  -> conversion start address
 * cea  -> conversion end address
 * ssel -> clock source
 * shp  -> sample-and-hold pulse-mode
 * shs  -> sample-and-hold source
 * sref -> select reference, all the channels select the same reference
 */
void ADC12SequenceOfChannels(uint msc,uint csa,uint cea,uint ssel,uint shp,uint shs,uint sref)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN((msc)+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+(ssel)+(shp)+(shs)+0x0002);
  ADC12SelectReference((cea),(sref));
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}

void ADC12SCSimple(uint csa,uint cea)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN(ADC12_MSC_SHI_FIRS+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+ADC12_SSEL_OSC+ADC12_SHP_PULSE+ADC12_SHS_SC+0x0002);
  ADC12SelectReference((cea),ADC12_VREF_AVSS);
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}
/*
 * this is the function of the initialization of the repeat-single-channel mode 
 * msc  -> multiple sample and conversion
 * csa  -> conversion start address
 * ssel -> ADC12 clock source
 * shp  -> sample-and-hold pulse-mode
 * shs  ->sample-and-hold source
 * sref -> select reference, all the channels select the same reference
 */
void ADC12RepeatSingleChannel(uint msc,uint csa,uint ssel,uint shp,uint shs,uint sref)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN((msc)+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+(ssel)+(shp)+(shs)+0x0004);
 // ADC12SelectReference((sref));
    switch(csa)
  {
  case ADC12_CHANNEL0_START: { P6DIR&=~0x01;P6SEL |= 0x01;ADC12_MCTL0_ASN((sref)+0x0000);break;}
  case ADC12_CHANNEL1_START: { P6DIR&=~0x02;P6SEL |= 0x02;ADC12_MCTL1_ASN((sref)+0x0001);break;}
  case ADC12_CHANNEL2_START: { P6DIR&=~0x04;P6SEL |= 0x04;ADC12_MCTL2_ASN((sref)+0x0002);break;}
  case ADC12_CHANNEL3_START: { P6DIR&=~0x08;P6SEL |= 0x08;ADC12_MCTL3_ASN((sref)+0x0003);break;}
  case ADC12_CHANNEL4_START: { P6DIR&=~0x10;P6SEL |= 0x10;ADC12_MCTL4_ASN((sref)+0x0004);break;}
  case ADC12_CHANNEL5_START: { P6DIR&=~0x20;P6SEL |= 0x20;ADC12_MCTL5_ASN((sref)+0x0005);break;}
  case ADC12_CHANNEL6_START: { P6DIR&=~0x40;P6SEL |= 0x40;ADC12_MCTL6_ASN((sref)+0x0006);break;}
  case ADC12_CHANNEL7_START: { P6DIR&=~0x80;P6SEL |= 0x80;ADC12_MCTL7_ASN((sref)+0x0007);break;}
  case ADC12_CHANNEL8_START: { ADC12_MCTL8_ASN((sref)+0x0008);break;}
  case ADC12_CHANNEL9_START: { ADC12_MCTL9_ASN((sref)+0x0009);break;}
  case ADC12_CHANNEL10_START: { ADC12_MCTL10_ASN((sref)+0x000a);break;}
  case ADC12_CHANNEL11_START: { ADC12_MCTL11_ASN((sref)+0x000b);break;}
  case ADC12_CHANNEL12_START: { ADC12_MCTL12_ASN((sref)+0x000c);break;}
  case ADC12_CHANNEL13_START: { ADC12_MCTL11_ASN((sref)+0x000d);break;}
  case ADC12_CHANNEL14_START: { ADC12_MCTL11_ASN((sref)+0x000e);break;}
  case ADC12_CHANNEL15_START: { ADC12_MCTL11_ASN((sref)+0x000f);break;}
  }
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}
void ADC12RSCSimple(uint csa)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN(ADC12_MSC_SHI_FIRS+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+ADC12_SSEL_OSC+ADC12_SHP_PULSE+ADC12_SHS_SC+0x0004);
  //ADC12SelectReference(ADC12_VREF_AVSS);
      switch(csa)
  {
  case ADC12_CHANNEL0_START: { P6DIR&=~0x01;P6SEL |= 0x01;ADC12_MCTL0_ASN((ADC12_VREF_AVSS)+0x0000);break;}
  case ADC12_CHANNEL1_START: { P6DIR&=~0x02;P6SEL |= 0x02;ADC12_MCTL1_ASN((ADC12_VREF_AVSS)+0x0001);break;}
  case ADC12_CHANNEL2_START: { P6DIR&=~0x04;P6SEL |= 0x04;ADC12_MCTL2_ASN((ADC12_VREF_AVSS)+0x0002);break;}
  case ADC12_CHANNEL3_START: { P6DIR&=~0x08;P6SEL |= 0x08;ADC12_MCTL3_ASN((ADC12_VREF_AVSS)+0x0003);break;}
  case ADC12_CHANNEL4_START: { P6DIR&=~0x10;P6SEL |= 0x10;ADC12_MCTL4_ASN((ADC12_VREF_AVSS)+0x0004);break;}
  case ADC12_CHANNEL5_START: { P6DIR&=~0x20;P6SEL |= 0x20;ADC12_MCTL5_ASN((ADC12_VREF_AVSS)+0x0005);break;}
  case ADC12_CHANNEL6_START: { P6DIR&=~0x40;P6SEL |= 0x40;ADC12_MCTL6_ASN((ADC12_VREF_AVSS)+0x0006);break;}
  case ADC12_CHANNEL7_START: { P6DIR&=~0x80;P6SEL |= 0x80;ADC12_MCTL7_ASN((ADC12_VREF_AVSS)+0x0007);break;}
  case ADC12_CHANNEL8_START: { ADC12_MCTL8_ASN((ADC12_VREF_AVSS)+0x0008);break;}
  case ADC12_CHANNEL9_START: { ADC12_MCTL9_ASN((ADC12_VREF_AVSS)+0x0009);break;}
  case ADC12_CHANNEL10_START: { ADC12_MCTL10_ASN((ADC12_VREF_AVSS)+0x000a);break;}
  case ADC12_CHANNEL11_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000b);break;}
  case ADC12_CHANNEL12_START: { ADC12_MCTL12_ASN((ADC12_VREF_AVSS)+0x000c);break;}
  case ADC12_CHANNEL13_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000d);break;}
  case ADC12_CHANNEL14_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000e);break;}
  case ADC12_CHANNEL15_START: { ADC12_MCTL11_ASN((ADC12_VREF_AVSS)+0x000f);break;}
  }
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}

/*
 * this is the function of the initialization of the repeat-sequence-of-channels mode 
 * msc  -> multiple sample and conversion
 * csa  -> conversion start address
 * cea  -> conversion end address
 * ssel -> ADC12 clock source
 * shp  -> sample-and-hold pulse-mode
 * shs  ->sample-and-hold source
 * sref -> select reference, all the channels select the same reference
 */
void ADC12RepeatSequenceOfChannels(uint msc,uint csa,uint cea,uint ssel,uint shp,uint shs,uint sref)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN((msc)+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+(ssel)+(shp)+(shs)+0x0006);
  ADC12SelectReference((cea),(sref));
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}
void ADC12RSSimple(uint csa,uint cea)
{
  uint ref=ADC12CTL0&0x0060;
  ADC12_CTL0_ASN(ADC12_MSC_SHI_FIRS+0x0010+ref); 
  ADC12_CTL1_ASN((csa)+ADC12_SSEL_OSC+ADC12_SHP_PULSE+ADC12_SHS_SC+0x0006);
  ADC12SelectReference((cea),ADC12_VREF_AVSS);
  #ifndef __Alternative_ADC12_
  ADC12_RESET_MO_HANDLER;
  ADC12_RESET_CTO_HANDLER;
  ADC12_RESET_MEM_HANDLER;
  #endif
}

#ifndef __Alternative_ADC12_
#pragma vector=ADC12_VECTOR
__interrupt void ADC12ISR(){
  _DINT();
  uchar res=0;
  switch (ADC12_GET_IV>>1) {
  case 1:
    res=(*ADC12MemOverflowHandler)();break;
  case 2:
    res=(*ADC12CTOHandler)();break;
  default:
    res=(*ADC12MemHandler)();break;
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif
