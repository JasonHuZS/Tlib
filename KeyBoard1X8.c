#include "KeyBoard1X8.h"
#include "stdarg.h"

static uchar KB1X8registeredBit=0;

void KB1X8Init(uchar bitmap){
  int i;
  for (i=0;i<8;i++)
    KB1X8handler[i]=ucblank;
  KB1X8errorHandler=ucblank;
  KB1X8registeredBit=bitmap;
  CLR_BIT(KB1X8_DIR,bitmap);
  CLR_BIT(KB1X8_SEL,bitmap);
  SET_BIT(KB1X8_IE,bitmap);
  SET_BIT(KB1X8_IES,bitmap);
  CLR_BIT(KB1X8_IFG,bitmap);
}

void KB1X8ResetHandler(int i){
  KB1X8handler[i]=ucblank;
}

void KB1X8SetHandler(int i,uchar (*handler)()){
  KB1X8handler[i]=handler;
}

void KB1X8ResetErrorHandler(){
  KB1X8errorHandler=ucblank;
}

void KB1X8SetErrorHandler(uchar (*handler)()){
  KB1X8errorHandler=handler;
}

void KB1X8Lock(uchar bitmap){
  CLR_BIT(KB1X8_IE,bitmap&KB1X8registeredBit);
}

void KB1X8Unlock(uchar bitmap){
  SET_BIT(KB1X8_IE,bitmap&KB1X8registeredBit);
}

void KB1X8SetHandlerGroup(uint startInd,uint handlerNum,...){
  uint i,l;
  uchar (*para)();
  va_list argp;
  va_start( argp, handlerNum );
  l=startInd+handlerNum;
  for (i=startInd;i<l&&i<8;i++){
    para=(uchar (*)())(va_arg(argp,void *));
    (va_arg(argp,void *));
    KB1X8handler[i]=para;
  }
  va_end(argp);
}

#pragma vector= KB1X8_INT_VEC
__interrupt void KB1X8ISR(){
  _DINT();
  uchar res=0,choose,way,finstate;
  DELAY_1MS;
  DELAY_1MS;
  choose=KB1X8_IFG&KB1X8registeredBit;
  way=bit2index(choose);
  switch(way){
  case 0:
    finstate=(~KB1X8_IES)&BIT0;
    if ((KB1X8_IN&BIT0)==finstate)
      res=(*KB1X8handler[0])();
    CLR_BIT(KB1X8_IFG,BIT0);
    break;
  case 1:
    finstate=(~KB1X8_IES)&BIT1;
    if ((KB1X8_IN&BIT1)==finstate)
      res=(*KB1X8handler[1])();
    CLR_BIT(KB1X8_IFG,BIT1);
    break;
  case 2:
    finstate=(~KB1X8_IES)&BIT2;
    if ((KB1X8_IN&BIT2)==finstate)
      res=(*KB1X8handler[2])();
    CLR_BIT(KB1X8_IFG,BIT2);
    break;
  case 3:
    finstate=(~KB1X8_IES)&BIT3;
    if ((KB1X8_IN&BIT3)==finstate)
      res=(*KB1X8handler[3])();
    CLR_BIT(KB1X8_IFG,BIT3);
    break;
  case 4:
    finstate=(~KB1X8_IES)&BIT4;
    if ((KB1X8_IN&BIT4)==finstate)
      res=(*KB1X8handler[4])();
    CLR_BIT(KB1X8_IFG,BIT4);
    break;
  case 5:
    finstate=(~KB1X8_IES)&BIT5;
    if ((KB1X8_IN&BIT5)==finstate)
      res=(*KB1X8handler[5])();
    CLR_BIT(KB1X8_IFG,BIT5);
    break;
  case 6:
    finstate=(~KB1X8_IES)&BIT6;
    if ((KB1X8_IN&BIT6)==finstate)
      res=(*KB1X8handler[6])();
    CLR_BIT(KB1X8_IFG,BIT6);
    break;
  case 7:
    finstate=(~KB1X8_IES)&BIT7;
    if ((KB1X8_IN&BIT7)==finstate)
      res=(*KB1X8handler[7])();
    CLR_BIT(KB1X8_IFG,BIT7);
    break;
  default:
    res=(*KB1X8errorHandler)();
    CLR_BIT(KB1X8_IFG,KB1X8registeredBit);
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
