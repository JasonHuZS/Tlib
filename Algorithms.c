#include "Algorithms.h"

uint SqrtInt(ulong M) {
  uint res,i; 
  ulong tmp, ttp;         // 结果、循环计数 
  if (M == 0)             // 被开方数，开方结果也为0 
   return 0; 
  res = 0;
  tmp = (M >> 30);        // 获取最高位：B[m-1] 
  M <<= 2; 
  if (tmp > 1){           // 最高位为1 
    res ++;                 // 结果当前位为1，否则为默认的0 
    tmp -= res; 
  } 
  for (i=15; i>0; i--){   // 求剩余的15位 
    res <<= 1;              // 左移一位 
    tmp <<= 2; 
    tmp += (M >> 30);     // 假设 
    ttp = res; 
    ttp = (ttp<<1)+1; 
    M <<= 2; 
    if (tmp >= ttp){      // 假设成立
      tmp -= ttp; res ++; 
    }
  }
  return res;
}

float InvSqrt (float x){
  float xhalf = 0.5f*x;
  uint i = *(uint*)&x;
  i = 0x5f3759df - (i >> 1); // 计算第一个近似根
  x = *(float*)&i;
  x = x*(1.5f - xhalf*x*x); // 牛顿迭代法
  return x;
}

float CarmackSqrt(float x){
  union{
    int intPart;
    float floatPart;
  } convertor;
  union{
    int intPart;
    float floatPart;
  } convertor2;
  convertor.floatPart = x;
  convertor2.floatPart = x;
  convertor.intPart = 0x1FBCF800 + (convertor.intPart >> 1);
  convertor2.intPart = 0x5f3759df - (convertor2.intPart >> 1);
  return 0.5f*(convertor.floatPart + (x * convertor2.floatPart));
}

uint UintRoundUp(uint size){
//round up (size) to power of 2
  --size;
  size|=size>>1;
  size|=size>>2;
  size|=size>>4;
  size|=size>>8;
  return ++size;
}

ulong UlongRoundUp(ulong size){
//round up (size) to power of 2
	--size;
	size|=size>>1;
	size|=size>>2;
	size|=size>>4;
	size|=size>>8;
	size|=size>>16;
	return ++size;
}

float SinLowP(int x){
  int i=0;
  long int result = 0 , temp = 0;
  
  temp = (180 - x) * x;
  result = temp;//此处result为sin(x)结果的2^13倍
  
  for( i = 0; temp >>= 1; i++ );  //最高位位数，从 0开始
  
  //下面把result转换成float内存格式
  result = ( result << 23-i ) + ( i+113 << 23 );  //i+113=127-(13-i)-1
  
  return *( (float *) &result) ;       
}

float SinHighP(int x){
  int i=0;
  long int result = 0 , temp = 0;
  
  temp = ( 180 - x ) * x;
  temp = temp * ( 1619 + ( (57 * temp) >>10) );
  result=temp;//此处result为sin(x)结果的2^24倍
  
  for( i = 0; temp >>= 1; i++ );  //最高位位数，从 0开始
  
  //下面把result转换成float内存格式
  result=( result << 23-i )+ ( 102+i << 23 );//102+i=127-(24-i)-1
  return *( (float *) &result );
}

#define abs(x)        ((x)>=0?(x):(-x))
float SinParabola(float x){
  const float B = 4/PI;
  const float C = -4/(PI*PI);
  
  float y = B * x + C * x * abs(x);
  
  const float p = 0.225;
  
  y = p * (y * abs(y) - y) + y;
  
  return y;
}
#undef abs