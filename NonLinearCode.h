#ifndef _NonLinearCode_H_
#define _NonLinearCode_H_

/*
 * this is a head file of nonlinear encoding and decoding.
 * algorithm(s) included:
 *     A-law nonlinear encoding
 */

/*
 * A-law encoding:
 * input:  12-bit long bipolar signal data (using natural coding)
 * output: 8-bit long unipolar encoded signal
 *
 * data converted by ADC12 needs to be preprocessed:
*     code=ALawEncoding(sigIn>0x800?sigIn:(0x800-sigIn));
 */
uchar ALawEncoding(uint sigIn);
/*
 * the result of decoding is half-adjusted.
 */
uint ALawDecoding(uchar sigIn);

#include "NonLinearCode.c"

#endif

//                            Ver. 0.1, all tested
//                            Hu Zhongsheng implemented, last update in 2013/9/6d