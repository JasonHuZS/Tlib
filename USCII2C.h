#ifndef _USCII2C_H_
#define _USCII2C_H_
#include "msp430.h"
#include "USCI.h"
#include "General.h"

/*
 * this is a head file of I2C mode, USCI.
 *
 * first of all, configurate an only address to each device.
 * then, use master or slave initilazation process to register this address.
 * to make two devices to communicate to each other, one must know the addr.
 *     of the other's.
 *
 * Port Usage:
 * if __USE_0_I2C_ defined:
 * P3.1 <- SDA
 * P3.2 <- SCL
 *
 * if __USE_1_I2C_ defined:
 * P5.1 <- SDA
 * P5.2 <- SCL
 *
 * connecting illustration:
 *
 *                            /|\  /|\  +Vcc 
 *                            res  res  10~12kohm
 *                             |    |
 *         -----------------   |    |  -----------------
 *        |              SDA|<-|---+->|SDA              |
 *        |       MCU       |  |      |      Device     |
 *        |                 |  |      |                 |
 *        |              SCL|<-+----->|SCL              |
 *        |                 |         |                 |
 */

/************************ISR HANDLER DEFINITIONS***************************/
/* 
 * NOTICE:
 * the basic data transmitting and recieving functionalities have already implemented.
 * the handler interfaces are for some EXTRA operations.
 * if no, please leave them.
 *
 * unless you have some requirements those need to implement the ISR yourself,
 *     please DO NOT use following pre-compilation selections:
 *     __Alternative_0_I2C_
 *     __Alternative_1_I2C_
 * to destroy the pre-implemented ISR framework.
 * the tested framework will definitely simplify your work.
 */

#ifdef __USE_0_I2C_
#ifndef __Alternative_0_I2C_

uchar (*USCII2C0NackHandler)();
uchar (*USCII2C0StopHandler)();
uchar (*USCII2C0StartHandler)();
uchar (*USCII2C0LostHandler)();
uchar (*USCII2C0TXHandler)();
uchar (*USCII2C0RXHandler)();

#define USCII2C_0_SET_NACK_HANDLER(handler)   USCII2C0NackHandler=(handler)
#define USCII2C_0_RESET_NACK_HANDLER          USCII2C0NackHandler=ucblank
#define USCII2C_0_SET_STOP_HANDLER(handler)   USCII2C0StopHandler=(handler)
#define USCII2C_0_RESET_STOP_HANDLER          USCII2C0StopHandler=ucblank
#define USCII2C_0_SET_START_HANDLER(handler)  USCII2C0StartHandler=(handler)
#define USCII2C_0_RESET_START_HANDLER         USCII2C0StartHandler=ucblank
#define USCII2C_0_SET_LOST_HANDLER(handler)   USCII2C0LostHandler=(handler)
#define USCII2C_0_RESET_LOST_HANDLER          USCII2C0LostHandler=ucblank

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCII2C0StateISR();

#define USCII2C_0_SET_TX_HANDLER(handler)     USCII2C0TXHandler=(handler)
#define USCII2C_0_RESET_TX_HANDLER            USCII2C0TXHandler=ucblank
#define USCII2C_0_SET_RX_HANDLER(handler)     USCII2C0RXHandler=(handler)
#define USCII2C_0_RESET_RX_HANDLER            USCII2C0RXHandler=ucblank

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCII2C0TRXISR();

#endif

#endif

#ifdef __USE_1_I2C_
#ifndef __Alternative_1_I2C_

uchar (*USCII2C1NackHandler)();
uchar (*USCII2C1StopHandler)();
uchar (*USCII2C1StartHandler)();
uchar (*USCII2C1LostHandler)();
uchar (*USCII2C1TXHandler)();
uchar (*USCII2C1RXHandler)();

#define USCII2C_1_SET_NACK_HANDLER(handler)   USCII2C1NackHandler=(handler)
#define USCII2C_1_RESET_NACK_HANDLER          USCII2C1NackHandler=ucblank
#define USCII2C_1_SET_STOP_HANDLER(handler)   USCII2C1StopHandler=(handler)
#define USCII2C_1_RESET_STOP_HANDLER          USCII2C1StopHandler=ucblank
#define USCII2C_1_SET_START_HANDLER(handler)  USCII2C1StartHandler=(handler)
#define USCII2C_1_RESET_START_HANDLER         USCII2C1StartHandler=ucblank
#define USCII2C_1_SET_LOST_HANDLER(handler)   USCII2C1LostHandler=(handler)
#define USCII2C_1_RESET_LOST_HANDLER          USCII2C1LostHandler=ucblank

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCII2C1StateISR();

#define USCII2C_1_SET_TX_HANDLER(handler)     USCII2C1TXHandler=(handler)
#define USCII2C_1_RESET_TX_HANDLER            USCII2C1TXHandler=ucblank
#define USCII2C_1_SET_RX_HANDLER(handler)     USCII2C1RXHandler=(handler)
#define USCII2C_1_RESET_RX_HANDLER            USCII2C1RXHandler=ucblank

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCII2C1TRXISR();

#endif
#endif

/************************I2C GENERAL DEFINITIONS***************************/
//UCBxI2CIE
#define USCII2C_0IE_ASN(bitmap)         ASN_BIT(UCB0I2CIE,(bitmap))
#define USCII2C_0IE_SET(bitmap)         SET_BIT(UCB0I2CIE,(bitmap))
#define USCII2C_0IE_CLR(bitmap)         CLR_BIT(UCB0I2CIE,(bitmap))

#define USCII2C_1IE_ASN(bitmap)         ASN_BIT(UCB1I2CIE,(bitmap))
#define USCII2C_1IE_SET(bitmap)         SET_BIT(UCB1I2CIE,(bitmap))
#define USCII2C_1IE_CLR(bitmap)         CLR_BIT(UCB1I2CIE,(bitmap))

//Address Registers
typedef uint USCII2C_Addr;
#define USCII2C_0OA_ASN(addr)           ASN_BIT(UCB0I2COA,(addr))
#define USCII2C_0SA_ASN(addr)           ASN_BIT(UCB0I2CSA,(addr))
#define USCII2C_GENERAL_CALL            (0x8000)

#define USCII2C_1OA_ASN(addr)           ASN_BIT(UCB1I2COA,(addr))
#define USCII2C_1SA_ASN(addr)           ASN_BIT(UCB1I2CSA,(addr))

/***************************UCBxCTL0 MODIFIER******************************/
#define USCII2C_OA10                    (0x80)
#define USCII2C_SA10                    (0x40)
#define USCII2C_MULTI_MASTER            (0x20)
#define USCII2C_MASTER                  (0x08)
#define USCII2C_I2C_MODE                (0x06)
#define USCII2C_SYNC                    (0x01)

/***************************UCBxCTL1 MODIFIER******************************/
typedef uchar USCII2C_Clk;
#define USCII2C_UCLK                    (0x00)
#define USCII2C_SMCLK                   (0xC0)

#define USCII2C_RECEIVER                (0x00)
#define USCII2C_TRANSMITTER             (0x10)

#define USCII2C_NACK                    (0x08)
#define USCII2C_STOP                    (0x04)
#define USCII2C_START                   (0x02)
#define USCII2C_RESET                   (0x01)

/***************************UCBxSTAT MODIFIER******************************/
#define USCII2C_SCL_LOW                 (0x40)
#define USCII2C_GENERAL_CALLED          (0x20)
#define USCII2C_BUS_BUSY                (0x10)
#define USCII2C_NACK_IFG                (0x08)
#define USCII2C_STOP_IFG                (0x04)
#define USCII2C_START_IFG               (0x02)
#define USCII2C_LOST_IFG                (0x01)

/**************************UCBxI2CIE MODIFIER******************************/
#define USCII2C_NACK_IE                 (0x08)
#define USCII2C_STOP_IE                 (0x04)
#define USCII2C_START_IE                (0x02)
#define USCII2C_LOST_IE                 (0x01)

/************************INITIALIZER DEFINITIONS***************************/
/*
 * Introduction:
 * default:
 * UCBxCTL0 <- 7-bit Own Addr., Single Master, I2C Mode, Sync
 * UCBxCTL1 <- SMCLK
 * UCBxI2COA<- No General Call
 * UCB0BRW  <- 12
 *
 * own and salve addr. is defaultly 7-bit, which means up to 128(?) devices are supported.
 *     this should satisfy most of conditions. if not, set USCII2C_OA10 bit 
 *     to widen the width of addr.:
 *     USCI_B0CTL0_SET(USCII2C_OA10);
 *     USCI_B0CTL0_SET(USCII2C_SA10);
 */

#ifdef __USE_0_I2C_
/*
 * Introduction:
 * Master Functional Scheduler.
 * master mode is an aggresive mode.
 * every movement needs to be configurated explicitly.
 */
void USCII2C0MasterInit(USCII2C_Addr ownAddr);
void USCII2C0MasterSend(USCII2C_Addr slaveAddr,uchar* dataAdd,uint length);
void USCII2C0MasterReceive(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length);

/*
 * Introduction:
 * Master sending and receiving functions with blocking machanism.
 * these func.s will not return untill sending and receiving finish.
 * it's much safer to use these func.s than use the upper one.
 * so use following functions instead in most of time.
 * but there is a certain risk to be stuck in these func.s for good if some errors
 *     occur in the communication layer.
 * choose which function to call wisely on demand.
 */
void USCII2C0MasterSendBlock(USCII2C_Addr slaveAddr,uchar* dataAdd,uint length);
void USCII2C0MasterReceiveBlock(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length);

#ifndef __Alternative_0_I2C_
/*
 * Introduction:
 * this is a short-cut version of USCII2C0MasterSend().
 * this function is implemented for the case that one-byte data sending is required.
 *
 * by the way, function like USCII2C0MasterReceiveByte() is useless.
 * this function can be easily archieved by calling:
 *     USCII2C0MasterReceive(slaveAddr,&oneByteData,1);
 */
void USCII2C0MasterSendByte(USCII2C_Addr slaveAddr,uchar data);
void USCII2C0MasterSendByteBlock(USCII2C_Addr slaveAddr,uchar data);
/* 
 * NOTICE:
 * salve mode is a passive mode.
 * to work well, I implemented two kinds of prototypes to match the cases with 
 *     and without my pre-implemented ISR framework.
 * if with my framework, the sending src addr. and recieving dst addr. can be 
 *     passed in and registered in the framework. further functionalities would 
 *     work automatically.
 * in this case, the prototype is:
 *     void USCII2C0SlaveInit(USCII2C_Addr ownAddr,
 *                            uchar* sendAddr,uint sendLen,
 *                            uchar* receiveAddr,uint receiveLen);
 *
 * if without my framework, all the salve mode features are in fact opened.
 * everything needs to be configurated by hand, which means bugs could be 
 *     easily planted.
 * in this case, the prototype is:
 *     void USCII2C0SlaveInit(USCII2C_Addr ownAddr);
 */
void USCII2C0SlaveInit(USCII2C_Addr ownAddr,
                       uchar* sendAddr,uint sendLen,
                       uchar* receiveAddr,uint receiveLen);

/*
 * Sending State Modifiers
 */
uint USCII2C0GetActualSendedNum();
void USCII2C0ResetSendCount();

/* 
 * Recieving State Modifiers
 */
uint USCII2C0GetActualReceivedNum();
void USCII2C0ResetReceiveCount();

#else

void USCII2C0SlaveInit(USCII2C_Addr ownAddr);

#endif

#endif

/*
 * NOTICE:
 * __USE_1_I2C_ case is similar to the above one.
 * read annotations above to know about each funcion.
 */
#ifdef __USE_1_I2C_

void USCII2C1MasterInit(USCII2C_Addr ownAddr);
void USCII2C1MasterSend(USCII2C_Addr slaveAddr,uchar* dataAdd,uint length);
void USCII2C1MasterReceive(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length);

void USCII2C1MasterSendBlock(USCII2C_Addr slaveAddr,uchar* dataAdd,uint length);
void USCII2C1MasterReceiveBlock(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length);

#ifndef __Alternative_1_I2C_

void USCII2C1MasterSendByte(USCII2C_Addr slaveAddr,uchar data);
void USCII2C1MasterSendByteBlock(USCII2C_Addr slaveAddr,uchar data);

void USCII2C1SlaveInit(USCII2C_Addr ownAddr,
                       uchar* sendAddr,uint sendLen,
                       uchar* ReceiveAddr,uint ReceiveLen);

uint USCII2C1GetActualSendedNum();
void USCII2C1ResetSendCount();

uint USCII2C1GetActualReceivedNum();
void USCII2C1ResetReceiveCount();

#else

void USCII2C1SlaveInit(USCII2C_Addr ownAddr);

#endif

#endif

#include "USCII2C.c"

#endif

//                            Ver. 0.3, all tested
//                            Hu Zhongsheng implemented, last update in 2013/9/5