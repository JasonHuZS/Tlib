#ifndef _DDS_H_
#define _DDS_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a head file of some peripheral DDS chips.
 * read specified head file for more infomation.
 * available chips and modes:
 *     AD9854: Parallel mode:   __USE_Parallel_AD9854_
 *             Serial SPI mode: __USE_SPI_AD9854_ 
 */

#ifdef __USE_Parallel_AD9854_
#include "AD9854Parallel.h"
#else
#ifdef __USE_SPI_AD9854_
#include "AD9854SPI.h"
#endif
#endif

#endif