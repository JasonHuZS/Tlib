#ifndef _KeyBoard4X4_H_
#define _KeyBoard4X4_H_
#include "msp430f2619.h"
#include "KeyBoard.h"
#include "General.h"

/*
 * this is a 4X4 KeyBoard head file.
 * call KB4X4Init() before use it.
 * set KB4X4handler to handle events.
 * user must know which port is using, thus
 *     #define __USE_PORT1_KB4X4_
 * before including this file.
 * PORT:
 * if defined __USE_PORT1_KB4X4_:
 * P1.0~3->interruption ports
 * P1.4~7->signal ports
 * if defined __USE_PORT2_KB4X4_:
 * P2.0~3->interruption ports
 * P2.4~7->signal ports
 * connecting illustration:
 * 
 *   Vcc /|\
 *        | 
 *        --res--------- Px.0~3
 *         10kohm   |
 *                 key
 *                  |
 *                  |
 *                Px.4~7
 */
/*
 * only work for pull-up resistor keyboard.
 */

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * followings are two function ptr declarations.
 */
/*
 * KB4X4handler is a function pointer matrix containing 16 items.
 * assigned handler normally:
 *     void handler_foo();
 *     KB1X8handler[0][0]=handler_foo;
 * 
 * the return values of handlers is meaningful.
 * if a handler returns 0, CPUOFF bit will remain the same.
 * if a handler returns non-0, CPUOFF bit will be cleared after ISR returns.
 */
uchar (*KB4X4handler[4][4])();
/*
 * KB4X4errorHandler handles default case in switch and error cases.
 */
uchar (*KB4X4errorHandler)();

/*
 * you are free to use mutators to modify handlers or just
 *     perform assignment operations directly.
 */
inline void KB4X4ResetHandler(int col,int row);
inline void KB4X4SetHandler(int col,int row,uchar (*handler)());
inline void KB4X4ResetErrorHandler();
inline void KB4X4SetErrorHandler(uchar (*handler)());

/*********************GENERAL MACRO DEFINITIONS**************************/
#define KB4X4_RESET_HANDLER(col,row)        KB4X4handler[(col)][(row)]=ucblank
#define KB4X4_SET_HANDLER(col,row,handler)  KB4X4handler[(col)][(row)]=(handler)
#define KB4X4_RESET_ERROR_HANDLER           KB4X4errorHandler=ucblank
#define KB4X4_SET_ERROR_HANDLER(handler)    KB4X4errorHandler=(handler)

/************************INITIALIZER DEFINITIONS**************************/
/*
 * call KB4X4Init() before doing anything!
 */
void KB4X4Init();

/*********************CONVENIENT TOOL DEFINITIONS*************************/
/*
 * fast set handler by calling this funny convenient tool.
 * fill in handlers in key board lattice order.
 * easy and straight-forward.
 */
void KB4X4SetHandlerGroup(uchar (*handler00)(),uchar (*handler01)(),uchar (*handler02)(),uchar (*handler03)(),
                          uchar (*handler10)(),uchar (*handler11)(),uchar (*handler12)(),uchar (*handler13)(),
                          uchar (*handler20)(),uchar (*handler21)(),uchar (*handler22)(),uchar (*handler23)(),
                          uchar (*handler30)(),uchar (*handler31)(),uchar (*handler32)(),uchar (*handler33)());


#ifdef __USE_PORT1_KB4X4_

#define KB4X4_INT_VEC   (PORT1_VECTOR)
#define KB4X4_IFG       (P1IFG)
#define KB4X4_IN        (P1IN)
#define KB4X4_IE        (P1IE)
#define KB4X4_IES       (P1IES)
#define KB4X4_DIR       (P1DIR)
#define KB4X4_SEL       (P1SEL)
#define KB4X4_OUT       (P1OUT)

#pragma vector= KB4X4_INT_VEC
__interrupt void KB4X4ISR();

#include "KeyBoard4X4.c"

#else
#ifdef __USE_PORT2_KB4X4_

#define KB4X4_INT_VEC   (PORT2_VECTOR)
#define KB4X4_IFG       (P2IFG)
#define KB4X4_IN        (P2IN)
#define KB4X4_IE        (P2IE)
#define KB4X4_IES       (P2IES)
#define KB4X4_DIR       (P2DIR)
#define KB4X4_SEL       (P2SEL)
#define KB4X4_OUT       (P2OUT)

#pragma vector= KB4X4_INT_VEC
__interrupt void KB4X4ISR();

#include "KeyBoard4X4.c"

#endif
#endif

#endif

//                            Ver. 0.8, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/24
