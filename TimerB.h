#ifndef _TimerB_H_
#define _TimerB_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a TIMERB head file
 * 
 * User-defined Port Usage:
 * P4.0 -> out:TB0 in: CCI0A/B
 * P4.1 -> out:TB1 in: CCI1A/B
 * P4.2 -> out:TB2 in: CCI2A/B
 * P4.3 -> out:TB3 in: CCI3A/B
 * P4.4 -> out:TB4 in: CCI4A/B
 * P4.5 -> out:TB5 in: CCI5A/B
 * P4.6 -> out:TB6 in: CCI6A/B
 * P4.7 -> in: TBCLK
 */

/*
 * NOTICE:
 * once TimerB_TBCLR is used. TimerB needs to be initialized again!
 * modify proper system clock using SysClk.h before use this lib.
 * if precision is required, using XT2 is required.
 *
 * NOTICE AGAIN:
 * TimerB has more special features than TimerA, such as grouping and 
 *     compare latch load control.
 * This feature shoulbe not be required normally.
 * If you do, make sure you understand the functionality of them, and use:
 *     void TimerBCompareAdvancedInit(TimerB_ModeCtrl mcbit,
 *                                    TimerB_TBCLGroup grpbit,
 *                                    uint TBCCTL_num,
 *                                    ...);
 *  to configurate.
 */

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * handler may be not as efficient as required.
 * if efficiency is required, write a ISR function yourself.
 * before doing that, use macro directive to disinclude existed implementation:
 *     #define __Alternative_TimerB_0_
 *     //...statement...
 *     #pragma vector=TIMERB0_VECTOR
 *     __interrupt void MyDefinedTIMERB0ISR(){}
 */

#ifndef __Alternative_TimerB_0_         //0xFFFA Timer B CC0

uchar (*TimerBisr0Handler)();

#define TIMERB_SET_HANDLER0(handler)        TimerBisr0Handler=(handler)
#define TIMERB_RESET_HANDLER0               TimerBisr0Handler=ucblank

#pragma vector=TIMERB0_VECTOR
__interrupt void TIMERB0ISR();
#endif

#ifndef __Alternative_TimerB_1_         //0xFFF8 Timer B CC1-6, TB

uchar (*TimerBCCHandler[6])();
uchar (*TimerBTBHandler)();

/* 
 * notice here chnl is the channel index, starting from 1 to 6.
 */
#define TIMERB_SET_CC_HANDLER(chnl,handler) TimerBCCHandler[(chnl)-1]=(handler)
#define TIMERB_RESET_CC_HANDLER(chnl)       TimerBCCHandler[(chnl)-1]=ucblank
#define TIMERB_SET_TB_HANDLER(handler)      TimerBTBHandler=(handler)
#define TIMERB_RESET_TB_HANDLER             TimerBTBHandler=ucblank

#pragma vector=TIMERB1_VECTOR
__interrupt void TIMERB1ISR();
#endif

/***********************GENERAL MACRO DEFINITIONS**************************/
#define TIMERB_TB_INIT(bitpos)              SET_BIT(P4DIR,(bitpos));\
                                            SET_BIT(P4SEL,(bitpos))
#define TIMERB_CCI_INIT(bitpos)             CLR_BIT(P4DIR,(bitpos));\
                                            SET_BIT(P4SEL,(bitpos))
#define TIMERB_START                        TimerBStart()
#define TIMERB_PAUSE                        TimerBPause()

/***************************TBCTL MODIFIER*********************************/
//TBCLx Group
typedef uint TimerB_TBCLGroup;
#define TIMERB_TBCLBIT              (0x6000)
#define TIMERB_ALL_INDEPENDENT      (0x0000)
#define TIMERB_12_34_56_0           (0x2000)
#define TIMERB_123_456_0            (0x4000)
#define TIMERB_ALL_GROUPED          (0x6000)
#define TIMERB_SET_ALL_INDEPENDENT  CLR_BIT(TBCTL,TIMERB_TBCLBIT)
#define TIMERB_SET_12_34_56_0       CLR_BIT(TBCTL,TIMERB_TBCLBIT);SET_BIT(TBCTL,TIMERB_12_34_56_0)
#define TIMERB_SET_123_456_0        CLR_BIT(TBCTL,TIMERB_TBCLBIT);SET_BIT(TBCTL,TIMERB_123_456_0)
#define TIMERB_SET_ALL_GROUPED      SET_BIT(TBCTL,TIMERB_ALL_GROUPED)

//Counter Length
typedef uint TimerB_CounterLength;
#define TIMERB_CNTLBIT              (0x1800)
#define TIMERB_16BIT                (0x0000)
#define TIMERB_12BIT                (0x0800)
#define TIMERB_10BIT                (0x1000)
#define TIMERB_8BIT                 (0x1800)

//CLK Control
#define TIMERB_TBCLK                CLR_BIT(P4DIR,BIT7);SET_BIT(P4SEL,BIT7);\
                                    CLR_BIT(TBCTL,0x0300)
#define TIMERB_SMCLK                CLR_BIT(TBCTL,0x0300);SET_BIT(TBCTL,0x0200)
#define TIMERB_INCLK                CLR_BIT(P2DIR,BIT1);SET_BIT(P2SEL,BIT1);\
                                    SET_BIT(TBCTL,0x0300)
#define TIMERB_DIVISOR(div)         CLR_BIT(TBCTL,0x00C0);\
                                    SET_BIT(TBCTL,((bit2index((div)))<<6))
                                    //div should and only should be 1 2 4 8

//Interrupt Control
#define TIMERB_TBCLR                SET_BIT(TBCTL,TBCLR)
#define TIMERB_SET_TBIE             SET_BIT(TBCTL,TBIE)
#define TIMERB_CLR_TBIE             CLR_BIT(TBCTL,TBIE)
#define TIMERB_CLR_TBIFG            CLR_BIT(TBCTL,TBIFG)

//Mode Control
typedef uint TimerB_ModeCtrl;
#define TIMERB_MCBIT                (0x0030)
#define TIMERB_STOP                 (0x0000)
#define TIMERB_UP                   (0x0010)
#define TIMERB_CONTINUE             (0x0020)
#define TIMERB_UP_DOWN              (0x0030)
#define TIMERB_SET_STOP             CLR_BIT(TBCTL,0x0030)
#define TIMERB_SET_UP               CLR_BIT(TBCTL,0x0030);SET_BIT(TBCTL,0x0010)
#define TIMERB_SET_CONTINUE         CLR_BIT(TBCTL,0x0030);SET_BIT(TBCTL,0x0020)
#define TIMERB_SET_UP_DOWN          SET_BIT(TBCTL,0x0030)

//there are 3 TBCCTLx, 0 1 2
/**************************TBCCTLx MODIFIER********************************/
#define TIMERB_CCTL0_SET(bitmap)    SET_BIT(TBCCTL0,(bitmap))
#define TIMERB_CCTL0_CLR(bitmap)    CLR_BIT(TBCCTL0,(bitmap))
#define TIMERB_CCTL0_INV(bitmap)    INV_BIT(TBCCTL0,(bitmap))

#define TIMERB_CCTL1_SET(bitmap)    SET_BIT(TBCCTL1,(bitmap))
#define TIMERB_CCTL1_CLR(bitmap)    CLR_BIT(TBCCTL1,(bitmap))
#define TIMERB_CCTL1_INV(bitmap)    INV_BIT(TBCCTL1,(bitmap))

#define TIMERB_CCTL2_SET(bitmap)    SET_BIT(TBCCTL2,(bitmap))
#define TIMERB_CCTL2_CLR(bitmap)    CLR_BIT(TBCCTL2,(bitmap))
#define TIMERB_CCTL2_INV(bitmap)    INV_BIT(TBCCTL2,(bitmap))

#define TIMERB_CCTL3_SET(bitmap)    SET_BIT(TBCCTL3,(bitmap))
#define TIMERB_CCTL3_CLR(bitmap)    CLR_BIT(TBCCTL3,(bitmap))
#define TIMERB_CCTL3_INV(bitmap)    INV_BIT(TBCCTL3,(bitmap))

#define TIMERB_CCTL4_SET(bitmap)    SET_BIT(TBCCTL4,(bitmap))
#define TIMERB_CCTL4_CLR(bitmap)    CLR_BIT(TBCCTL4,(bitmap))
#define TIMERB_CCTL4_INV(bitmap)    INV_BIT(TBCCTL4,(bitmap))

#define TIMERB_CCTL5_SET(bitmap)    SET_BIT(TBCCTL5,(bitmap))
#define TIMERB_CCTL5_CLR(bitmap)    CLR_BIT(TBCCTL5,(bitmap))
#define TIMERB_CCTL5_INV(bitmap)    INV_BIT(TBCCTL5,(bitmap))

#define TIMERB_CCTL6_SET(bitmap)    SET_BIT(TBCCTL6,(bitmap))
#define TIMERB_CCTL6_CLR(bitmap)    CLR_BIT(TBCCTL6,(bitmap))
#define TIMERB_CCTL6_INV(bitmap)    INV_BIT(TBCCTL6,(bitmap))

//Output Mode
/*
 * TR -> Toggle/Reset
 * SR -> Set/Reset
 * TS -> Toggle/Set
 * RS -> Reset/Set
 */
typedef uint TimerB_OutputM;
#define TIMERB_OMBIT                (0x00E0)
#define TIMERB_OUT                  (0x0000)
#define TIMERB_SET                  (0x0020)
#define TIMERB_TR                   (0x0040)
#define TIMERB_SR                   (0x0060)
#define TIMERB_TOGGLE               (0x0080)
#define TIMERB_RESET                (0x00A0)
#define TIMERB_TS                   (0x00C0)
#define TIMERB_RS                   (0x00E0)

//Capture Mode
typedef uint TimerB_CaptureM;
#define TIMERB_CMBIT                (0xC000)
#define TIMERB_NOCAP                (0x0000)
#define TIMERB_RISING               (0x4000)
#define TIMERB_FALLING              (0x8000)
#define TIMERB_BOTH                 (0xC000)

//Capture/Compare Input Select
typedef uint TimerB_CCInput;
#define TIMERB_CCISBIT              (0x3000)
#define TIMERB_CCIA                 (0x0000)
#define TIMERB_CCIB                 (0x1000)
#define TIMERB_GND                  (0x2000)
#define TIMERB_VCC                  (0x3000)

//Compare Latch Load
typedef uint TimerB_CompLatchLoad;
#define TIMERB_CLLDBIT              (0x0600)
#define TIMERB_WHEN_WRITTEN         (0x0000)
#define TIMERB_TBR_TO_0             (0x0200)
#define TIMERB_TBR_TO_CL0           (0x0400)
#define TIMERB_TBR_TO_CLX           (0x0600)

//Others
#define TIMERB_SCS                  (0x0800)
#define TIMERB_CAP                  (0x0100)
#define TIMERB_CCI                  (0x0008)
#define TIMERB_OUTPUT               (0x0004)

//Interrupt&Overflow Control
#define TIMERB_SET_CC0IE            SET_BIT(TBCCTL0,CCIE)
#define TIMERB_CLR_CC0IE            CLR_BIT(TBCCTL0,CCIE)
#define TIMERB_CLR_CC0IFG           CLR_BIT(TBCCTL0,CCIFG)
#define TIMERB_GET_CC0COV           (TBCCTL0&COV)
#define TIMERB_CLR_CC0COV           CLR_BIT(TBCCTL0,COV)

#define TIMERB_SET_CC1IE            SET_BIT(TBCCTL1,CCIE)
#define TIMERB_CLR_CC1IE            CLR_BIT(TBCCTL1,CCIE)
#define TIMERB_CLR_CC1IFG           CLR_BIT(TBCCTL1,CCIFG)
#define TIMERB_GET_CC1COV           (TBCCTL1&COV)
#define TIMERB_CLR_CC1COV           CLR_BIT(TBCCTL1,COV)

#define TIMERB_SET_CC2IE            SET_BIT(TBCCTL2,CCIE)
#define TIMERB_CLR_CC2IE            CLR_BIT(TBCCTL2,CCIE)
#define TIMERB_CLR_CC2IFG           CLR_BIT(TBCCTL2,CCIFG)
#define TIMERB_GET_CC2COV           (TBCCTL2&COV)
#define TIMERB_CLR_CC2COV           CLR_BIT(TBCCTL2,COV)

#define TIMERB_SET_CC3IE            SET_BIT(TBCCTL3,CCIE)
#define TIMERB_CLR_CC3IE            CLR_BIT(TBCCTL3,CCIE)
#define TIMERB_CLR_CC3IFG           CLR_BIT(TBCCTL3,CCIFG)
#define TIMERB_GET_C3COV           (TBCCTL3&COV)
#define TIMERB_CLR_CC3COV           CLR_BIT(TBCCTL3,COV)

#define TIMERB_SET_CC4IE            SET_BIT(TBCCTL4,CCIE)
#define TIMERB_CLR_CC4IE            CLR_BIT(TBCCTL4,CCIE)
#define TIMERB_CLR_CC4IFG           CLR_BIT(TBCCTL4,CCIFG)
#define TIMERB_GET_CC4COV           (TBCCTL4&COV)
#define TIMERB_CLR_CC4COV           CLR_BIT(TBCCTL4,COV)

#define TIMERB_SET_CC5IE            SET_BIT(TBCCTL5,CCIE)
#define TIMERB_CLR_CC5IE            CLR_BIT(TBCCTL5,CCIE)
#define TIMERB_CLR_CC5IFG           CLR_BIT(TBCCTL5,CCIFG)
#define TIMERB_GET_CC5COV           (TBCCTL5&COV)
#define TIMERB_CLR_CC5COV           CLR_BIT(TBCCTL5,COV)

#define TIMERB_SET_CC6IE            SET_BIT(TBCCTL6,CCIE)
#define TIMERB_CLR_CC6IE            CLR_BIT(TBCCTL6,CCIE)
#define TIMERB_CLR_CC6IFG           CLR_BIT(TBCCTL6,CCIFG)
#define TIMERB_GET_CC6COV           (TBCCTL6&COV)
#define TIMERB_CLR_CC6COV           CLR_BIT(TBCCTL6,COV)

/***************************TBCCRx MODIFIER********************************/
#define TIMERB_CCR0_ASN(value)      ASN_BIT(TBCCR0,(value))
#define TIMERB_CCR1_ASN(value)      ASN_BIT(TBCCR1,(value))
#define TIMERB_CCR2_ASN(value)      ASN_BIT(TBCCR2,(value))
#define TIMERB_CCR3_ASN(value)      ASN_BIT(TBCCR3,(value))
#define TIMERB_CCR4_ASN(value)      ASN_BIT(TBCCR4,(value))
#define TIMERB_CCR5_ASN(value)      ASN_BIT(TBCCR5,(value))
#define TIMERB_CCR6_ASN(value)      ASN_BIT(TBCCR6,(value))

/*****************************TBIV MODIFIER********************************/
#define TIMERB_TBIV_1IFG            (0x0002)
#define TIMERB_TBIV_2IFG            (0x0004)
#define TIMERB_TBIV_3IFG            (0x0006)
#define TIMERB_TBIV_4IFG            (0x0008)
#define TIMERB_TBIV_5IFG            (0x000A)
#define TIMERB_TBIV_6IFG            (0x000C)
#define TIMERB_TBIV_TBIFG           (0x000E)


/*********************GENERAL FUNCTION DEFINITIONS*************************/
/*
 * TIMERB is too difficult to encapsulate well, so the following initializing 
 *     func.s may or may not match your requirement.
 * if not, use modifier above additionally or write your own version.
 * if you choose to define your own version, do not use functions below.
 */
/*
 * NOTICE:
 * Port is not initialized. do port initialization yourself.
 * hint: try to use following macro:
 *     TIMERB_TB_INIT(bitpos)
 *     TIMERB_CCI_INIT(bitpos)
 */

//call this function when you want to start timing
void TimerBStart();
//call this function when you want to stop timing
void TimerBPause();

/*
 * default: 
 * TBCTL <- all independent, 16bit, SMCLK, /1
 * TBCCTLx <- load on write,  No Capture
 */
void TimerBCompareInit(TimerB_ModeCtrl mcbit,       //TBCTL mode control bits
                       uint TBCCTL_num,             //number of used TBCCTLx
                       ...);                        //TBCCTLx control bits, x <- 0~6
//control bits sequence: 
//    (uint)value of TBCCRx, (TimerB_OutputM)output mode control bits

/*
 * default: 
 * TBCTL <- 16bit, SMCLK, /1
 * TBCCTLx <- No Capture
 */
void TimerBCompareAdvancedInit(TimerB_ModeCtrl mcbit,       //TBCTL mode control bits
                               TimerB_TBCLGroup grpbit,     //TBCTL grouping ctrl bits
                               uint TBCCTL_num,             //number of used TBCCTLx
                               ...);                        //TBCCTLx control bits, x <- 0~6
//control bits sequence: 
//    (uint)value of TBCCRx, (TimerB_OutputM)output mode control bits, (TimerB_CompLatchLoad) TBCLx load ctrl bits

/*
 * default: 
 * TBCTL <- all independent, 16bit, SMCLK, /1
 * TBCCTLx <- load on write, CCIA, SCS, CAP
 */
void TimerBCaptureInit(TimerB_ModeCtrl mcbit,       //TBCTL mode control bits
                       uint TBCCTL_num,             //number of used TBCCTLx
                       ...);                        //TBCCTLx control bits, x <- 0~6
//control bits sequence: 
//    (uint)value of TBCCRx, (TimerB_CaptureM)capture mode control bits
/*
 * NOTICE AGAIN:
 * I found that when calling functions TimerBCompareInit(), TimerBCompareAdvancedInit(), 
 *     and TimerBCaptureInit(), if parameter(s) '(uint)value of TBCCRx' is(are) larger 
 *     than or equal to 32768, which is 0x8000 in hexadecimal, the compiler will recognise 
 *     this number as a 32-bit long integer, instead of a 16-bit long one, since compiler 
 *     presumes all integers are signed.
 * in this case, the stack is corrupted by compiler, so that the result is no longer 
 *     correct.
 * I tried to fix this problem but well, I didnot come up with a good way to handle this.
 * However, I do have a explicit way to avoid this:
 *     TimerBCompareInit(TIMERB_UP,1,(uint)0x8000,TIMERB_OUT);
 * in this way, you tell the compiler that number is in uint type, without regarding 
 *     it as a signed integer.
 *
 *
 * variable length argument is introduced, call those functions as follows:
 *     TimerBCompareInit(TIMERB_UP,1,
 *                       0x1000,TIMERB_TR);
 * or more complecated one:
 *     TimerBCaptureInit(TIMERB_UP_DOWN,3,
 *                       0x3000,TIMERB_RISING, //<- ch0 setting
 *                       0x2000,TIMER_FALLING, //<- ch1 setting
 *                       0x1000,TIMER_BOTH);   //<- ch2 setting
 * I try to maintain as many interfaces as it ought to be.
 * it should be robust enough. try and return feedback to me.
 * if default does not match your requirement slightly, use modifiers above.
 */

#include "TimerB.c"

#endif

//                            Ver. 0.4, all tested
//                            Hu Zhongsheng implemented, last update in 2013/9/6
