#ifndef _USCI_H_
#define _USCI_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a head file of USCI modules.
 * to use encapsulated modules,  define following selections before 
 *     including this head file:
 *     #define __USE_0_I2C_
 *     #define __USE_1_I2C_
 *     #define __USE_0_UART_
 *     #define __USE_1_UART_
 *     #define __USE_A0_SPI_
 *     #define __USE_A1_SPI_
 *     #define __USE_B0_SPI_
 *     #define __USE_B1_SPI_
 *
 * see specified introductions in head files of respective modules.
 *
 * USCI modules are too complecated to configurate by hand systematically.
 * to really simplify this module, a software scheduler is needed.
 * I have no time to implement this, so if you are reading this head file,
 *     that means USCI feature is one of your requirements.
 * so if you are a good C programmer, and an ambitious one, please implement
 *     one and share it with others.
 */

/***********************GENERAL MACRO DEFINITIONS**************************/
//UCAxCTL0 / UCBxCTL0
#define USCI_A0CTL0_SET(bitmap)         SET_BIT(UCA0CTL0,(bitmap))
#define USCI_A0CTL0_CLR(bitmap)         CLR_BIT(UCA0CTL0,(bitmap))
#define USCI_A0CTL0_ASN(bitmap)         ASN_BIT(UCA0CTL0,(bitmap))

#define USCI_A1CTL0_SET(bitmap)         SET_BIT(UCA1CTL0,(bitmap))
#define USCI_A1CTL0_CLR(bitmap)         CLR_BIT(UCA1CTL0,(bitmap))
#define USCI_A1CTL0_ASN(bitmap)         ASN_BIT(UCA1CTL0,(bitmap))

#define USCI_B0CTL0_GET(bitmap)         (UCB0CTL0&(bitmap))
#define USCI_B0CTL0_SET(bitmap)         SET_BIT(UCB0CTL0,(bitmap))
#define USCI_B0CTL0_CLR(bitmap)         CLR_BIT(UCB0CTL0,(bitmap))
#define USCI_B0CTL0_ASN(bitmap)         ASN_BIT(UCB0CTL0,(bitmap))

#define USCI_B1CTL0_GET(bitmap)         (UCB1CTL0&(bitmap))
#define USCI_B1CTL0_SET(bitmap)         SET_BIT(UCB1CTL0,(bitmap))
#define USCI_B1CTL0_CLR(bitmap)         CLR_BIT(UCB1CTL0,(bitmap))
#define USCI_B1CTL0_ASN(bitmap)         ASN_BIT(UCB1CTL0,(bitmap))

//UCAxCTL1 / UCBxCTL1
#define USCI_A0CTL1_SET(bitmap)         SET_BIT(UCA0CTL1,(bitmap))
#define USCI_A0CTL1_CLR(bitmap)         CLR_BIT(UCA0CTL1,(bitmap))
#define USCI_A0CTL1_ASN(bitmap)         ASN_BIT(UCA0CTL1,(bitmap))

#define USCI_A1CTL1_SET(bitmap)         SET_BIT(UCA1CTL1,(bitmap))
#define USCI_A1CTL1_CLR(bitmap)         CLR_BIT(UCA1CTL1,(bitmap))
#define USCI_A1CTL1_ASN(bitmap)         ASN_BIT(UCA1CTL1,(bitmap))

#define USCI_B0CTL1_GET(bitmap)         (UCB0CTL1&(bitmap))
#define USCI_B0CTL1_SET(bitmap)         SET_BIT(UCB0CTL1,(bitmap))
#define USCI_B0CTL1_CLR(bitmap)         CLR_BIT(UCB0CTL1,(bitmap))
#define USCI_B0CTL1_ASN(bitmap)         ASN_BIT(UCB0CTL1,(bitmap))

#define USCI_B1CTL1_GET(bitmap)         (UCB1CTL1&(bitmap))
#define USCI_B1CTL1_SET(bitmap)         SET_BIT(UCB1CTL1,(bitmap))
#define USCI_B1CTL1_CLR(bitmap)         CLR_BIT(UCB1CTL1,(bitmap))
#define USCI_B1CTL1_ASN(bitmap)         ASN_BIT(UCB1CTL1,(bitmap))

//UCAxBRy / UCBxBRy
#define UCA0BRW_                        (UCA0BR0_)
DEFW(   UCA0BRW                       , UCA0BR0_)
#define USCI_A0BRW_ASN(rate)            ASN_BIT(UCA0BRW,(rate))
#define USCI_A0BR0_ASN(rate)            ASN_BIT(UCA0BR0,(rate))
#define USCI_A0BR1_ASN(rate)            ASN_BIT(UCA0BR1,(rate))

#define UCA1BRW_                        (UCA1BR0_)
DEFW(   UCA1BRW                       , UCA1BR0_)
#define USCI_A1BRW_ASN(rate)            ASN_BIT(UCA1BRW,(rate))
#define USCI_A1BR0_ASN(rate)            ASN_BIT(UCA1BR0,(rate))
#define USCI_A1BR1_ASN(rate)            ASN_BIT(UCA1BR1,(rate))

#define UCB0BRW_                        (UCB0BR0_)
DEFW(   UCB0BRW                       , UCB0BR0_)
#define USCI_B0BRW_ASN(rate)            ASN_BIT(UCB0BRW,(rate))
#define USCI_B0BR0_ASN(rate)            ASN_BIT(UCB0BR0,(rate))
#define USCI_B0BR1_ASN(rate)            ASN_BIT(UCB0BR1,(rate))

#define UCB1BRW_                        (UCB1BR0_)
DEFW(   UCB1BRW                       , UCB1BR0_)
#define USCI_B1BRW_ASN(rate)            ASN_BIT(UCB1BRW,(rate))
#define USCI_B1BR0_ASN(rate)            ASN_BIT(UCB1BR0,(rate))
#define USCI_B1BR1_ASN(rate)            ASN_BIT(UCB1BR1,(rate))

//UCAxSTAT / UCBxSTAT
#define USCI_A0STAT_GET(bitmap)         (UCA0STAT&(bitmap))
#define USCI_A0STAT_SET(bitmap)         SET_BIT(UCA0STAT,(bitmap))
#define USCI_A0STAT_CLR(bitmap)         CLR_BIT(UCA0STAT,(bitmap))

#define USCI_A1STAT_GET(bitmap)         (UCA1STAT&(bitmap))
#define USCI_A1STAT_SET(bitmap)         SET_BIT(UCA1STAT,(bitmap))
#define USCI_A1STAT_CLR(bitmap)         CLR_BIT(UCA1STAT,(bitmap))

#define USCI_B0STAT_GET(bitmap)         (UCB0STAT&(bitmap))
#define USCI_B0STAT_SET(bitmap)         SET_BIT(UCB0STAT,(bitmap))
#define USCI_B0STAT_CLR(bitmap)         CLR_BIT(UCB0STAT,(bitmap))

#define USCI_B1STAT_GET(bitmap)         (UCB1STAT&(bitmap))
#define USCI_B1STAT_SET(bitmap)         SET_BIT(UCB1STAT,(bitmap))
#define USCI_B1STAT_CLR(bitmap)         CLR_BIT(UCB1STAT,(bitmap))

//UCAxMCTL
#define USCI_A0MCTL_ASN(val)            ASN_BIT(UCA0MCTL,(val))
#define USCI_A1MCTL_ASN(val)            ASN_BIT(UCA1MCTL,(val))

//UCAxRXBUF / UCBxRXBUF
#define USCI_A0RXBUF_GET                (UCA0RXBUF)
#define USCI_A1RXBUF_GET                (UCA1RXBUF)
#define USCI_B0RXBUF_GET                (UCB0RXBUF)
#define USCI_B1RXBUF_GET                (UCB1RXBUF)

//UCBxTXBUF
#define USCI_A0TXBUF_ASN(val)            ASN_BIT(UCA0TXBUF,(val))
#define USCI_A1TXBUF_ASN(val)            ASN_BIT(UCA1TXBUF,(val))
#define USCI_B0TXBUF_ASN(val)            ASN_BIT(UCB0TXBUF,(val))
#define USCI_B1TXBUF_ASN(val)            ASN_BIT(UCB1TXBUF,(val))

//Interrupt Registers
#define USCI_0IE_SET(bitmap)            SET_BIT(IE2,(bitmap))
#define USCI_0IE_CLR(bitmap)            CLR_BIT(IE2,(bitmap))
#define USCI_0IFG_GET(bitmap)           (IFG2&(bitmap))
#define USCI_0IFG_CLR(bitmap)           CLR_BIT(IFG2,(bitmap))

#define USCI_1IE_SET(bitmap)            SET_BIT(UC1IE,(bitmap))
#define USCI_1IE_CLR(bitmap)            CLR_BIT(UC1IE,(bitmap))
#define USCI_1IFG_GET(bitmap)           (UC1IFG&(bitmap))
#define USCI_1IFG_CLR(bitmap)           CLR_BIT(UC1IFG,(bitmap))

/**************************IE2/UC1IE MODIFIER******************************/
#define USCI_BTXIE                      (0x08)
#define USCI_BRXIE                      (0x04)
#define USCI_ATXIE                      (0x02)
#define USCI_ARXIE                      (0x01)

/*************************IFG2/UC1IFG MODIFIER*****************************/
#define USCI_BTXIFG                     (0x08)
#define USCI_BRXIFG                     (0x04)
#define USCI_ATXIFG                     (0x02)
#define USCI_ARXIFG                     (0x01)

/***********************SELECTION CLASSIFICATIONS**************************/
#ifdef __USE_0_I2C_
#define __USE_USCI_A0_
#define __USE_USCI_B0_
#include "USCII2C.h"
#endif

#ifdef __USE_1_I2C_
#define __USE_USCI_A1_
#define __USE_USCI_B1_
#include "USCII2C.h"
#endif

#if defined(__USE_0_UART_)&&(!defined(__USE_USCI_A0_))
#define __USE_USCI_A0_
#include "USCIUART.h"
#endif

#if defined(__USE_1_UART_)&&(!defined(__USE_USCI_A1_))
#define __USE_USCI_A1_
#include "USCIUART.h"
#endif

#if defined(__USE_A0_SPI_)&&(!defined(__USE_USCI_A0_))&&(!defined(__USE_USCI_B0_))
#define __USE_USCI_A0_
#define __USE_USCI_B0_
#include "USCISPI.h"
#endif

#if defined(__USE_A1_SPI_)&&(!defined(__USE_USCI_A1_))&&(!defined(__USE_USCI_B1_))
#define __USE_USCI_A1_
#define __USE_USCI_B1_
#include "USCISPI.h"
#endif

#if defined(__USE_B0_SPI_)&&(!defined(__USE_USCI_B0_))&&(!defined(__USE_USCI_A0_))
#define __USE_USCI_B0_
#define __USE_USCI_A0_
#include "USCISPI.h"
#endif

#if defined(__USE_B1_SPI_)&&(!defined(__USE_USCI_B1_))&&(!defined(__USE_USCI_A1_))
#define __USE_USCI_B1_
#define __USE_USCI_A1_
#include "USCISPI.h"
#endif

#endif

//                            Ver. 0.3, all tested
//                            Hu Zhongsheng implemented, last update in 2013/8/14