#ifndef   _LCD12864_H_
#define   _LCD12864_H_
#include "msp430f2619.h"
#include "General.h"

/*
 * this is a LCD12864 head file
 * this head file is written for LCD12864 with ST77920 controller
 *    in parallel
 * these functions work with osc. freq. of 1Mhz as default
 * P8.0->RS   BIT0
 * P8.1->R/W  BIT1
 * P8.2->E    BIT2
 * P8.3->PSB  BIT3
 * P8.4->NC   BIT4
 * P8.5->RST  BIT5
 * P7  ->data
 */

/*********************GENERAL MACRO DEFINITIONS**************************/
#define LCD12864_RS1               SET_BIT(P8OUT,BIT0)
#define LCD12864_RS0               CLR_BIT(P8OUT,BIT0)
#define LCD12864_RW1               SET_BIT(P8OUT,BIT1)
#define LCD12864_RW0               CLR_BIT(P8OUT,BIT1)
#define LCD12864_EN1               SET_BIT(P8OUT,BIT2)
#define LCD12864_EN0               CLR_BIT(P8OUT,BIT2)
#define LCD12864_PSB1              SET_BIT(P8OUT,BIT3)
#define LCD12864_PSB0              CLR_BIT(P8OUT,BIT3)
#define LCD12864_RST1              SET_BIT(P8OUT,BIT5)
#define LCD12864_RST0              CLR_BIT(P8OUT,BIT5)
#define LCD12864_BASIC_DELAY(n)    NestedDelay(40,(n))

#define LCD12864_INIT              LCD12864InitLCD()
//Basic Instruction Set
#define LCD12864_BASIC_INSTR_SET   LCD12864WriteCom(0x30)
#define LCD12864_CLEAR_DDRAM       LCD12864WriteCom(0x01)
#define LCD12864_CLEAR_DDRAM_ADDR  LCD12864WriteCom(0x02)
#define LCD12864_DISP_ON           LCD12864WriteCom(0x0C)
#define LCD12864_DISP_OFF          LCD12864WriteCom(0x08)
#define LCD12864_CURSE(on,inverse) LCD12864WriteCom((0x08)|((on)<<1)|(inverse))
#define LCD12864_CURSE_LEFT        LCD12864WriteCom(0x10)
#define LCD12864_CURSE_RIGNT       LCD12864WriteCom(0x14)
#define LCD12864_SHIFT_LEFT        LCD12864WriteCom(0x18)
#define LCD12864_SHIFT_RIGHT       LCD12864WriteCom(0x1C)
#define LCD12864_SET_CGRAM_ADDR(addr)  LCD12864WriteCom((0x40)|(addr))

//Extend Instruction Set
#define LCD12864_EXT_INSTR_SET     LCD12864WriteCom(0x34)
#define LCD12864_HOLD              LCD12864WriteCom(0x01)
#define LCD12864_ADDR_SELECT(sr)   LCD12864WriteCom((0x02)|(sr))
#define LCD12864_INVERSE(line)     LCD12864WriteCom((0x4)|(line))
#define LCD12864_SLEEP             LCD12864WriteCom(0x08)
#define LCD12864_WAKE_UP           LCD12864WriteCom(0x0C)
#define LCD12864_SET_IRAM_ADDR(addr)   LCD12864WriteCom((0x40)|(addr))
#define LCD12864_SET_SCROLL_ADDR(addr) LCD12864WriteCom((0x40)|(addr))

//Graph-related
#define LCD12864_TURN_OFF_GRAPH    LCD12864WriteCom(0x34)
#define LCD12864_TURN_ON_GRAPH     LCD12864WriteCom(0x36)


/*******************GENERAL FUNCTION DEFINITIONS*************************/
/*
 * NOTE: call LCD12864InitLCD() first before calling any other functions
 */
void LCD12864InitLCD();
uchar LCD12864CheckBusy();
void LCD12864WriteCom(uchar LCD12864com);
void LCD12864WriteDat(uchar LCD12864dat);

/*
 * followings are character-ralated.
 * note:
 * ROW           ADDRESS
 *   1              0x80
 *   2              0x90
 *   3              0x88
 *   4              0x98
 */
/*
 * when using dis_char() to display Chinese characters,
 *    count one as two.
 * hint: use sprintf() function in stdio.h to format
 *    a string.
 */
void LCD12864DispChar(uchar addr,uchar *pt,uchar num);
void LCD12864SendChar(uchar addr,uchar *pt);

//Graph-related
//this function will clear the whole CGRAM
void LCD12864CleanGraphRam();
//this function moves dot info. from matrix to cgram
void LCD12864WriteGraphIn(unsigned* matrix,unsigned row,unsigned col,
                    unsigned grow,unsigned gcol);
//this function parses a function values that are less than 64, stored
//in fval, and generates a graph matrix
void LCD12864ParseFunction(uchar fval[128],unsigned matrix[16][32]);

#include "LCD12864.c"

#endif

//                            Ver. 0.93, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/20
