#ifndef __Algorithms_H_
#define __Algorithms_H_

/*
 * this is a head file of different algorithms.
 * since CPU in MSP430 is not a much powerful one.
 * using fast algorithms to handle necessary complecated calculations
 *     is important.
 * following algorithms are basically machine-oblivious.
 *
 * Algorithm(s) included:
 *     square root algorithms
 *     rounding up algorithms
 *     fast sin asymptotic algorithms
 *     template in C++ simulation
 */

/* a 32bit-long unsigned integer sqrt algorithm. */
uint SqrtInt(ulong M);
/*
 * a float invert sqrt algorithm.
 * to solve the sqrt, call this func as follow:
 *     store=1.0/InvSqrt(x);
 */
float InvSqrt (float x);
/*
 * sqrt root algorithm invented by Carmack, once used in QUAKE3.
 * not as precise as the upper one.
 */
float CarmackSqrt(float x);


/* bit hacks, round up to the nearest power of 2 */
uint UintRoundUp(uint size);
ulong UlongRoundUp(ulong size);


#ifndef PI
#define PI            (3.141592653)
#endif
/*
 * low precision sin asymptotic algorithm.
 * input range from 0 to 90.
 */
float SinLowP(int x);
/*
 * higher precision sin asymptotic algorithm.
 * input range from 0 to 90.
 */
float SinHighP(int x);
/*
 * super high precision and high speed asymptotic algorithm.
 * error < 0.001
 */
float SinParabola(float x);


/* template definition simulation */
/*
 * tricky and dangerous, but fast and funny!
 * use it like:
 *     SWAP_TEMPLATE(int)
 * NOTICE there is NO ';' following!
 * and call the function wherever you want:
 *     int a=10,b=20;
 *     //...statement...
 *     swap_int(&a,&b);
 * or:
 *     SWAP_CALL(int,a,b);
 */
#define SWAP_TEMPLATE(type)  \
void swap_##type (type * a, type * b){\
  type temp;\
  temp=*a;\
  *a=*b;\
  *b=temp;\
}
#define SWAP_CALL(type,a,b)   swap_##type (& (a), & (b))

#include "Algorithms.c"

#endif


//                            Ver. 0.1, all tested
//                            Hu Zhongsheng implemented, last update in 2013/9/6