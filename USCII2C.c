#include "USCII2C.h"

#ifdef __USE_0_I2C_
uchar USCII2C0Block=0;

void USCII2C0MasterInit(USCII2C_Addr ownAddr){
  USCI_B0CTL1_ASN(USCII2C_SMCLK+USCII2C_RESET);
  USCI_B0CTL0_ASN(USCII2C_MASTER+USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_0OA_ASN(ownAddr);
  USCI_B0BRW_ASN(12);
  SET_BIT(P3SEL,BIT1+BIT2);
  #ifndef __Alternative_0_I2C_
  USCII2C_0_RESET_NACK_HANDLER;
  USCII2C_0_RESET_STOP_HANDLER;
  USCII2C_0_RESET_START_HANDLER;
  USCII2C_0_RESET_LOST_HANDLER;
  USCII2C_0_RESET_TX_HANDLER;
  USCII2C_0_RESET_RX_HANDLER;
  #endif
  USCI_B0CTL1_CLR(USCII2C_RESET);
}

#ifndef __Alternative_0_I2C_
static uchar* USCII2C0sendFrom;
static uint USCII2C0sendLength;
static uint USCII2C0sendCount;

uint USCII2C0GetActualSendedNum(){
  return USCII2C0sendCount;
}

void USCII2C0ResetSendCount(){
  USCII2C0sendCount=0;
}

static uchar USCII2C0sendData;
void USCII2C0MasterSendByte(USCII2C_Addr slaveAddr,uchar data){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BRXIE);
  USCI_0IE_SET(USCI_BTXIE);
  USCII2C0sendData=data;
  USCII2C0sendFrom=&USCII2C0sendData;
  USCII2C0sendLength=1;
  USCII2C0sendCount=0;
  USCI_B0CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
}

void USCII2C0MasterSendByteBlock(USCII2C_Addr slaveAddr,uchar data){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BRXIE);
  USCI_0IE_SET(USCI_BTXIE);
  USCII2C0sendData=data;
  USCII2C0sendFrom=&USCII2C0sendData;
  USCII2C0sendLength=1;
  USCII2C0sendCount=0;
  USCII2C0Block=1;
  USCI_B0CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
  while(USCII2C0Block);
}

#endif

void USCII2C0MasterSend(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BRXIE);
  USCI_0IE_SET(USCI_BTXIE);
  #ifndef __Alternative_0_I2C_
  USCII2C0sendFrom=dataAddr;
  USCII2C0sendLength=length;
  USCII2C0sendCount=0;
  #endif
  USCI_B0CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
}

void USCII2C0MasterSendBlock(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BRXIE);
  USCI_0IE_SET(USCI_BTXIE);
  #ifndef __Alternative_0_I2C_
  USCII2C0sendFrom=dataAddr;
  USCII2C0sendLength=length;
  USCII2C0sendCount=0;
  #endif
  USCII2C0Block=1;
  USCI_B0CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
  while(USCII2C0Block);
}

#ifndef __Alternative_0_I2C_
static uchar* USCII2C0receiveTo;
static uint USCII2C0receiveLength;
static uint USCII2C0receiveCount;

uint USCII2C0GetActualReceivedNum(){
  return USCII2C0receiveCount;
}

void USCII2C0ResetReceiveCount(){
  USCII2C0receiveCount=0;
}

#endif

void USCII2C0MasterReceive(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BTXIE);
  USCI_0IE_SET(USCI_BRXIE);
  #ifndef __Alternative_0_I2C_
  USCII2C0receiveTo=dataAddr;
  USCII2C0receiveLength=length;
  USCII2C0receiveCount=0;
  #endif
  USCI_B0CTL1_CLR(USCII2C_TRANSMITTER);
  USCI_B0CTL1_SET(USCII2C_START);
}

void USCII2C0MasterReceiveBlock(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B0CTL1_GET(USCII2C_STOP));
  USCII2C_0SA_ASN(slaveAddr);
  USCII2C_0IE_ASN(USCII2C_NACK_IE);
  USCI_0IE_CLR(USCI_BTXIE);
  USCI_0IE_SET(USCI_BRXIE);
  #ifndef __Alternative_0_I2C_
  USCII2C0receiveTo=dataAddr;
  USCII2C0receiveLength=length;
  USCII2C0receiveCount=0;
  #endif
  USCII2C0Block=1;
  USCI_B0CTL1_CLR(USCII2C_TRANSMITTER);
  USCI_B0CTL1_SET(USCII2C_START);
  while(USCII2C0Block);
}

#ifndef __Alternative_0_I2C_
void USCII2C0SlaveInit(USCII2C_Addr ownAddr,
                       uchar* sendAddr,uint sendLen,
                       uchar* receiveAddr,uint receiveLen){
  USCI_B0CTL1_ASN(USCII2C_RESET);
  USCI_B0CTL0_ASN(USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_0OA_ASN(ownAddr);
  SET_BIT(P3SEL,BIT1+BIT2);
  USCII2C0sendFrom=sendAddr;
  USCII2C0sendLength=sendLen;
  USCII2C0sendCount=0;
  USCII2C0receiveTo=receiveAddr;
  USCII2C0receiveLength=receiveLen;
  USCII2C0receiveCount=0;
  USCII2C_0_RESET_NACK_HANDLER;
  USCII2C_0_RESET_STOP_HANDLER;
  USCII2C_0_RESET_START_HANDLER;
  USCII2C_0_RESET_LOST_HANDLER;
  USCII2C_0_RESET_TX_HANDLER;
  USCII2C_0_RESET_RX_HANDLER;
  USCI_B0CTL1_CLR(USCII2C_RESET);
  USCII2C_0IE_ASN(USCII2C_START_IE+USCII2C_STOP_IE);
  USCI_0IE_SET(USCI_BRXIE+USCI_BTXIE);
}

#else

void USCII2C0SlaveInit(USCII2C_Addr ownAddr){
  USCI_B0CTL1_ASN(USCII2C_RESET);
  USCI_B0CTL0_ASN(USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_0OA_ASN(ownAddr);
  SET_BIT(P3SEL,BIT1+BIT2);
  USCI_B0CTL1_CLR(USCII2C_RESET);
  USCII2C_0IE_ASN(USCII2C_START_IE+USCII2C_STOP_IE);
  USCI_0IE_SET(USCI_BRXIE+USCI_BTXIE);
}
#endif

#ifndef __Alternative_0_I2C_

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCII2C0StateISR(){
  _DINT();
  uchar res=0;
  if (USCI_B0STAT_GET(USCII2C_NACK_IFG)){
    USCI_B0CTL1_SET(USCII2C_STOP);
    USCI_B0STAT_CLR(USCII2C_NACK_IFG);
    res=(*USCII2C0NackHandler)();
  }else if (USCI_B0STAT_GET(USCII2C_STOP_IFG)){
    USCI_B0STAT_CLR(USCII2C_STOP_IFG);
    res=(*USCII2C0StopHandler)();
  }else if (USCI_B0STAT_GET(USCII2C_START_IFG)){
    USCI_B0STAT_CLR(USCII2C_START_IFG);
    res=(*USCII2C0StartHandler)();
  }else if (USCI_B0STAT_GET(USCII2C_LOST_IFG)){
    USCI_B0STAT_CLR(USCII2C_LOST_IFG);
    res=(*USCII2C0LostHandler)();
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCII2C0TRXISR(){
  _DINT();
  uchar res=0;
  if (USCI_B0CTL0_GET(USCII2C_MASTER)){
    if (USCI_0IFG_GET(USCI_BTXIFG)){
      if (USCII2C0sendCount<USCII2C0sendLength){
        USCI_B0TXBUF_ASN(USCII2C0sendFrom[USCII2C0sendCount]);
        USCII2C0sendCount++;
      }else {
        USCI_B0CTL1_SET(USCII2C_STOP);
        USCII2C0Block=0;
        USCI_0IFG_CLR(USCI_BTXIFG);
        res=(*USCII2C0TXHandler)();
      }
    }else if (USCI_0IFG_GET(USCI_BRXIFG)){
      if (USCII2C0receiveCount+1<USCII2C0receiveLength){
        USCII2C0receiveTo[USCII2C0receiveCount]=USCI_B0RXBUF_GET;
        if (USCII2C0receiveCount+2==USCII2C0receiveLength){
          USCI_B0CTL1_SET(USCII2C_STOP);
        }
        USCII2C0receiveCount++;
      }else{
        USCII2C0receiveTo[USCII2C0receiveCount]=USCI_B0RXBUF_GET;
        USCII2C0Block=0;
        res=(*USCII2C0RXHandler)();
      }
    }
  }else {
    if (USCI_0IFG_GET(USCI_BTXIFG)){
      if (USCII2C0sendCount<USCII2C0sendLength){
        USCI_B0TXBUF_ASN(USCII2C0sendFrom[USCII2C0sendCount]);
        USCII2C0sendCount++;
        if (USCII2C0sendCount==USCII2C0sendLength)
          USCII2C0sendCount=0;
      }else{
        USCI_0IFG_CLR(USCI_BTXIFG);
      }
      res=(*USCII2C0TXHandler)();
    }else if (USCI_0IFG_GET(USCI_BRXIFG)){
      if (USCII2C0receiveCount<USCII2C0receiveLength){
        USCII2C0receiveTo[USCII2C0receiveCount]=USCI_B0RXBUF_GET;
        USCII2C0receiveCount++;
        if (USCII2C0receiveCount==USCII2C0receiveLength)
          USCII2C0receiveCount=0;
      }else {
        USCI_0IFG_CLR(USCI_BRXIFG);
      }
      res=(*USCII2C0RXHandler)();
    }
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif

#endif

#ifdef __USE_1_I2C_

uchar USCII2C1Block=0;

void USCII2C1MasterInit(USCII2C_Addr ownAddr){
  USCI_B1CTL1_ASN(USCII2C_SMCLK+USCII2C_RESET);
  USCI_B1CTL0_ASN(USCII2C_MASTER+USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_1OA_ASN(ownAddr);
  USCI_B1BRW_ASN(12);
  SET_BIT(P5SEL,BIT1+BIT2);
  #ifndef __Alternative_1_I2C_
  USCII2C_1_RESET_NACK_HANDLER;
  USCII2C_1_RESET_STOP_HANDLER;
  USCII2C_1_RESET_START_HANDLER;
  USCII2C_1_RESET_LOST_HANDLER;
  USCII2C_1_RESET_TX_HANDLER;
  USCII2C_1_RESET_RX_HANDLER;
  #endif
  USCI_B1CTL1_CLR(USCII2C_RESET);
}

#ifndef __Alternative_1_I2C_
static uchar* USCII2C1sendFrom;
static uint USCII2C1sendLength;
static uint USCII2C1sendCount;

uint USCII2C1GetActualSendedNum(){
  return USCII2C1sendCount;
}

void USCII2C1ResetSendCount(){
  USCII2C1sendCount=0;
}

static uchar USCII2C1sendData;
void USCII2C1MasterSendByte(USCII2C_Addr slaveAddr,uchar data){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BRXIE);
  USCI_1IE_SET(USCI_BTXIE);
  USCII2C1sendData=data;
  USCII2C1sendFrom=&USCII2C1sendData;
  USCII2C1sendLength=1;
  USCII2C1sendCount=0;
  USCI_B1CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
}

void USCII2C1MasterSendByteBlock(USCII2C_Addr slaveAddr,uchar data){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BRXIE);
  USCI_1IE_SET(USCI_BTXIE);
  USCII2C1sendData=data;
  USCII2C1sendFrom=&USCII2C1sendData;
  USCII2C1sendLength=1;
  USCII2C1sendCount=0;
  USCII2C1Block=1;
  USCI_B1CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
  while(USCII2C1Block);
}

#endif

void USCII2C1MasterSend(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BRXIE);
  USCI_1IE_SET(USCI_BTXIE);
  #ifndef __Alternative_1_I2C_
  USCII2C1sendFrom=dataAddr;
  USCII2C1sendLength=length;
  USCII2C1sendCount=0;
  #endif
  USCI_B1CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
}

void USCII2C1MasterSendBlock(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_LOST_IE+USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BRXIE);
  USCI_1IE_SET(USCI_BTXIE);
  #ifndef __Alternative_1_I2C_
  USCII2C1sendFrom=dataAddr;
  USCII2C1sendLength=length;
  USCII2C1sendCount=0;
  #endif
  USCII2C1Block=1;
  USCI_B1CTL1_SET(USCII2C_TRANSMITTER+USCII2C_START);
  while(USCII2C1Block);
}

#ifndef __Alternative_1_I2C_
static uchar* USCII2C1receiveTo;
static uint USCII2C1receiveLength;
static uint USCII2C1receiveCount;

uint USCII2C1GetActualReceivedNum(){
  return USCII2C1receiveCount;
}

void USCII2C1ResetReceiveCount(){
  USCII2C1receiveCount=0;
}

#endif

void USCII2C1MasterReceive(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BTXIE);
  USCI_1IE_SET(USCI_BRXIE);
  #ifndef __Alternative_1_I2C_
  USCII2C1receiveTo=dataAddr;
  USCII2C1receiveLength=length;
  USCII2C1receiveCount=0;
  #endif
  USCI_B1CTL1_CLR(USCII2C_TRANSMITTER);
  USCI_B1CTL1_SET(USCII2C_START);
}

void USCII2C1MasterReceive(USCII2C_Addr slaveAddr,uchar* dataAddr,uint length){
  while(USCI_B1CTL1_GET(USCII2C_STOP));
  USCII2C_1SA_ASN(slaveAddr);
  USCII2C_1IE_ASN(USCII2C_NACK_IE);
  USCI_1IE_CLR(USCI_BTXIE);
  USCI_1IE_SET(USCI_BRXIE);
  #ifndef __Alternative_1_I2C_
  USCII2C1receiveTo=dataAddr;
  USCII2C1receiveLength=length;
  USCII2C1receiveCount=0;
  #endif
  USCII2C1Block=1;
  USCI_B1CTL1_CLR(USCII2C_TRANSMITTER);
  USCI_B1CTL1_SET(USCII2C_START);
  while(USCII2C1Block);
}

#ifndef __Alternative_1_I2C_
void USCII2C1SlaveInit(USCII2C_Addr ownAddr,
                       uchar* sendAddr,uint sendLen,
                       uchar* receiveAddr,uint receiveLen){
  USCI_B1CTL1_ASN(USCII2C_RESET);
  USCI_B1CTL0_ASN(USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_1OA_ASN(ownAddr);
  SET_BIT(P5SEL,BIT1+BIT2);
  USCII2C1sendFrom=sendAddr;
  USCII2C1sendLength=sendLen;
  USCII2C1sendCount=0;
  USCII2C1receiveTo=receiveAddr;
  USCII2C1receiveLength=receiveLen;
  USCII2C1receiveCount=0;
  USCII2C_1_RESET_NACK_HANDLER;
  USCII2C_1_RESET_STOP_HANDLER;
  USCII2C_1_RESET_START_HANDLER;
  USCII2C_1_RESET_LOST_HANDLER;
  USCII2C_1_RESET_TX_HANDLER;
  USCII2C_1_RESET_RX_HANDLER;
  USCI_B1CTL1_CLR(USCII2C_RESET);
  USCII2C_1IE_ASN(USCII2C_START_IE+USCII2C_STOP_IE);
  USCI_1IE_SET(USCI_BRXIE+USCI_BTXIE);
}

#else

void USCII2C1SlaveInit(USCII2C_Addr ownAddr){
  USCI_B1CTL1_ASN(USCII2C_RESET);
  USCI_B1CTL0_ASN(USCII2C_I2C_MODE+USCII2C_SYNC);
  USCII2C_1OA_ASN(ownAddr);
  SET_BIT(P5SEL,BIT1+BIT2);
  USCI_B1CTL1_CLR(USCII2C_RESET);
  USCII2C_1IE_ASN(USCII2C_START_IE+USCII2C_STOP_IE);
  USCI_1IE_SET(USCI_BRXIE+USCI_BTXIE);
}
#endif

#ifndef __Alternative_1_I2C_

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCII2C1StateISR(){
  _DINT();
  uchar res=0;
  if (USCI_B1STAT_GET(USCII2C_NACK_IFG)){
    USCI_B1CTL1_SET(USCII2C_STOP);
    USCI_B1STAT_CLR(USCII2C_NACK_IFG);
    res=(*USCII2C1NackHandler)();
  }else if (USCI_B1STAT_GET(USCII2C_STOP_IFG)){
    USCI_B1STAT_CLR(USCII2C_STOP_IFG);
    res=(*USCII2C1StopHandler)();
  }else if (USCI_B1STAT_GET(USCII2C_START_IFG)){
    USCI_B1STAT_CLR(USCII2C_START_IFG);
    res=(*USCII2C1StartHandler)();
  }else if (USCI_B1STAT_GET(USCII2C_LOST_IFG)){
    USCI_B1STAT_CLR(USCII2C_LOST_IFG);
    res=(*USCII2C1LostHandler)();
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCII2C1TRXISR(){
  _DINT();
  uchar res=0;
  if (USCI_B1CTL0_GET(USCII2C_MASTER){
    if (USCI_1IFG_GET(USCI_BTXIFG)){
      if (USCII2C1sendCount<USCII2C1sendLength){
        USCI_B1TXBUF_ASN(USCII2C1sendFrom[USCII2C1sendCount]);
        USCII2C1sendCount++;
      }else {
        USCI_B1CTL1_SET(USCII2C_STOP);
        USCII2C1Block=0;
        USCI_1IFG_CLR(USCI_BTXIFG);
        res=(*USCII2C1TXHandler)();
      }
    }else if (USCI_1IFG_GET(USCI_BRXIFG)){
      if (USCII2C1receiveCount+1<USCII2C1receiveLength){
        USCII2C1receiveTo[USCII2C1receiveCount]=USCI_B1RXBUF_GET;
        if (USCII2C1receiveCount+2==USCII2C1receiveLength){
          USCI_B1CTL1_SET(USCII2C_STOP);
        }
        USCII2C1receiveCount++;
      }else{
        USCII2C1receiveTo[USCII2C1receiveCount]=USCI_B1RXBUF_GET;
        USCII2C1Block=0;
        res=(*USCII2C1RXHandler)();
      }
    }
  }else {
    if (USCI_1IFG_GET(USCI_BTXIFG)){
      if (USCII2C1sendCount<USCII2C1sendLength){
        USCI_B1TXBUF_ASN(USCII2C1sendFrom[USCII2C1sendCount]);
        USCII2C1sendCount++;
        if (USCII2C1sendCount==USCII2C1sendLength)
          USCII2C1sendCount=0;
      }else{
        USCI_1IFG_CLR(USCI_BTXIFG);
      }
      res=(*USCII2C1TXHandler)();
    }else if (USCI_1IFG_GET(USCI_BRXIFG)){
      if (USCII2C1receiveCount<USCII2C1receiveLength){
        USCII2C1receiveTo[USCII2C1receiveCount]=USCI_B1RXBUF_GET;
        USCII2C1receiveCount++;
        if (USCII2C1receiveCount==USCII2C1receiveLength)
          USCII2C1receiveCount=0;
      }else {
        USCI_1IFG_CLR(USCI_BRXIFG);
      }
      res=(*USCII2C1RXHandler)();
    }
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif

#endif
