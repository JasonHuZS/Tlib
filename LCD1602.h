#ifndef _LCD1602_H_
#define _LCD1602_H_
#include "General.h"

/*
 *This is the head file for LCD1602
 *This head file includes functions of settings and display of LCD1602 as below:
 *	LCD1602WriteInstruction(com)	used to write controling instruction into LCD1602
 *	LCD1602WriteData(da)		used to write displaying data into LCD1602
 *This head file also includes a common method of initializing LCD1602 as below:
 *	LCD1602Init()		        (see details in LCD1602.c)
 *This head file also includes several common functions with LCD1602 as below:
 *	LCD1602_CLR_SCREEN		Clear the screen
 *	LCD1602_DIS1_CHAR(L,C)		Write Character C at location L(1~16) in row one
 *	LCD1602_DIS2_CHAR(L,C)		Write Character C at location L(1~16) in row two
 *      LCD1602_DIS1_STRING(L,S,N)	Write String S with length N at location L(1~16) in row one
 *      LCD1602_DIS2_STRING(L,S,N)	Write String S with length N at location L(1~16) in row two
 *Ports are used as below:
 *	P8.0-->RS
 *	P8.1-->R/W
 *	P8.2-->E
 *	P7  -->DATA					
 */

/*
 *Several commonly used instructions for LCD1602 settings
 */
#define LCD1602_SCREEN_ON_CURSOR_ON_BLINK_ON	LCD1602WriteInstruction(0x0F); delay(120);
#define LCD1602_SCREEN_ON_CURSOR_ON_BLINK_OFF	LCD1602WriteInstruction(0x0E); delay(120);
#define LCD1602_SCREEN_ON_CURSOR_OFF		LCD1602WriteInstruction(0x0C); delay(120);
#define LCD1602_CURSOR_SHIFT_ON_CHAR_SHIFT_OFF	LCD1602WriteInstruction(0x06); delay(120);
#define LCD1602_CURSOR_SHIFT_ON_CHAR_SHIFT_ON	LCD1602WriteInstruction(0x07); delay(120);
#define LCD1602_CURSOR_SHIFT_OFF_CHAR_SHIFT_ON	LCD1602WriteInstruction(0x05); delay(120);
#define LCD1602_CURSOR_SHIFT_OFF_CHAR_SHIFT_OFF	LCD1602WriteInstruction(0x04); delay(120);
#define LCD1602_8BITS_TWO_ROWS			LCD1602WriteInstruction(0x38); delay(120);

/*
 *Several commonly used instructions for LCD1602 displaying
 */
#define LCD1602_CLR_SCREEN	LCD1602WriteInstruction(0x01); delay(300)
#define LCD1602_DIS1_CHAR(L,C)	LCD1602WriteInstruction(((L)-1)|0x80); LCD1602WriteData((C))
#define LCD1602_DIS2_CHAR(L,C)	LCD1602WriteInstruction(((L)-1)|0xc0); LCD1602WriteData((C))
#define LCD1602_DIS1_STRING(L,S,N)	LCD1602DispString(((L)-1)|0x80,S,N)
#define LCD1602_DIS2_STRING(L,S,N)	LCD1602DispString(((L)-1)|0xc0,S,N)

/*
 *Several definition used in LCD1602 ports setting
 */
#define LCD1602_OUT 	P8DIR|=BIT0+BIT1+BIT2
#define LCD1602_EN1 	P8OUT|=BIT2
#define LCD1602_EN0 	P8OUT&=~BIT2
#define LCD1602_RW1 	P8OUT|=BIT1
#define LCD1602_RW0 	P8OUT&=~BIT1
#define LCD1602_RS1 	P8OUT|=BIT0
#define LCD1602_RS0 	P8OUT&=~BIT0

/*
 *Funtions for initializing, instruction writing and data writing
 */
void LCD1602WriteInstruction(uchar com);
void LCD1602WriteData(uchar da);
void LCD1602Init();
void LCD1602DispString(uchar addr,uchar *pt,uchar num);

#include "LCD1602.c"

#endif

//							Ver 0.2
//							Yongchen Jiang coded, 10th Jul 2013