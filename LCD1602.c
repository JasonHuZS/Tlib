#include "LCD1602.h"

void LCD1602WriteInstruction(uchar com)
{
  LCD1602_OUT;
  P7DIR=0xff;
  P7OUT=com;
  LCD1602_RW0;
  LCD1602_RS0;
  delay(100);
  LCD1602_EN1;
  delay(100);
  LCD1602_EN0;
  delay(100);
}

void LCD1602WriteData(uchar da)
{
  LCD1602_OUT;
  P7DIR=0xff;
  P7OUT=da;
  LCD1602_RW0;
  LCD1602_RS1;
  delay(100);
  LCD1602_EN1;
  delay(100);
  LCD1602_EN0;
  delay(100);
}

void LCD1602DispString(uchar addr,uchar *pt,uchar num)
{
  uchar i;
  LCD1602WriteInstruction(addr);
  for (i=0;i<num;i++)
    LCD1602WriteData(*pt++);
}

void LCD1602Init()
{
  LCD1602WriteInstruction(0x38);		//8位总线,双行显示
  DELAY_10MS;
  LCD1602WriteInstruction(0x0C);		//开显示,无光标
  DELAY_10MS;
  LCD1602WriteInstruction(0x06);		//光标右移,文字不右移
  DELAY_10MS;
  LCD1602WriteInstruction(0x01);		//清除显示
  DELAY_100MS;
}