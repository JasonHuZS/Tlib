#ifndef __FFT_H_
#define __FFT_H_
#include "Complex.h"

/*
 * this head file includes two kinds of implementations.
 * one is recursive fft and the other one is iterative fft.
 * iterative one is recommanded.
 * to choose one, please define:
 *  #define __USE_ITERATIVE_FFT_
 * before include this head file to choose the coresponding package.
 */
/*
 * NOTICE:
 * these FFT implementations can only take input signals with length of 128.
 * more general ones are about to be implemented.
 */


#ifdef __USE_RECURSIVE_FFT_

#include "RecursiveFFT.h"

#else

#ifdef  __USE_ITERATIVE_FFT_

#include "IterativeFFT.h"

#endif

#endif

#endif
