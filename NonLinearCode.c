#include "NonLinearCode.h"

uchar ALawEncoding(uint sigIn){
  uchar op=0;
  if (sigIn&0x400)      //8
    op=0x70+((sigIn>>6)&0xf);
  else if (sigIn&0x200) //7
    op=0x60+((sigIn>>5)&0xf);
  else if (sigIn&0x100) //6
    op=0x50+((sigIn>>4)&0xf);
  else if (sigIn&0x80) //5
    op=0x40+((sigIn>>3)&0xf);
  else if (sigIn&0x40)  //4
    op=0x30+((sigIn>>2)&0xf);
  else if (sigIn&0x20)  //3
    op=0x20+((sigIn>>1)&0xf);
  else if (sigIn&0x10)  //2
    op=op=0x10+(sigIn&0xf);
  else                  //1
    op=(sigIn&0xf);
  op+=(sigIn&0x800)>>4;
  return op;
}

uint ALawDecoding(uchar sigIn){
  uint op=0;
  switch ((sigIn>>4)&0x7){
  case 0:
    op=(0xf&sigIn)+((sigIn&0x80)<<4);break;
  case 1:
    op=0x10+(0xf&sigIn)+((sigIn&0x80)<<4);break;
  case 2:
    op=0x21+((0xf&sigIn)<<1)+((sigIn&0x80)<<4);break;
  case 3:
    op=0x42+((0xf&sigIn)<<2)+((sigIn&0x80)<<4);break;
  case 4:
    op=0x84+((0xf&sigIn)<<3)+((sigIn&0x80)<<4);break;
  case 5:
    op=0x108+((0xf&sigIn)<<4)+((sigIn&0x80)<<4);break;
  case 6:
    op=0x210+((0xf&sigIn)<<5)+((sigIn&0x80)<<4);break;
  case 7:
    op=0x420+((0xf&sigIn)<<6)+((sigIn&0x80)<<4);break;
  }
  return op;
}