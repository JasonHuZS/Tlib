#ifndef _DMA_H_
#define _DMA_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a DMA headfile.
 * this head file encapsulates all features of DMA in msp430.
 * and it provides some kinds of easy usage of DMA to make it nicer to use.
 * to understand how to use this lib, read the annotation in the context.
 */

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * handler may be not as efficient as required.
 * if efficiency is required, write a ISR function yourself.
 * before doing that, use macro directive to disinclude existed implementation:
 *     #define __Alternative_DMA_
 *     //...statement...
 *     #pragma vector=DMA_VECTOR
 *     __interrupt void MyDefinedDMAISR(){}
 */
#ifndef __Alternative_DMA_
/*
 * they are the handlers of: 
 * DMAisrHandler[0] <- DMA Channel 0 Interrupt
 * DMAisrHandler[1] <- DMA Channel 1 Interrupt
 * DMAisrHandler[2] <- DMA Channel 2 Interrupt
 * respectively.
 */
uchar (*DMAisrHandler[3])();

#define DMA_SET_HANDLER(ind,handler)  DMAisrHandler[(ind)]=(handler)
#define DMA_RESET_HANDLER(ind)        DMAisrHandler[(ind)]=ucblank

#pragma vector=DMA_VECTOR
__interrupt void DMAISR();

#endif

/***********************GENERAL MACRO DEFINITIONS**************************/
//Control Registers
#define DMA_SET_CTL0(bitmap)          SET_BIT(DMACTL0,(bitmap))
#define DMA_CLR_CTL0(bitmap)          CLR_BIT(DMACTL0,(bitmap))
#define DMA_ASN_CTL0(bitmap)          ASN_BIT(DMACTL0,(bitmap))

#define DMA_SET_CTL1(bitmap)          SET_BIT(DMACTL1,(bitmap))
#define DMA_CLR_CTL1(bitmap)          CLR_BIT(DMACTL1,(bitmap))
#define DMA_ASN_CTL1(bitmap)          ASN_BIT(DMACTL1,(bitmap))

//Channel Registers
//channel 0
#define DMA_SET_0CTL(bitmap)          SET_BIT(DMA0CTL,(bitmap))
#define DMA_CLR_0CTL(bitmap)          CLR_BIT(DMA0CTL,(bitmap))
#define DMA_ASN_0CTL(bitmap)          ASN_BIT(DMA0CTL,(bitmap))

#define DMA_SET_0SA(addr)             SET_BIT(DMA0SA,(addr))
#define DMA_CLR_0SA(addr)             CLR_BIT(DMA0SA,(addr))
#define DMA_ASN_0SA(addr)             ASN_BIT(DMA0SA,(addr))

#define DMA_SET_0DA(addr)             SET_BIT(DMA0DA,(addr))
#define DMA_CLR_0DA(addr)             CLR_BIT(DMA0DA,(addr))
#define DMA_ASN_0DA(addr)             ASN_BIT(DMA0DA,(addr))

#define DMA_SET_0SZ(size)             SET_BIT(DMA0SZ,(size))
#define DMA_CLR_0SZ(size)             CLR_BIT(DMA0SZ,(size))
#define DMA_ASN_0SZ(size)             ASN_BIT(DMA0SZ,(size))

//channel 1
#define DMA_SET_1CTL(bitmap)          SET_BIT(DMA1CTL,(bitmap))
#define DMA_CLR_1CTL(bitmap)          CLR_BIT(DMA1CTL,(bitmap))
#define DMA_ASN_1CTL(bitmap)          ASN_BIT(DMA1CTL,(bitmap))

#define DMA_SET_1SA(addr)             SET_BIT(DMA1SA,(addr))
#define DMA_CLR_1SA(addr)             CLR_BIT(DMA1SA,(addr))
#define DMA_ASN_1SA(addr)             ASN_BIT(DMA1SA,(addr))

#define DMA_SET_1DA(addr)             SET_BIT(DMA1DA,(addr))
#define DMA_CLR_1DA(addr)             CLR_BIT(DMA1DA,(addr))
#define DMA_ASN_1DA(addr)             ASN_BIT(DMA1DA,(addr))

#define DMA_SET_1SZ(size)             SET_BIT(DMA1SZ,(size))
#define DMA_CLR_1SZ(size)             CLR_BIT(DMA1SZ,(size))
#define DMA_ASN_1SZ(size)             ASN_BIT(DMA1SZ,(size))

//channel 2
#define DMA_SET_2CTL(bitmap)          SET_BIT(DMA2CTL,(bitmap))
#define DMA_CLR_2CTL(bitmap)          CLR_BIT(DMA2CTL,(bitmap))
#define DMA_ASN_2CTL(bitmap)          ASN_BIT(DMA2CTL,(bitmap))

#define DMA_SET_2SA(addr)             SET_BIT(DMA2SA,(addr))
#define DMA_CLR_2SA(addr)             CLR_BIT(DMA2SA,(addr))
#define DMA_ASN_2SA(addr)             ASN_BIT(DMA2SA,(addr))

#define DMA_SET_2DA(addr)             SET_BIT(DMA2DA,(addr))
#define DMA_CLR_2DA(addr)             CLR_BIT(DMA2DA,(addr))
#define DMA_ASN_2DA(addr)             ASN_BIT(DMA2DA,(addr))

#define DMA_SET_2SZ(size)             SET_BIT(DMA2SZ,(size))
#define DMA_CLR_2SZ(size)             CLR_BIT(DMA2SZ,(size))
#define DMA_ASN_2SZ(size)             ASN_BIT(DMA2SZ,(size))

/***********************DMACTL0 MACRO DEFINITIONS**************************/
/*
 * ind    <- channel index 0-2
 * bitpos <- choose from the following macros
 */
#define DMA_SET_DMATSEL(ind,bitpos)   DMA_SET_CTL0(((bitpos)<<((ind)<<2)))
#define DMA_CLR_DMATSEL(ind)          DMA_CLR_CTL0(((0x000F)<<((ind)<<2)))
//DMA Trigger Select
typedef uint DMA_Trigger;
#define DMA_SOFTWARE                  (0x0000)
#define DMA_TACCIFG0                  (0x0007)
#define DMA_TACCIFG2                  (0x0001)
#define DMA_TBCCIFG0                  (0x0008)
#define DMA_TBCCIFG2                  (0x0002)
#define DMA_UCA0RXIFG                 (0x0003)
#define DMA_UCA0TXIFG                 (0x0004)
#define DMA_UCA1RXIFG                 (0x0009)
#define DMA_UCA1TXIFG                 (0x000A)
#define DMA_UCB0RXIFG                 (0x000C)
#define DMA_UCB0TXIFG                 (0x000D)
#define DMA_DAC12IFG                  (0x0005)
#define DMA_ADC12IFG                  (0x0006)
#define DMA_DMAIFG                    (0x000E)
#define DMA_DMAE0                     (0x000F)
#define DMA_MULTIPLIER                (0x000B)

/***********************DMACTL1 MACRO DEFINITIONS**************************/
#define DMA_FETCH_ON_NEXT             DMA_SET_CTL1(0x0004)
#define DMA_FETCH_IMMEDIATE           DMA_CLR_CTL1(0x0004)
#define DMA_ROUND_ROBIN               DMA_SET_CTL1(0x0002)
#define DMA_DEFAULT_ROBIN             DMA_CLR_CTL1(0x0002)
#define DMA_NMI_ENABLE                DMA_SET_CTL1(0x0001)
#define DMA_NMI_DISABLE               DMA_CLR_CTL1(0x0001)

/***********************DMAxCTL MACRO DEFINITIONS**************************/
//DMA Transfer Mode
typedef uint DMA_TransferM;
#define DMA_DTBIT                     (0x7000)
#define DMA_SINGLE                    (0x0000)
#define DMA_BLOCK                     (0x1000)
#define DMA_BURST_BLOCK               (0x3000)
#define DMA_REPEATED_SINGLE           (0x4000)
#define DMA_REPEATED_BLOCK            (0x5000)
#define DMA_REPEATED_BURST_BlOCK      (0x7000)

//DMA Destination Increment
#define DMA_DSTINCRBIT                (0x0C00)
#define DMA_DST_UNCHANGED             (0x0000)
#define DMA_DST_DECREMENT             (0x0400)
#define DMA_DST_INCREMENT             (0x0C00)

//DMA Source Increment
#define DMA_SRCINCRBIT                (0x0300)
#define DMA_SRC_UNCHANGED             (0x0000)
#define DMA_SRC_DECREMENT             (0x0200)
#define DMA_SRC_INCREMENT             (0x0300)

//DMA Increments
typedef uint DMA_Incr;
#define DMA_UNCHANGED                 (0x0000)
#define DMA_DECREMENT                 (0x0200)
#define DMA_INCREMENT                 (0x0300)

//Data Length
#define DMA_DST_BYTE                  (0x0080)
#define DMA_DST_WORD                  (0x0000)
#define DMA_SRC_BYTE                  (0x0040)
#define DMA_SRC_WORD                  (0x0000)

//Interrupt&Trigger Control
#define DMA_EDGE_SENSE                (0x0000)
#define DMA_LEVEL_SENSE               (0x0020)
#define DMA_ENABLE                    (0x0010)
#define DMA_DISABLE                   (0x0000)
#define DMA_IFG                       (0x0008)
#define DMA_IE                        (0x0004)
#define DMA_ABORT                     (0x0002)
#define DMA_REQ                       (0x0001)

#define DMA_CH0_EDGE_SENSE            DMA_CLR_0CTL(0x0020)
#define DMA_CH0_LEVEL_SENSE           DMA_SET_0CTL(0x0020)
#define DMA_CH0_ENABLE                DMA_SET_0CTL(0x0010)
#define DMA_CH0_DISABLE               DMA_CLR_0CTL(0x0010)
#define DMA_CH0_CLR_IFG               DMA_CLR_0CTL(0x0008)
#define DMA_CH0_SET_IE                DMA_SET_0CTL(0x0004)
#define DMA_CH0_CLR_IE                DMA_CLR_0CTL(0x0004)
#define DMA_CH0_ABORT                 (DMA0CTL&0x0002)
#define DMA_CH0_SET_REQ               DMA_SET_0CTL(0x0001)

#define DMA_CH1_EDGE_SENSE            DMA_CLR_1CTL(0x0020)
#define DMA_CH1_LEVEL_SENSE           DMA_SET_1CTL(0x0020)
#define DMA_CH1_ENABLE                DMA_SET_1CTL(0x0010)
#define DMA_CH1_DISABLE               DMA_CLR_1CTL(0x0010)
#define DMA_CH1_CLR_IFG               DMA_CLR_1CTL(0x0008)
#define DMA_CH1_SET_IE                DMA_SET_1CTL(0x0004)
#define DMA_CH1_CLR_IE                DMA_CLR_1CTL(0x0004)
#define DMA_CH1_ABORT                 (DMA1CTL&0x0002)
#define DMA_CH1_SET_REQ               DMA_SET_1CTL(0x0001)

#define DMA_CH2_EDGE_SENSE            DMA_CLR_2CTL(0x0020)
#define DMA_CH2_LEVEL_SENSE           DMA_SET_2CTL(0x0020)
#define DMA_CH2_ENABLE                DMA_SET_2CTL(0x0010)
#define DMA_CH2_DISABLE               DMA_CLR_2CTL(0x0010)
#define DMA_CH2_CLR_IFG               DMA_CLR_2CTL(0x0008)
#define DMA_CH2_SET_IE                DMA_SET_2CTL(0x0004)
#define DMA_CH2_CLR_IE                DMA_CLR_2CTL(0x0004)
#define DMA_CH2_ABORT                 (DMA2CTL&0x0002)
#define DMA_CH2_SET_REQ               DMA_SET_2CTL(0x0001)

/************************DMAIV MACRO DEFINITIONS***************************/
#define DMA_GET_IV                    (DMAIV&0x000E)
#define DMA_IV_CH0                    (0x0002)
#define DMA_IV_CH1                    (0x0004)
#define DMA_IV_CH2                    (0x0006)

/***********************TRANSFER TYPE DEFINITIONS**************************/
typedef uint DMA_DataType;
#define DMA_CHAR                      (0x0040)
#define DMA_INTEGER                   (0x0000)
//if you choose FLOAT and LONG_INTEGER, make sure your srcType and dstType are the same!
//or something bad may happen and corrupt the program!
#define DMA_FLOAT                     (0x0001)
#define DMA_LONG_INTEGER              (0x0001)

/*******************INITIALIZATION PROCESS DEFINITIONS*********************/
/*
 * DO remember declare this to start DMA.
 * if you do not know whether DMA is started, always declare it.
 */
#define DMA_ENABLE_All                DMAEnableAll()
void DMAEnableAll();
void DMADisable(uchar channel);

/*
 * this INIT macro is not necessary. but for stability, but declaring it is a good choice.
 * default:
 * DMACTL1<-fetch on next instr., round-robin, NMI disabled
 * if you do not call this and DMACTL1 has not been modified, 
 *     the state of DMA will be:
 * DMACTL1<- fetch immediately, default piority, NMI disabled
 */
#define DMA_INIT                      DMA_SET_CTL1(0x0006)

/*Initialization Process
 * default:
 * DMAxCTL <- edge sensitive
 *
 * this function return the channel number of initialized channels.
 * if you have initialized 3 and continue to do so, the return value will be
 *     0 to warn you that.
 * so check the return value to know if the initialization is successful or not.
 */
uchar DMASet(DMA_Trigger trigger,DMA_TransferM trMode,             //channel initialization info.
             void* srcAddr,DMA_DataType srcType,DMA_Incr srcDir,   //src info
             void* dstAddr,DMA_DataType dstType,DMA_Incr dstDir,   //dst info
             uint length);
/*
 * set the next channel number to initialize.
 * chNum should be 0,1 or 2.
 * the channel counter would increase automatically after DMASet() called,
 *     so use it only when you want to re-initialize a used channel.
 */
inline void DMASetChannelCounter(uchar chNum);

/**********************Instant Initialization Macros***********************/
/*
 * transfer data from srcAddr to dstAddr repeatedly, triggered by trigger.
 * DataType should be DMA_CHAR or DMA_INTEGER only.
 */
uchar DMASingleToSingle(DMA_Trigger trigger,       //channel initialization info.
                        void* srcAddr,DMA_DataType srcType,   //src info
                        void* dstAddr,DMA_DataType dstType);  //dst info

/*
 * transfer multiply data from array begins with srcAddr 
 *     to dstAddr only once, triggered by trigger.
 * srcDir is INCREMENT as default.
 * DataType should be DMA_CHAR or DMA_INTEGER only.
 */
uchar DMAMulToSingle(DMA_Trigger trigger,       //channel initialization info.
                     void* srcAddr,DMA_DataType srcType,   //src info
                     void* dstAddr,DMA_DataType dstType,   //dst info
                     uint length);

/*
 * transfer multiply data from srcAddr to array begins with dstAddr
 *      only once, triggered by trigger.
 * dstDir is INCREMENT as default.
 * DataType should be DMA_CHAR or DMA_INTEGER only.
 */
uchar DMASingleToMul(DMA_Trigger trigger,       //channel initialization info.
                     void* srcAddr,DMA_DataType srcType,   //src info
                     void* dstAddr,DMA_DataType dstType,   //dst info
                     uint length);

/*
 * transfer multiply data from array begins with srcAddr 
 *      to array begins with dstAddr only once, triggered by trigger.
 * srcDir and dstDir are INCREMENT as default.
 * DataType could be DMA_CHAR, DMA_INTEGER, DMA_FLOAT or DMA_LONG_INTEGER.
 * if you choose FLOAT or LONG_INTEGER, length is right the length of the src array.
 */
uchar DMAMulToMul(DMA_Trigger trigger,       //channel initialization info.
                  void* srcAddr,DMA_DataType srcType,   //src info
                  void* dstAddr,DMA_DataType dstType,   //dst info
                  uint length);

#include "DMA.c"

#endif

//                            Ver. 0.3, all untested
//                            Hu Zhongsheng implemented, last update in 2013/7/31
