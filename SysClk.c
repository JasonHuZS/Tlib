#include "SysClk.h"

// freq. of DCO
static uint SysClkDCOFreq=1000;

extern uint SysClkforDelay=1;

void SysClkInitXT2()                      //initialize XT2 osc.
{
  uchar i;
  BCSCTL1 &= ~XT2OFF;
  do
  {
    IFG1&=~OFIFG;
    for (i=0;i<0xff;i++);
  }
  while ((IFG1&OFIFG)==OFIFG);
}

void SysClkMaxDCO()
{
  DCOCTL=0xe0;
  BCSCTL1=0x0f;
  SysClkDCOFreq=20000;
  SysClkforDelay=20;
}

void SysClkDCOConfig(uchar freq)
{
  uchar xt2=BCSCTL1 & 0x80;
  switch(freq)
  {
    case 1:  DCOCTL=CALDCO_1MHZ;  BCSCTL1=CALBC1_1MHZ & 0x7f+xt2;  
      SysClkDCOFreq=1000; SysClkforDelay=1; break;
    case 8:  DCOCTL=CALDCO_8MHZ;  BCSCTL1=CALBC1_8MHZ & 0x7f+xt2;  
    SysClkDCOFreq=8000; SysClkforDelay=8; break;
    case 12: DCOCTL=CALDCO_12MHZ; BCSCTL1=CALBC1_12MHZ & 0x7f+xt2; 
    SysClkDCOFreq=12000; SysClkforDelay=12; break;
    case 16: DCOCTL=CALDCO_16MHZ; BCSCTL1=CALBC1_16MHZ & 0x7f+xt2; 
    SysClkDCOFreq=16000;SysClkforDelay=16; break;
    default: DCOCTL=CALDCO_1MHZ;  BCSCTL1=CALBC1_1MHZ & 0x7f+xt2; 
    SysClkDCOFreq=1000; SysClkforDelay=1; break;
  }
}

void SysClkMClkConfig(uchar source, uchar div)
{
  switch(source)
  {
    case 'D': BCSCTL2&=0x3f; SysClkfreq=SysClkDCOFreq; break;
    case 'X': BCSCTL2&=0x3f; BCSCTL2|=SELM_2; BCSCTL3|=XT2S_2; 
	  SysClkfreq=SYSCLK_XT2_FREQ; SysClkforDelay=SYSCLK_XT2_FREQ/1000; break;
    default:  BCSCTL2&=0x3f; SysClkfreq=SysClkDCOFreq; break;
  }
  switch(div)
  {
    case 1:  BCSCTL2=(BCSCTL2 & 0xcf) + DIVM_0; break;
    case 2:  BCSCTL2=(BCSCTL2 & 0xcf) + DIVM_1; 
    SysClkfreq>>=1; SysClkforDelay>>=1; break;
    case 4:  BCSCTL2=(BCSCTL2 & 0xcf) + DIVM_2; 
    SysClkfreq>>=2; SysClkforDelay>>=2; break;
    case 8:  BCSCTL2=(BCSCTL2 & 0xcf) + DIVM_3; 
    SysClkfreq>>=3; SysClkforDelay>>=3; break;
    default: BCSCTL2=(BCSCTL2 & 0xcf) + DIVM_0; break;
  }
}

void SysClkSClkConfig(uchar source, uchar div)
{
  switch(source)
  {
    case 'D': BCSCTL2&=~SELS; SysClkfreqS=SysClkDCOFreq;break;
    case 'X': BCSCTL2|=SELS;  BCSCTL3|=XT2S_2; SysClkfreqS=SYSCLK_XT2_FREQ; break;
    default:  BCSCTL2&=~SELS; break;
  }
  switch(div)
  {
    case 1:  BCSCTL2=(BCSCTL2 & 0xf9) + DIVS_0; break;
    case 2:  BCSCTL2=(BCSCTL2 & 0xf9) + DIVS_1; SysClkfreqS>>=1; break;
    case 4:  BCSCTL2=(BCSCTL2 & 0xf9) + DIVS_2; SysClkfreqS>>=2; break;
    case 8:  BCSCTL2=(BCSCTL2 & 0xf9) + DIVS_3; SysClkfreqS>>=3; break;
    default: BCSCTL2=(BCSCTL2 & 0xf9) + DIVS_0; break;
  }
}
