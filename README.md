# Tlib
A library that is specifically designed for MSP430 series from TI, with some helpers for peripheral devices as well

This is a library that my classmates and I wrote back in 2013, when we were senior students at university, preparing a contest
for design and implementation of electronic devices. 

Since that context was funded by Texus Instruments, a.k.a TI, we were using MSP430 in the contest. Hence I organized all those
wanted to attend to the contest and responsible for software to write this library in order to simply the coding.

It was working very nicely, at least for me. The difference between software and hardware was shown huge to me. Modularity
is one of the best advantages compared to hardware and this ahead of time library is really came in handy in terms of 
accelerating the software implementations. 

I am currently planning to apply for university again, to I dug this code out again to try to familarize myself with the 
work I have done. Checking the code, I realized I was really naive back then, and I would no longer code like that. Nonetheless,
it remarks the path of my education and growth of experiences and capabilities.

One thing that is worth mentioning, is the library is designed to work with IAR embedded workbench, instead of CCS, which is a modified version of eclipse that TI developed.
