#ifndef _BasicMenuFramework_H_
#define _BasicMenuFramework_H_
#include "General.h"

/*
 * this is a particular head file of a basic implementation of menu framework.
 * it provides a simple and basic kind of implementation.
 * use it as long as it has already matched your request.
 * 
 * Introduction:
 * the typical steps of building a menu using this tool is like following:
 *     BMENUF_SET_ROOT(&frame_root);
 *     BMenuFFrameInitGroup(4,
 *                          &frame_root,frame_root_res,
 *                          &frame1,&frame1_res,
 *                          &frame2,&frame2_res,
 *                          &frame3,&frame3_res);
 *     BMenuFAssociate(&frame_root,3,&frame1,&frame2,&frame3);
 *     BMENUF_START;
 *
 * see further introductions, read the notes in the context respectively.
 */
/************************STRUCTURE DEFINITION****************************/
struct BasicMenuFrame_t {
  int (*response)();
  struct BasicMenuFrame_t *parent;
  uint totalSubNum;
  struct BasicMenuFrame_t * subFrame[4];
};
typedef struct BasicMenuFrame_t BasicMenuFrame;

//menu root
BasicMenuFrame * BMenuFroot;

/*********************GENERAL MACRO DEFINITIONS**************************/
#define BMENUF_SET_ROOT(frame)                BMenuFroot=(frame)
#define BMENUF_START                          ((*BMenuFroot->response)())
#define BMENUF_GET_CHILD(frame,index)         ((frame)->subFrame[(index)])
#define BMENUF_TO_CHILD(frame,index)          (frame)=BMENUF_GET_CHILD(frame,index)
#define BMENUF_SET_RESPONSE(frame,resfunc)    (frame)->response=(resfunc)

/*******************GENERAL FUNCTION DEFINITIONS*************************/
inline void BMenuFStart();
inline void BMenuFSetRoot(BasicMenuFrame * frame);
inline void BMenuFSetResponse(BasicMenuFrame * frame,int (*resfunc)());

/*
 * NOTICE:
 * call BMenuFFrameInit() to initialize a frame first before doing anything.
 * or you can do all the things by hand.
 * but before that, make sure you know what you are doing.
 */
void BMenuFFrameInit(BasicMenuFrame *frame,int (*resfunc)());


/*
 * this function appends child at the tail of subFrame array of par.
 * this function has bound check.
 * if out-of-bound happens, error handler will be called.
 * keep totalSubNum in your mind if you choose to use this.
 */
void BMenuFAppendChild(BasicMenuFrame * par,BasicMenuFrame * child);
/*
 * if successful, return 1.
 *     or 0.
 */
int BMenuFEjectChild(BasicMenuFrame * frame);
int BMenuFInsertChild(BasicMenuFrame * par,uint index,BasicMenuFrame * child);
int BMenuFDeleteChild(BasicMenuFrame * par,uint index,BasicMenuFrame * child);

/*********************Convenient Tool Definitions*************************/
/*
 * FOLLOWINGS ARE CONVENIENT TOOL KIT USING VARIABLE ARGUMENTS TECHNIQUE.
 * READ INSTRUCTION BEFORE USING IT.
 */
/*
 * NOTICE:
 * notice that there is a '...' here.
 * it means that this is a variable length function.
 * '...' needs frameNum sets of arguments in sequence of 
 *     (BasicMenuFrame *) and (int (*)()).
 * mentioning that (int (*)()) is the data type of a function pointer 
 *     which matches BasicMenuFrame->response.
 * use it as following:
 * BMenuFFrameInitGroup(4,
 *                      frame0,fr0res,
 *                      frame1,fr1res,
 *                      frame2,fr2res,
 *                      frame3,fr3res);
 * I am sure it will help you accelarate your coding speed.
 */
void BMenuFFrameInitGroup(uint frameNum,...);
/*
 * NOTICE:
 * notice that there is a '...' here.
 * it means that this is a variable length function.
 * '...' needs one or multiple arguments of (BasicMenuFrame *) type.
 * ensure you know this and do NOT make trival mistakes.
 * say:
 *     BMenuFAssociateGroup(frame0,4,frame1,frame2,frame3,frame4);
 * or:
 *     BMenuFAssociateGroup(frame0,2,frame1,frame2);
 * but:
 *     BMenuFAssociateGroup(frame0,6,frame1,frame2,frame3,
 *                          frame4,frame5,frame6);
 * is trival. since there is bound checking, the frames over 4 will be thrown away.
 * return the number of the terms really being filled in.
 */
int BMenuFAssociate(BasicMenuFrame * par,uint childNum,...);

#include "BasicMenuFramework.c"

#endif

//                            Ver. 0.6, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/24
