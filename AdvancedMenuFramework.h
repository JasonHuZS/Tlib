#ifndef _AdvancedMenuFramework_H_
#define _AdvancedMenuFramework_H_
#include "General.h"

/*
 * this is a particular head file of an advanced implementation of menu framework.
 * it provides a little bit complecated kind of implementation.
 * use it wisely. make sure you know what you are doing.
 *
 * similar usage to BasicMenuFramework, but more complecated.
 * only use it when BasicMenuFramework does not match your requirement.
 * read notes of each tool in the context respectively.
 */

/************************STRUCTURE DEFINITION****************************/
struct AdvancedMenuFrame_t {
  int (*response)();
  struct AdvancedMenuFrame_t *parent;
  uint totalSubNum;
  struct AdvancedMenuFrame_t ** subFrame;
  uint index;            //the index that is being selected.
  char * frameName;
};
typedef struct AdvancedMenuFrame_t AdvancedMenuFrame;

//menu root
AdvancedMenuFrame * AMenuFroot;

/*********************GENERAL MACRO DEFINITIONS**************************/
#define AMENUF_SET_ROOT(frame)                AMenuFroot=(frame)
#define AMENUF_START                          ((*AMenuFroot->response)())
#define AMENUF_GET_CHILD(frame,index)         ((frame)->subFrame[(index)])
#define AMENUF_TO_CHILD(frame,index)          (frame)=AMENUF_GET_CHILD(frame,index)
#define AMENUF_SET_RESPONSE(frame,resfunc)    (frame)->response=(resfunc)
#define AMENUF_SET_NAME(frame,name)           (frame)->frameName=(name)
#define AMENUF_CLR_INDEX(frame)               (frame)->index=0
#define AMENUF_INC_INDEX(frame)               (frame)->index++
#define AMENUF_DEC_INDEX(frame)               (frame)->index--

/*******************GENERAL FUNCTION DEFINITIONS*************************/
inline void AMenuFSetRoot(AdvancedMenuFrame * frame);
inline void AMenuFSetResponse(AdvancedMenuFrame * frame,int (*resfunc)());
inline void AMenuFSetName(AdvancedMenuFrame *frame,char *name);
inline void AMenuFClrIndex(AdvancedMenuFrame *frame);
inline void AMenuFIncIndex(AdvancedMenuFrame *frame);
inline void AMenuFDecIndex(AdvancedMenuFrame *frame);

/****************Initializing Function Definitions***********************/
/*
 * NOTICE:
 * a standard frame configuration in this implementation consists of two parts.
 * FIRST: to initialize a frame, call AMenuFFrameInit();
 * SECOND : to associate a frame with a configurated subFrame array,
 *             call AMenuFAssociate();
 * use auxilary functions like:
 *     AMenuFClrArray(), AMenuFFillArray()
 */
void AMenuFFrameInit(AdvancedMenuFrame *frame,int (*resfunc)(),char *name);
int AMenuFAssociate(AdvancedMenuFrame * par,
                     AdvancedMenuFrame **array,uint arraySize);

/***************Array Auxilary Function Definitions**********************/
/*
 * subFrame array auxilary functions:
 */
void AMenuFClrArray(AdvancedMenuFrame **array,uint arraySize);
inline void AMenuFModArray(AdvancedMenuFrame **array,uint index, AdvancedMenuFrame * term);
//convenient function tool
/*
 * NOTICE:
 * this is a variable length functions.
 * '...' eats (AdvancedMenuFrame *)-type var.s.
 * feed it right.
 * return the number of the terms really being filled in.
 */
int AMenuFFillArray(AdvancedMenuFrame **array,uint size, ...);

/*******************Child Modifier Definitions***************************/
/*
 * followings are afterward mutators.
 * if you have finished those two steps, but still want to do something further,
 *     use them.
 */
// CANNOT do bound check for the array! MAKE SURE YOU KNOW THE SIZE OF subFrame!
void AmenuFAppendChild(AdvancedMenuFrame *frame,AdvancedMenuFrame *child);
/*
 * if successful, return 1.
 *     or 0.
 */
int AmenuFEjectChild(AdvancedMenuFrame *frame);
int AmenuFInsertChild(AdvancedMenuFrame *frame,uint index,AdvancedMenuFrame *child);
int AmenuFDeleteChild(AdvancedMenuFrame *frame,uint index,AdvancedMenuFrame *child);

/*********************Convenient Tool Definitions*************************/
/*
 * FOLLOWINGS ARE CONVENIENT TOOL KIT USING VARIABLE ARGUMENTS TECHNIQUE.
 * READ INSTRUCTION BEFORE USING IT.
 */
/*
 * NOTICE:
 * notice that there is a '...' here.
 * it means that this is a variable length function.
 * '...' needs frameNum sets of arguments in sequence of 
 *     (AdvancedMenuFrame *), (int (*)()) and (char *).
 * mentioning that (int (*)()) is the data type of a function pointer 
 *     which matches AdvancedMenuFrame->response.
 * use it as following:
 * AMenuFFrameInitGroup(4,
 *                      frame0,fr0res,name0,
 *                      frame1,fr1res,name1,
 *                      frame2,fr2res,name2,
 *                      frame3,fr3res,name3);
 * I am sure it will help you accelarate your coding speed.
 */
void AMenuFFrameInitGroup(uint frameNum,...);

#include "AdvancedMenuFramework.c"

#endif

//                            Ver. 0.3, all untested
//                            Hu Zhongsheng implemented, last update in 2013/7/24
