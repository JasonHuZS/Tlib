#include "DMA.h"

static uchar DMAChannel=0;

void DMASetChannelCounter(uchar chNum){
  DMAChannel=chNum;
}

void DMAEnableAll(){
  switch (DMAChannel){
  case 3:
    DMA_CH2_ENABLE;
  case 2:
    DMA_CH1_ENABLE;
  case 1:
    DMA_CH0_ENABLE;
  }
}

void DMADisable(uchar channel){
  switch (channel){
  case 2:
    DMA_CH2_ENABLE;break;
  case 1:
    DMA_CH1_ENABLE;break;
  case 0:
    DMA_CH0_ENABLE;break;
  }
}

uchar DMASet(DMA_Trigger trigger,DMA_TransferM trMode,             //channel initialization info.
             void* srcAddr,DMA_DataType srcType,DMA_Incr srcDir,   //src info
             void* dstAddr,DMA_DataType dstType,DMA_Incr dstDir,   //dst info
             uint length){
  uint bitmap;
  if (DMAChannel>=3) return 0;
  DMA_SET_DMATSEL(DMAChannel,trigger);
  length<<=(srcType&1);
  bitmap=trMode+srcDir+(dstDir<<2)+(srcType&0xFFFE)+((dstType&0xFFFE)<<1);
  switch (DMAChannel){
  case 0:
    DMA_ASN_0CTL(bitmap);
    DMA_ASN_0SA((uint)srcAddr);
    DMA_ASN_0DA((uint)dstAddr);
    DMA_ASN_0SZ(length);
    break;
  case 1:
    DMA_ASN_1CTL(bitmap);
    DMA_ASN_1SA((uint)srcAddr);
    DMA_ASN_1DA((uint)dstAddr);
    DMA_ASN_1SZ(length);
    break;
  case 2:
    DMA_ASN_2CTL(bitmap);
    DMA_ASN_2SA((uint)srcAddr);
    DMA_ASN_2DA((uint)dstAddr);
    DMA_ASN_2SZ(length);
    break;
  }
  #ifndef __Alternative_DMA_
  DMA_RESET_HANDLER(DMAChannel);
  #endif
  return ++DMAChannel;
}

uchar DMASingleToSingle(DMA_Trigger trigger,       //channel initialization info.
             void* srcAddr,DMA_DataType srcType,   //src info
             void* dstAddr,DMA_DataType dstType){  //dst info
  return DMASet(trigger,DMA_REPEATED_SINGLE,srcAddr,srcType,DMA_UNCHANGED,
                dstAddr,dstType,DMA_UNCHANGED,1);
}

uchar DMAMulToSingle(DMA_Trigger trigger,       //channel initialization info.
                     void* srcAddr,DMA_DataType srcType,   //src info
                     void* dstAddr,DMA_DataType dstType,   //dst info
                     uint length){
  return DMASet(trigger,DMA_BLOCK,srcAddr,srcType,DMA_INCREMENT,
                dstAddr,dstType,DMA_UNCHANGED,length);
}

uchar DMASingleToMul(DMA_Trigger trigger,       //channel initialization info.
                     void* srcAddr,DMA_DataType srcType,   //src info
                     void* dstAddr,DMA_DataType dstType,   //dst info
                     uint length){
  return DMASet(trigger,DMA_BLOCK,srcAddr,srcType,DMA_UNCHANGED,
                dstAddr,dstType,DMA_INCREMENT,length);
}

uchar DMAMulToMul(DMA_Trigger trigger,       //channel initialization info.
                  void* srcAddr,DMA_DataType srcType,   //src info
                  void* dstAddr,DMA_DataType dstType,   //dst info
                  uint length){
  return DMASet(trigger,DMA_BLOCK,srcAddr,srcType,DMA_INCREMENT,
                dstAddr,dstType,DMA_INCREMENT,length);
}

#ifndef __Alternative_DMA_
#pragma vector=DMA_VECTOR
__interrupt void DMAISR(){
  _DINT();
  uchar ind,res=0;
  ind=(DMA_GET_IV>>1)-1;
  if (ind<3)
    res=(*DMAisrHandler[ind])();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif
