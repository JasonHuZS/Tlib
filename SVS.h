#ifndef _SVS_H_
#define _SVS_H_
#include "General.h"
/*
 * The SVS configure and inquiry functions 
 * On Plafrom of MSP430F2619
 * Parameter of SVSInit(): 
 *		vld for the threshold Voltage(0~15)
 *			0000 SVS is off
 *			0001 1.9 V
 *			0010 2.1 V
 *			0011 2.2 V
 *			0100 2.3 V
 *			0101 2.4 V
 *			0110 2.5 V
 *			0111 2.65 V
 *			1000 2.8 V
 *			1001 2.9 V
 *			1010 3.05 V
 *			1011 3.2 V
 *			1100 3.35 V
 *			1101 3.5 V
 *			1110 3.7 V
 *			1111 Compares external input voltage SVSIN to 1.25 V.
 * 		poron: when the poron is set, there is a POR signal when supply voltage drops under threshold voltage
 * 		svsout: when svsout is set, P5.7 is used as the output of SVS
 * Parameter of SVSFlag():
 * 		when clear = 1, SVSFG is cleared when function returns.
 * 		else, SVSFG remains.
 * TODO: there seems to be a lack of Interrupt vector for SVSFG, so there is no ISR
 *
 * Port Usage:
 *  P5.7 -> out: SVSOUT
 *  P6.7 -> in: SVSIN
 */

//VLD macros
typedef uint SVS_VLD;
#define SVS_VLDBIT            (0xf0)
#define SVS_1_9V              (0x10)
#define SVS_2_1V              (0x20)
#define SVS_2_2V              (0x30)
#define SVS_2_3V              (0x40)
#define SVS_2_4V              (0x50)
#define SVS_2_5V              (0x60)
#define SVS_2_65V             (0x70)
#define SVS_2_8V              (0x80)
#define SVS_2_9V              (0x90)
#define SVS_3_05V             (0xA0)
#define SVS_3_2V              (0xB0)
#define SVS_3_35V             (0xC0)
#define SVS_3_5V              (0xD0)
#define SVS_3_7V              (0xE0)
#define SVS_SVSIN_COMP_1_25V  (0xF0)

#define SVS_PORON             SET_BIT(SVSCTL,PORON)
#define SVS_POROFF            CLR_BIT(SVSCTL,PORON)
#define SVS_OUTPUT            (SVSCTL&SVSOP)
#define SVS_ON                (SVSCTL&SVSON)
#define SVS_FLAG_CLEAR        CLR_BIT(SVSCTL,SVSFG)
#define SVS_FLAG              (SVSCTL&SVSFG)

#define SVS_IN_INIT           CLR_BIT(P6DIR,BIT7);SET_BIT(P6SEL,BIT7)

void SVSInit(SVS_VLD vld, boolean poron, boolean svsout);

#include "SVS.c"

#endif

//	Version 0.7, untested
//	Zheng Xueli implemented, last update in 2013/07/27
