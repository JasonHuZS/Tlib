#ifndef _DAC12_H_
#define _DAC12_H_
#include "msp430.h"
#include "General.h"

/*
 * this is the head file for DAC12
 * this head file includes functions of initializing DAC12 as below
 *      DAC12_0Init(uint ref,uint refs,uint ir,uint res,uint load,uint amp)
 *      DAC12_1Init(uint ref,uint refs,uint ir,uint res,uint load,uint amp)
 *      see details in DAC12.c
 * this head file also includes functions of writing data to DAC_DAT
 *      DAC12_0WriteData(uint data)
 *      DAC12_0WriteData(uint data)
 *      see details in DAC12.c
 * port are used as below:
 *      P6.6 -> PDAC12_0
 *      P6.7 -> PDAC12_1
 */

/***********************ISR HANDLER DEFINITIONS***********************/
#ifndef __Alternative_DAC12_

uchar (*DAC12isr0Handler)();
uchar (*DAC12isr1Handler)();

#define DAC12_SET_0HANDLER(handler)   DAC12isr0Handler=handler
#define DAC12_RESET_0HANDLER          DAC12isr0Handler=ucblank
#define DAC12_SET_1HANDLER(handler)   DAC12isr1Handler=handler
#define DAC12_RESET_1HANDLER          DAC12isr1Handler=ucblank

#pragma vector=DAC12_VECTOR
__interrupt void DAC12ISR();

#endif

/********************DAC12xCTL MACRO DEFINITIONS************************/
#define DAC12_0CTL_SET(bitmap)        SET_BIT(DAC12_0CTL,(bitmap))
#define DAC12_0CTL_CLR(bitmap)        CLR_BIT(DAC12_0CTL,(bitmap))
#define DAC12_0CTL_ASN(bitmap)        ASN_BIT(DAC12_0CTL,(bitmap))

#define DAC12_1CTL_SET(bitmap)        SET_BIT(DAC12_1CTL,(bitmap))
#define DAC12_1CTL_CLR(bitmap)        CLR_BIT(DAC12_1CTL,(bitmap))
#define DAC12_1CTL_ASN(bitmap)        ASN_BIT(DAC12_1CTL,(bitmap))

//reference voltage
typedef uint DAC12_RefVol;
#define DAC12_REFBIT                  (0x6000)
#define DAC12_VREF                    (0x0000)
#define DAC12_VEREF                   (0x6000)

//resolution
typedef uint DAC12_Resolution;
#define DAC12_RESBIT                  (0x1000)
#define DAC12_12BIT                   (0x0000)
#define DAC12_8BIT                    (0x1000)

//load select
typedef uint DAC12_Load;
#define DAC12_LOADBIT                 (0x0C00)
#define DAC12_WRITTEN                 (0x0000)
#define DAC12_GRP_WRITTEN             (0x0400)
#define DAC12_TA1                     (0x0800)
#define DAC12_TB2                     (0x0C00)

//input range
typedef uint DAC12_InputRange;
#define DAC12_IRBIT		      (0x0100)
#define DAC12_AV3                     (0x0000)
#define DAC12_AV1                     (0x0100)

//amplifier setting
typedef uint DAC12_Amplifier;
#define DAC12_AMPBIT                  (0x00E0)
#define DAC12_IN_OFF_OUT_HIGHZ        (0x0000)
#define DAC12_IN_OFF_OUT_0V           (0x0020)
#define DAC12_IN_LOW_OUT_LOW          (0x0040)
#define DAC12_IN_LOW_OUT_MED          (0x0060)
#define DAC12_IN_LOW_OUT_HIG          (0x0080)
#define DAC12_IN_MED_OUT_MED          (0x00A0)
#define DAC12_IN_MED_OUT_HIG          (0x00C0)
#define DAC12_IN_HIG_OUT_HIG          (0x00E0)

#define DAC12_OPS                     (0x8000)

//data format
#define DAC12_DFBIT                   (0x0010)
#define DAC12_STRAIGHT_BINARY         (0x0000)
#define DAC12_2S_COMPLEMENT           (0X0010)               

//interrupt control
#define DAC12_SET_IE0                 DAC12_0CTL_SET(DAC12IE)
#define DAC12_CLR_IE0                 DAC12_0CTL_CLR(DAC12IE)
#define DAC12_GET_IFG0                (DAC12_0CTL&DAC12IFG)
#define DAC12_CLR_IFG0                DAC12_0CTL_CLR(DAC12IFG)

#define DAC12_SET_IE1                 DAC12_1CTL_SET(DAC12IE)
#define DAC12_CLR_IE1                 DAC12_1CTL_CLR(DAC12IE)
#define DAC12_GET_IFG1                (DAC12_1CTL&DAC12IFG)
#define DAC12_CLR_IFG1                DAC12_1CTL_CLR(DAC12IFG)

//others
#define DAC12_ENABLE0                 DAC12_0CTL_SET(DAC12ENC)
#define DAC12_DISABLE0                DAC12_0CTL_CLR(DAC12ENC)


#define DAC12_ENABLE1                 DAC12_1CTL_SET(DAC12ENC)
#define DAC12_DISABLE1                DAC12_1CTL_CLR(DAC12ENC)

/********************DAC12 MODIFIERS&INITIALIZATION*********************/
#define DAC12_0DAT_SET(value)         SET_BIT(DAC12_0DAT,(value))
#define DAC12_1DAT_SET(value)         SET_BIT(DAC12_1DAT,(value))
//DAC12 Initialization
/*
 * default:
 * DAC12_xCTL <- DAC12ENC, Straight binary
 * ref  -> reference voltage
 * ir   -> input range
 * res  -> resolution
 * load -> load select
 * amp  -> amplifier setting
 */
#ifndef __Alternative_DAC12_

#define DAC12_0INIT(ref,ir,res,load,amp) \
  DAC12_0CTL_ASN(DAC12ENC+(ref)+(ir)+(res)+(load)+(amp));\
  SET_BIT(P6DIR,BIT6);\
  SET_BIT(P6SEL,BIT6);\
  DAC12_RESET_0HANDLER

#define DAC12_1INIT(ref,ir,res,load,amp) \
  DAC12_1CTL_ASN(DAC12ENC+(ref)+(ir)+(res)+(load)+(amp));\
  SET_BIT(P6DIR,BIT7);\
  SET_BIT(P6SEL,BIT7);\
  DAC12_RESET_1HANDLER

#else

#define DAC12_0INIT(ref,ir,res,load,amp) \
  DAC12_0CTL_ASN(DAC12ENC+(ref)+(ir)+(res)+(load)+(amp))

#define DAC12_1INIT(ref,ir,res,load,amp) \
  DAC12_1CTL_ASN(DAC12ENC+(ref)+(ir)+(res)+(load)+(amp))

#endif

#define DAC12_0SIMPLE_INIT  \
  DAC12_0INIT(DAC12_VREF,DAC12_AV1,DAC12_12BIT,DAC12_WRITTEN,DAC12_IN_MED_OUT_MED)

#define DAC12_1SIMPLE_INIT  \
  DAC12_1INIT(DAC12_VREF,DAC12_AV1,DAC12_12BIT,DAC12_WRITTEN,DAC12_IN_MED_OUT_MED)

/***********************FUNCTION DECLARATIONS*************************/
inline void DAC12_0Init(DAC12_RefVol ref,DAC12_InputRange ir,
                        DAC12_Resolution res,DAC12_Load load,DAC12_Amplifier amp);
inline void DAC12_0SimpleInit(void);
inline void DAC12_1Init(DAC12_RefVol ref,DAC12_InputRange ir,
                        DAC12_Resolution res,DAC12_Load load,DAC12_Amplifier amp);
inline void DAC12_1SimpleInit(void);
inline void DAC12_0WriteData(uint data);
inline void DAC12_1WriteData(uint data);

#include "DAC12.c"

#endif


                                    //Ver. 0.8, all tested excepted VEREF
                                    //Yuanyuan Tan implemented, last update in 2013/7/27
