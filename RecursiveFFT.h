#ifndef _RecursiveFFT_H_
#define _RecursiveFFT_H_
#include "Complex.h"

/*
 * NOTICE:
 * this is a head file that packs fft algorithm.
 * before using it, please turn on VLA to optimize the memory usage.
 */

/*
 * use this function like this:
 * RFFTCal(rtnarr,inpsig,SALEN,0,1,1);
 * where rtnarr is a complex[128] array and
 * inpsig is a unsigned[128] array.
 */

void RFFTCal(Complex *rtnarr,unsigned *inpsig,unsigned n,
         unsigned start,unsigned step,unsigned indshft);

#include "RecursiveFFT.c"

#endif

//                            Ver. 0.75, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/18
