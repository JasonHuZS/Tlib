#include "AdvancedMenuFramework.h"
#include <stdarg.h>
#include <stddef.h>

void AMenuFSetRoot(AdvancedMenuFrame * frame){
  AMenuFroot=frame;
}

void AMenuFSetResponse(AdvancedMenuFrame * frame,int (*resfunc)()){
  frame->response=resfunc;
}

void AMenuFSetName(AdvancedMenuFrame *frame,char *name){
  frame->frameName=name;
}

void AMenuFClrIndex(AdvancedMenuFrame *frame){
  frame->index=0;
}

void AMenuFIncIndex(AdvancedMenuFrame *frame){
  frame->index++;
}

void AMenuFDecIndex(AdvancedMenuFrame *frame){
  frame->index--;
}


void AMenuFFrameInit(AdvancedMenuFrame *frame,int (*resfunc)(),char *name){
  frame->response=resfunc;
  frame->parent=NULL;
  frame->totalSubNum=0;
  frame->subFrame=NULL;
  frame->index=0;
  frame->frameName=name;
}


int AMenuFAssociate(AdvancedMenuFrame * par,
                     AdvancedMenuFrame **array,uint arraySize){
  uint i;
  if (array==NULL)
    return 0;
  par->subFrame=array;
  par->totalSubNum=arraySize;
  for(i=0;i<arraySize;i++)
    if (array[i]!=NULL)
      (array[i])->parent=par;
  return 1;
}

void AMenuFClrArray(AdvancedMenuFrame **array,uint arraySize){
  uint i;
  for(i=0;i<arraySize;i++)
    array[i]=NULL;
}

int AMenuFFillArray(AdvancedMenuFrame **array,uint size, ...){
  uint i;
  AdvancedMenuFrame * para;
  va_list argp;
  va_start( argp, size );
  for (i=0;i<size;i++){
    para=va_arg(argp,AdvancedMenuFrame *);
    array[i]=para;
  }
  va_end(argp);
  return i;
}

void AMenuFModArray(AdvancedMenuFrame **array,uint index, AdvancedMenuFrame * term){
  array[index]=term;
}

void AmenuFAppendChild(AdvancedMenuFrame *frame,AdvancedMenuFrame *child){
  frame->subFrame[frame->totalSubNum++]=child;
  child->parent=frame;
}

int AmenuFEjectChild(AdvancedMenuFrame *frame){
  uint l=frame->totalSubNum;
  if (l==0)
    return 0;
  frame->subFrame[l-1]->parent=NULL;
  frame->subFrame[l--]=NULL;
  frame->totalSubNum=l;
  return 1;
}

int AmenuFInsertChild(AdvancedMenuFrame *frame,uint index,AdvancedMenuFrame *child){
  if (index>=frame->totalSubNum)
    return 0;
  frame->subFrame[index]->parent=NULL;
  frame->subFrame[index]=child;
  child->parent=frame;
  return 1;
}

int AmenuFDeleteChild(AdvancedMenuFrame *frame,uint index,AdvancedMenuFrame *child){
  uint i,l;
  l=frame->totalSubNum;
  if (index>=l)
    return 0;
  frame->subFrame[index]=NULL;
  child->parent=NULL;
  for (i=index+1;i<l;i++)
    frame->subFrame[i-1]=frame->subFrame[i];
  frame->totalSubNum=l-1;
  return 1;
}

void AMenuFFrameInitGroup(uint frameNum,...){
  uint i;
  AdvancedMenuFrame * frame;
  int (*resfunc)();
  char * name;
  va_list argp;
  va_start( argp, frameNum );
  for (i=0;i<frameNum;i++){
    frame=va_arg(argp,BasicMenuFrame *);
    resfunc=(int (*)())(va_arg(argp,void *));
    (va_arg(argp,void *));
    name=va_arg(argp,char *);
    AMenuFFrameInit(frame,resfunc,name);
  }
}
