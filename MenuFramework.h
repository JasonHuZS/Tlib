#ifndef _MenuFramework_H_
#define _MenuFramework_H_

/*
 * this is a general head file of menu framework.
 * this framework provides a shotcut to fast establish a menu tree.
 * this framework has multiple implementations, choose the one you really need.
 * say:
 *     #define __USE_AdvanceMenuFramework_
 *     #include "MenuFramework.h"
 * NOTICE:
 * if you DON'T predefine any derectives, BasicMenuFramework.h will be included
 *     as default.
 */

#ifdef __USE_AdvancedMenuFramework_
#include "AdvancedMenuFramework.h"
#else
#ifdef __USE_DynamicMenuFramework_
#include "DynamicMenuFramework.h"
#else
#include "BasicMenuFramework.h"
#endif
#endif

#endif

