#include "ComparatorA.h"

static uchar CompAmapping[8]={BIT3,BIT4,BIT0,BIT1,
                              BIT2,BIT5,BIT6,BIT7};

void CompAExternalInit(uchar caplus,uchar caminus){
  uchar bitmap=(CompAmapping[caplus]+CompAmapping[caminus]);
  COMPA_ASN_CTL1(CAIE);
  COMPA_ASN_CTL2((CAF+(caminus<<<3)));
  switch (caplus) {
  case 0:
    COMPA_PLUS_CA0;break;
  case 1:
    COMPA_PLUS_CA1;break;
  case 2:
    COMPA_PLUS_CA2;break;
  default:
    COMPA_PLUS_CA0;
  }
  COMPA_ASN_PD(bitmap);
  COMPA_IN_INIT(bitmap);
  #ifndef __Alternative_ComparatorA_
  COMPA_RESET_HANDLER;
  #endif
}

void CompAInternalInit(uchar caminus){
  uchar bitmap=(CompAmapping[caminus]);
  COMPA_ASN_CTL1((CAREF_1+CAIE));
  COMPA_ASN_CTL2((CAF+(caminus<<<3)));
  COMPA_ASN_PD(bitmap);
  COMPA_IN_INIT(bitmap);
  #ifndef __Alternative_ComparatorA_
  COMPA_RESET_HANDLER;
  #endif
}

#ifndef __Alternative_ComparatorA_

#pragma vector=COMPARATORA_VECTOR
__interrupt void CompAISR(){
  _DINT();
  uchar res=0;
  res=(*CompAisrHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  COMPA_CLR_CAIFG;
  __bis_SR_register_on_exit(GIE);
}

#endif
