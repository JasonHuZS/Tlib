#include "TimerB.h"
#include <stdarg.h>
#include <stddef.h>

static uint TimerBmodeControl;

void TimerBStart(){
  SET_BIT(TBCTL,TimerBmodeControl);
}

void TimerBPause(){
  CLR_BIT(TBCTL,TimerBmodeControl);
  ASN_BIT(TBR,0);
}

void TimerBCompareInit(TimerB_ModeCtrl mcbit,uint TBCCTL_num,...){
  uint i;
  uint TBCCR,ombit;
  unsigned short volatile* reg[7]={&TBCCR0,&TBCCR1,&TBCCR2,
                                   &TBCCR3,&TBCCR4,&TBCCR5,&TBCCR6};
  unsigned short volatile* ctlreg[7]={&TBCCTL0,&TBCCTL1,&TBCCTL2,
                                      &TBCCTL3,&TBCCTL4,&TBCCTL5,&TBCCTL6};
  va_list argp;
  va_start(argp,TBCCTL_num);
  TimerBmodeControl=mcbit;
  TIMERB_TBCLR;
  TBCTL=TIMERB_ALL_INDEPENDENT+TASSEL_2;
  for (i=0;i<TBCCTL_num&&i<6;i++){
    TBCCR=va_arg(argp,uint);
    ombit=va_arg(argp,TimerB_OutputM);
    ASN_BIT(*reg[i],TBCCR);
    ASN_BIT(*ctlreg[i],TIMERB_WHEN_WRITTEN+ombit);
  }
  va_end(argp);
  #ifndef __Alternative_TimerB_0_
  TIMERB_RESET_HANDLER0;
  #endif
  #ifndef __Alternative_TimerB_1_
  TIMERB_RESET_CC_HANDLER(1);
  TIMERB_RESET_CC_HANDLER(2);
  TIMERB_RESET_CC_HANDLER(3);
  TIMERB_RESET_CC_HANDLER(4);
  TIMERB_RESET_CC_HANDLER(5);
  TIMERB_RESET_CC_HANDLER(6);
  TIMERB_RESET_TB_HANDLER;
  #endif
}

void TIMERBCompareAdvancedInit(TimerB_ModeCtrl mcbit,       //TBCTL mode control bits
                               TimerB_TBCLGroup grpbit,     //TBCTL grouping ctrl bits
                               uint TBCCTL_num,             //number of used TBCCTLx
                               ...){                        //TBCCTLx control bits, x <- 0~6
  uint i;
  uint TBCCR,ombit,clldbit;
  unsigned short volatile* reg[7]={&TBCCR0,&TBCCR1,&TBCCR2,
                                   &TBCCR3,&TBCCR4,&TBCCR5,&TBCCR6};
  unsigned short volatile* ctlreg[7]={&TBCCTL0,&TBCCTL1,&TBCCTL2,
                                      &TBCCTL3,&TBCCTL4,&TBCCTL5,&TBCCTL6};
  va_list argp;
  va_start(argp,TBCCTL_num);
  TimerBmodeControl=mcbit;
  TIMERB_TBCLR;
  TBCTL=TASSEL_2+grpbit;
  for (i=0;i<TBCCTL_num&&i<6;i++){
    TBCCR=va_arg(argp,uint);
    ombit=va_arg(argp,TimerB_OutputM);
    clldbit=va_arg(argp,TimerB_CompLatchLoad);
    ASN_BIT(*reg[i],TBCCR);
    ASN_BIT(*ctlreg[i],ombit+clldbit);
  }
  va_end(argp);
  #ifndef __Alternative_TimerB_0_
  TIMERB_RESET_HANDLER0;
  #endif
  #ifndef __Alternative_TimerB_1_
  TIMERB_RESET_CC_HANDLER(1);
  TIMERB_RESET_CC_HANDLER(2);
  TIMERB_RESET_CC_HANDLER(3);
  TIMERB_RESET_CC_HANDLER(4);
  TIMERB_RESET_CC_HANDLER(5);
  TIMERB_RESET_CC_HANDLER(6);
  TIMERB_RESET_TB_HANDLER;
  #endif
}

void TimerBCaptureInit(TimerB_ModeCtrl mcbit,uint TBCCTL_num,...){
  uint i;
  uint TBCCR,cmbit;
  unsigned short volatile* reg[7]={&TBCCR0,&TBCCR1,&TBCCR2,
                                   &TBCCR3,&TBCCR4,&TBCCR5,&TBCCR6};
  unsigned short volatile* ctlreg[7]={&TBCCTL0,&TBCCTL1,&TBCCTL2,
                                      &TBCCTL3,&TBCCTL4,&TBCCTL5,&TBCCTL6};
  va_list argp;
  va_start(argp,TBCCTL_num);
  TimerBmodeControl=mcbit;
  TIMERB_TBCLR;
  TBCTL=TASSEL_2;
  for (i=0;i<TBCCTL_num&&i<6;i++){
    TBCCR=va_arg(argp,uint);
    cmbit=va_arg(argp,TimerB_CaptureM);
    ASN_BIT(*reg[i],TBCCR);
    ASN_BIT(*ctlreg[i],CAP+SCS+cmbit);
  }
  va_end(argp);
  #ifndef __Alternative_TimerB_0_
  TIMERB_RESET_HANDLER0;
  #endif
  #ifndef __Alternative_TimerB_1_
  TIMERB_RESET_CC_HANDLER(1);
  TIMERB_RESET_CC_HANDLER(2);
  TIMERB_RESET_CC_HANDLER(3);
  TIMERB_RESET_CC_HANDLER(4);
  TIMERB_RESET_CC_HANDLER(5);
  TIMERB_RESET_CC_HANDLER(6);
  TIMERB_RESET_TB_HANDLER;
  #endif
}

#ifndef __Alternative_TimerB_0_      //0xFFFA Timer B CC0
#pragma vector=TIMERB0_VECTOR
__interrupt void TimerB0ISR(){
  _DINT();
  uchar res=0;
  res=(*TimerBisr0Handler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif

#ifndef __Alternative_TimerB_1_      //0xFFF8 Timer B CC1-6, TB
#pragma vector=TIMERB1_VECTOR
__interrupt void TimerB1ISR(){
  _DINT();
  uchar res=0;
  uint tbtmp;
  while(tbtmp=TBIV&0x000E){
    if (tbtmp==TIMERB_TBIV_TBIFG)
      res=(*TimerBTBHandler)();
    else
      res=(*TimerBCCHandler[(tbtmp>>1)-1])();
  }
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif
