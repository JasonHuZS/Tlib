#ifndef _KeyBoard1X8_H_
#define _KeyBoard1X8_H_
#include "msp430f2619.h"
#include "KeyBoard.h"
#include "General.h"

/*
 * this is a 1X8 KeyBoard head file.
 * call KB1X8Init(uchar bitmap) before use it.
 * set KB1X8handler to handle events.
 * user must know which port is using, thus
 *     #define __USE_PORT1_KB1X8_
 * before including this file.
 * PORT:
 * if defined __USE_PORT1_KB1X8_:
 * use any of P1 you config to use.
 * if defined __USE_PORT2_KB1X8_:
 * use any of P2 you config to use.
 * connecting illustration:
 * 
 *   Vcc /|\
 *        | 
 *        --res--------- Px.y
 *         10kohm   |
 *                 key
 *                  |
 *                  |
 *                 ---Gnd
 *                  -
 */
/*
 * only work for pull-up resistor keyboard.
 */
 
/************************ISR HANDLER DEFINITIONS**************************/

/*
 * KB1X8handler is a function pointer array containing 9 items.
 * it contains the handler of corresponding key response.
 * assigned handler normally:
 *     uchar handler_foo();
 *     KB1X8handler[0]=handler_foo;
 * 
 * the return values of handlers is meaningful.
 * if a handler returns 0, CPUOFF bit will remain the same.
 * if a handler returns non-0, CPUOFF bit will be cleared after ISR returns.
 */
uchar (*KB1X8handler[8])();
/*
 * KB1X8errorHandler handles default case in switch and error cases.
 */
uchar (*KB1X8errorHandler)();

/*
 * you are free to use mutators to modify handlers or just
 *     perform assignment directly.
 * but using mutators is recommanded.
 */
inline void KB1X8ResetHandler(int i);
inline void KB1X8SetHandler(int i,uchar (*handler)());
inline void KB1X8ResetErrorHandler();
inline void KB1X8SetErrorHandler(uchar (*handler)());

/*********************GENERAL MACRO DEFINITIONS**************************/
#define KB1X8_LOCK(bits)                    KB1X8Lock((bits))
#define KB1X8_UNLOCK(bits)                  KB1X8Unlock((bits))
#define KB1X8_ON_PRESS(bits)                SET_BIT(KB1X8_IES,bits)
#define KB1X8_ON_RELEASE(bits)              CLR_BIT(KB1X8_IES,bits)
#define KB1X8_RESET_HANDLER(i)              KB1X8handler[(i)]=ucblank
#define KB1X8_SET_HANDLER(i,handler)        KB1X8handler[(i)]=(handler)
#define KB1X8_RESET_ERROR_HANDLER           KB1X8errorHandler=ucblank
#define KB1X8_SET_ERROR_HANDLER(handler)    KB1X8errorHandler=(handler)

/************************INITIALIZER DEFINITIONS**************************/
/*
 * use bitmap to configurate which keys you actually want to use.
 * like:
 * KB1X8Init(BIT1+BIT2+BIT3+BIT4);
 */
void KB1X8Init(uchar bitmap);

/********************INTERRUPT CONTROL DEFINITIONS************************/
void KB1X8Lock(uchar bitmap);
void KB1X8Unlock(uchar bitmap);

/*********************CONVENIENT TOOL DEFINITIONS*************************/
/*
 * FOLLOWINGS ARE CONVENIENT TOOL KIT USING VARIABLE ARGUMENTS TECHNIQUE.
 * READ INSTRUCTION BEFORE USING IT.
 */
/*
 * NOTICE:
 * notice that there is a '...' here.
 * it means that this is a variable length function.
 * '...' needs frameNum sets of arguments in sequence of (uchar *)().
 * mentioning that (uchar *)() is the data type of a function pointer 
 *     which matches those ISR handlers.
 * use it as following:
 * KB1X8SetHandlerGroup(1,4, //set KB1X8handler[1~4]
 *                      handler1,handler2,handler3,handler4);
 * I am sure it will help you accelarate your coding speed.
 */
void KB1X8SetHandlerGroup(uint startInd,uint handlerNum,...);

#ifdef __USE_PORT1_KB1X8_

#define KB1X8_INT_VEC   (PORT1_VECTOR)
#define KB1X8_IFG       (P1IFG)
#define KB1X8_IN        (P1IN)
#define KB1X8_IE        (P1IE)
#define KB1X8_IES       (P1IES)
#define KB1X8_DIR       (P1DIR)
#define KB1X8_SEL       (P1SEL)
#define KB1X8_OUT       (P1OUT)

#pragma vector= KB1X8_INT_VEC
__interrupt void KB1X8ISR();

#include "KeyBoard1X8.c"

#else
#ifdef __USE_PORT2_KB1X8_

#define KB1X8_INT_VEC   (PORT2_VECTOR)
#define KB1X8_IFG       (P2IFG)
#define KB1X8_IN        (P2IN)
#define KB1X8_IE        (P2IE)
#define KB1X8_IES       (P2IES)
#define KB1X8_DIR       (P2DIR)
#define KB1X8_SEL       (P2SEL)
#define KB1X8_OUT       (P2OUT)

#pragma vector= KB1X8_INT_VEC
__interrupt void KB1X8ISR();

#include "KeyBoard1X8.c"

#endif
#endif

#endif

//                            Ver. 0.83, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/24
