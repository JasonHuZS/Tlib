#include "msp430f2619.h"
#include "General.h"
#include "LCD12864.h"

static void LCD12864Init();

static void LCD12864Init(){
  P7SEL=0;
  P8SEL=0;
  P7DIR = 0XFF;
  P8DIR = 0XFF;
  P8OUT=0;
  LCD12864_BASIC_DELAY(10);
  P8OUT = BIT3+BIT4+BIT5;
  LCD12864_BASIC_DELAY(1);
}

uchar LCD12864CheckBusy(){
  uchar sig;
  P7DIR=0;
  P8OUT&=~(BIT0+BIT2);
  //LCD12864_BASIC_DELAY(1);
  P8OUT|=BIT1;
  LCD12864_BASIC_DELAY(1);
  P8OUT|=BIT2;
  LCD12864_BASIC_DELAY(1);
  P8OUT&=~BIT2;
  LCD12864_BASIC_DELAY(1);
  sig=P7IN;
  P7DIR=0xFF;
  return sig&BIT7;
}

void LCD12864WriteCom(uchar LCD12864com){
  while (!(LCD12864CheckBusy()));
  P8OUT&=~(BIT0+BIT1+BIT2);
  P7OUT=LCD12864com;
  LCD12864_BASIC_DELAY(1);
  P8OUT|=BIT2;
  LCD12864_BASIC_DELAY(1);
  P8OUT&=~BIT2;
  LCD12864_BASIC_DELAY(1);
}

void LCD12864WriteDat(uchar LCD12864dat){
  while (!(LCD12864CheckBusy()));
  P8OUT&=~(BIT0+BIT1+BIT2);
  P8OUT|=BIT0;
  P7OUT=LCD12864dat;
  LCD12864_BASIC_DELAY(1);
  P8OUT|=BIT2;
  LCD12864_BASIC_DELAY(1);
  P8OUT&=~BIT2;
  LCD12864_BASIC_DELAY(1);
}

void LCD12864InitLCD(){
  LCD12864Init();
  P8OUT&=~BIT2;
  LCD12864_BASIC_DELAY(5);
  LCD12864WriteCom(0x30);
  LCD12864_BASIC_DELAY(1);
  LCD12864WriteCom(0x30);
  LCD12864WriteCom(0x0c);
  LCD12864WriteCom(0x1);
  LCD12864_BASIC_DELAY(10);
  LCD12864WriteCom(0x06);
  
}

void LCD12864DispChar(uchar addr,uchar *pt,uchar num){
  unsigned char i;
  LCD12864WriteCom(addr);
  for(i=0;i<num;i++)
    LCD12864WriteDat(*pt++);
}

void LCD12864SendChar(uchar addr,uchar *pt){
  unsigned char i;
  LCD12864WriteCom(addr);
  for(i=0;i<16;i++){
    if (*pt=='\0') break;
    LCD12864WriteDat(*pt++);
  }
}

void LCD12864CleanGraphRam(){
  int i,j;
  LCD12864_TURN_OFF_GRAPH;
  for (i=0;i<16;i++){   //row
    for (j=0;j<32;j++){ //column
      LCD12864WriteCom(0x80+j);
      LCD12864WriteCom(0x80+i);
      LCD12864WriteDat(0x00);
      LCD12864WriteDat(0x00);
    }
  }
  return;
}

void LCD12864WriteGraphIn(unsigned* matrix,unsigned row,unsigned col,
                    unsigned grow,unsigned gcol){
  unsigned i,j,data,rowadd=0;
  uchar datah,datal;
  LCD12864_TURN_OFF_GRAPH;
  for (i=0;i<row;i++){            //row
    for (j=0;j<col;j++){          //column
      LCD12864WriteCom(0x80+grow+i);     //send column address
      LCD12864WriteCom(0x80+gcol+j);     //send row address
      data=*(matrix+rowadd+j);
      datah=(data>>8);
      datal=data&0xff;
      LCD12864WriteDat(datah);
      LCD12864WriteDat(datal);
    }
    rowadd+=col;
  }
  LCD12864_TURN_ON_GRAPH;
  return;
}

void LCD12864ParseFunction(uchar fval[128],unsigned matrix[16][32]){
  unsigned i,j,k,fvalc;
  unsigned datah1,datal1,datah2,datal2;
  for(i=0,fvalc=0;i<8;i++,fvalc+=16){
    for(j=0;j<32;j++){
      datah1=0;
      datal1=0;
      datah2=0;
      datal2=0;
      for(k=0;k<8;k++){
        if (64-j<=fval[fvalc+7-k]){
          datah1|=(1<<k);
          datah2|=(1<<k);
        } else if (32-j<=fval[fvalc+7-k])
          datah2|=(1<<k);
      }
      for(k=0;k<8;k++){
        if (64-j<=fval[fvalc+15-k]){
          datal1|=(1<<k);
          datal2|=(1<<k);
        } else if (32-j<=fval[fvalc+15-k])
          datal2|=(1<<k);
      }
      matrix[i][j]=(datah1<<8)+datal1;
      matrix[i+8][j]=(datah2<<8)+datal2;
    }
  }
}
