#ifndef _LCD_H_
#define _LCD_H_

/*
 * this is a general head file of LCD
 * use macro definition to choose which LCD to use
 * say:
 *     #define __USE_LCD1602_
 *     #include "LCD.h"
 * to include LCD1602 head file.
 * read respect head file for instructions in detail.
 */

#ifdef __USE_LCD12864_
#include "LCD12864.h"
#endif

#ifdef __USE_LCD1602_
#include "LCD1602.h"
#endif

#endif