#ifndef _USCISPI_H_
#define _USCISPI_H_
#include "msp430.h"
#include "USCI.h"
#include "General.h"

/* 
 * this is a head file of SPI mode, USCI.
 * 
 * the resources of different USCI modules with the same indexes DO conflict.
 * so if you are using this feature, and include this head file directly,
 *     please delete that statement, and include this functionality in a 
 *     safer way by inlcuding USCI.h:
 *     #define __USE_A0_SPI_
 *     #define __USE_B0_SPI_
 *     //other statements
 *     #include "USCI.h"
 * in this case, A0 SPI and B0 SPI DO actually conflict.
 * but in USCI.h, both pre-compilation selections will be processed,
 *     and disable the second one automatically to ensure the normal 
 *     performance of the SPI module.
 * 
 * Port Usage:
 * if __USE_A0_SPI_ defined:
 * P3.0 <- UCA0CLK
 * P3.4 <- UCA0SIMO
 * P3.5 <- UCA0SOMI
 * P3.3 <- UCA0STE
 *
 * if __USE_A1_SPI_ defined:
 * P5.0 <- UCA1CLK
 * P3.6 <- UCA1SIMO
 * P3.7 <- UCA1SOMI
 * P5.3 <- UCA1STE
 *
 * if __USE_B0_SPI_ defined:
 * P3.3 <- UCB0CLK
 * P3.1 <- UCB0SIMO
 * P3.2 <- UCB0SOMI
 * P3.0 <- UCB0STE
 *
 * if __USE_B1_SPI_ defined:
 * P5.3 <- UCB1CLK
 * P5.1 <- UCB1SIMO
 * P5.2 <- UCB1SOMI
 * P5.0 <- UCB1STE
 *
 * connecting illustration:
 *                           
 *         -----------------                   -----------------
 *        |             SIMO|---------------->|SIMO             |
 *        |    MASTER   SOMI|<----------------|SOMI  SLAVE      |
 *        |     MCU      CLK|---------------->|CLK    MCU       |
 *        |              STE|<----------------|STE              |
 *        |                 |(STE alternative)|                 |
 */

/************************ISR HANDLER DEFINITIONS***************************/
/* 
 * NOTICE:
 * the basic data transmitting and recieving functionalities have already implemented.
 * the handler interfaces are for some EXTRA operations.
 * if no, please leave them.
 *
 * considering the compatability of different modes, I constructed the framework 
 *     to be a little bit inefficient one to make the functionality match into 
 *     a few functions.
 * if you are lazy, my framework will absolutely work well.
 * if you have some requirements that my framework could not satisfy,
 *     use following pre-compilation selections:
 *     __Alternative_A0_SPI_
 *     __Alternative_A1_SPI_
 *     __Alternative_B0_SPI_
 *     __Alternative_B1_SPI_
 * to disinclude the pre-implemented ISR framework.
 * 
 * this mode is not so difficult as I2C. enable interrupt request by using:
 *     USCI_0IE_SET(USCI_ATXIE);
 *     USCI_0IE_SET(USCI_ARXIE);
 * or any other similar.
 */

#ifdef __USE_A0_SPI_
#ifndef __Alternative_A0_SPI_

uchar (*USCISPIA0TXHandler)();
uchar (*USCISPIA0RXHandler)();

#define USCISPI_A0_SET_TX_HANDLER(handler)    USCISPIA0TXHandler=(handler)
#define USCISPI_A0_RESET_TX_HANDLER           USCISPIA0TXHandler=ucblank
#define USCISPI_A0_SET_RX_HANDLER(handler)    USCISPIA0RXHandler=(handler)
#define USCISPI_A0_RESET_RX_HANDLER           USCISPIA0RXHandler=ucblank

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCISPIA0TXISR();

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCISPIA0RXISR();

#endif
#endif

#ifdef __USE_A1_SPI_
#ifndef __Alternative_A1_SPI_

uchar (*USCISPIA1TXHandler)();
uchar (*USCISPIA1RXHandler)();

#define USCISPI_A1_SET_TX_HANDLER(handler)    USCISPIA1TXHandler=(handler)
#define USCISPI_A1_RESET_TX_HANDLER           USCISPIA1TXHandler=ucblank
#define USCISPI_A1_SET_RX_HANDLER(handler)    USCISPIA1RXHandler=(handler)
#define USCISPI_A1_RESET_RX_HANDLER           USCISPIA1RXHandler=ucblank

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCISPIA1TXISR();

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCISPIA1RXISR();

#endif
#endif

#ifdef __USE_B0_SPI_
#ifndef __Alternative_B0_SPI_

uchar (*USCISPIB0TXHandler)();
uchar (*USCISPIB0RXHandler)();

#define USCISPI_B0_SET_TX_HANDLER(handler)    USCISPIB0TXHandler=(handler)
#define USCISPI_B0_RESET_TX_HANDLER           USCISPIB0TXHandler=ucblank
#define USCISPI_B0_SET_RX_HANDLER(handler)    USCISPIB0RXHandler=(handler)
#define USCISPI_B0_RESET_RX_HANDLER           USCISPIB0RXHandler=ucblank

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCISPIB0TXISR();

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCISPIB0RXISR();

#endif
#endif

#ifdef __USE_B1_SPI_
#ifndef __Alternative_B1_SPI_

uchar (*USCISPIB1TXHandler)();
uchar (*USCISPIB1RXHandler)();

#define USCISPI_B1_SET_TX_HANDLER(handler)    USCISPIB1TXHandler=(handler)
#define USCISPI_B1_RESET_TX_HANDLER           USCISPIB1TXHandler=ucblank
#define USCISPI_B1_SET_RX_HANDLER(handler)    USCISPIB1RXHandler=(handler)
#define USCISPI_B1_RESET_RX_HANDLER           USCISPIB1RXHandler=ucblank

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCISPIB1TXISR();

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCISPIB1RXISR();

#endif
#endif

/************************I2C GENERAL DEFINITIONS***************************/
#ifdef __USE_A0_SPI_
#define USCISPI_A0_SET_STE              SET_BIT(P3OUT,BIT3)
#define USCISPI_A0_CLR_STE              CLR_BIT(P3OUT,BIT3)
#define USCISPI_A0_INV_STE              INV_BIT(P3OUT,BIT3)
#endif

#ifdef __USE_A1_SPI_
#define USCISPI_A1_SET_STE              SET_BIT(P5OUT,BIT3)
#define USCISPI_A1_CLR_STE              CLR_BIT(P5OUT,BIT3)
#define USCISPI_A1_INV_STE              INV_BIT(P5OUT,BIT3)
#endif

#ifdef __USE_B0_SPI_
#define USCISPI_B0_SET_STE              SET_BIT(P3OUT,BIT0)
#define USCISPI_B0_CLR_STE              CLR_BIT(P3OUT,BIT0)
#define USCISPI_B0_INV_STE              INV_BIT(P3OUT,BIT0)
#endif

#ifdef __USE_B1_SPI_
#define USCISPI_B1_SET_STE              SET_BIT(P5OUT,BIT0)
#define USCISPI_B1_CLR_STE              CLR_BIT(P5OUT,BIT0)
#define USCISPI_B1_INV_STE              INV_BIT(P5OUT,BIT0)
#endif

/**********************UCAxCTL0/UCBxCTL0 MODIFIER**************************/
#define USCISPI_CAPTURE_FIRST           (0x80)
#define USCISPI_CLK_POLE_HIGH           (0x40)
#define USCISPI_MSB_FIRST               (0x20)
#define USCISPI_7BIT_DATA               (0x10)
#define USCISPI_MASTER                  (0x08)

typedef uchar USCISPI_SPIMode;
#define USCISPI_PIN_MODEBIT             (0x06)
#define USCISPI_3PIN                    (0x00)
#define USCISPI_4PIN_STE1_ENABLE        (0x02)
#define USCISPI_4PIN_STE0_ENABLE        (0x04)

#define USCISPI_SYNC                    (0x01)

/**********************UCAxCTL1/UCBxCTL1 MODIFIER**************************/
typedef uchar USCISPI_Clk;
#define USCISPI_UCLK                    (0x00)
#define USCISPI_SMCLK                   (0xC0)

#define USCISPI_RESET                   (0x01)

/***************************UCAxSTAT MODIFIER******************************/
#define USCISPI_SEND_LISTEN             (0x80)
#define USCISPI_CONFLICT                (0x40)
#define USCISPI_OVERRUN                 (0x20)
#define USCISPI_BUSY                    (0x01)

/************************INITIALIZER DEFINITIONS***************************/
/*
 * Introduction:
 * default:
 * UCAxCTL0/UCBxCTL0 <- Capture First, Pole Low, MSB First, 8-bit Data, Sync
 * UCAxCTL1/UCBxCTL1 <- SMCLK
 * UCAxBRW /UCBxBRW  <- 2
 */
/*
 * NOTICE:
 * call initializer before sending and recieving data.
 */
#ifdef __USE_A0_SPI_

void USCISPIA0MasterInit(USCISPI_SPIMode mode);
void USCISPIA0SlaveInit(USCISPI_SPIMode mode);

#ifndef __Alternative_A0_SPI_

/*
 * NOTICE:
 * these scheduling functions are all built in the framework.
 * these functions compat with different USCISPI_SPIMode initialized modules.
 * so no matter which mode you chose to initialize in, you can send data easily
 *     by calling these functions.
 */
void USCISPIA0Send(uchar* dataAddr,uint length);
void USCISPIA0SendByte(uchar data);

void USCISPIA0Recieve(uchar* dataAddr,uint length);

/*
 * Sending State Modifiers
 */
uint USCISPIA0GetActualSendedNum();
void USCISPIA0ResetSendCount();

/* 
 * Recieving State Modifiers
 */
uint USCISPIA0GetActualRecievedNum();
void USCISPIA0ResetRecieveCount();

#endif
#endif

/*
 * Introduction:
 * __USE_A1_SPI_ case is similar to __USE_A0_SPI_
 * read introduction above instead.
 */
#ifdef __USE_A1_SPI_

void USCISPIA1MasterInit(USCISPI_SPIMode mode);
void USCISPIA1SlaveInit(USCISPI_SPIMode mode);

#ifndef __Alternative_A1_SPI_

void USCISPIA1Send(uchar* dataAddr,uint length);
void USCISPIA1SendByte(uchar data);
void USCISPIA1Recieve(uchar* dataAddr,uint length);

uint USCISPIA1GetActualSendedNum();
void USCISPIA1ResetSendCount();

uint USCISPIA1GetActualRecievedNum();
void USCISPIA1ResetRecieveCount();

#endif
#endif

#ifdef __USE_B0_SPI_

void USCISPIB0MasterInit(USCISPI_SPIMode mode);
void USCISPIB0SlaveInit(USCISPI_SPIMode mode);

#ifndef __Alternative_B0_SPI_

void USCISPIB0Send(uchar* dataAddr,uint length);
void USCISPIB0SendByte(uchar data);
void USCISPIB0Recieve(uchar* dataAddr,uint length);

uint USCISPIB0GetActualSendedNum();
void USCISPIB0ResetSendCount();

uint USCISPIB0GetActualRecievedNum();
void USCISPIB0ResetRecieveCount();

#endif
#endif

#ifdef __USE_B1_SPI_

void USCISPIB1MasterInit(USCISPI_SPIMode mode);
void USCISPIB1SlaveInit(USCISPI_SPIMode mode);

#ifndef __Alternative_B1_SPI_

void USCISPIB1Send(uchar* dataAddr,uint length);
void USCISPIB1SendByte(uchar data);
void USCISPIB1Recieve(uchar* dataAddr,uint length);

uint USCISPIB1GetActualSendedNum();
void USCISPIB1ResetSendCount();

uint USCISPIB1GetActualRecievedNum();
void USCISPIB1ResetRecieveCount();

#endif
#endif

#include "USCISPI.c"

#endif
