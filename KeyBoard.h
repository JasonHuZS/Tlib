#ifndef _KeyBoard_H_
#define _KeyBoard_H_

/*
 * this is a general keyboard head file.
 * choose your keyboard usage like:
 *     #define __USE_PORT1_KB1X8_
 *     #include "KeyBoard.h"
 * to include the proper head file and implementation
 *further instruction and illustration, read respective head file.
 */

#define KB_ANTISHAKE_TIME     (0x1fff)
#define KB_ANTISHAKE          delay(KB_ANTISHAKE_TIME)

/*
 * precompilation inclusion hierachy:
 * PORT1    KB1X8
 * PORT2    KB1X8
 * PORT1    KB4X4
 * PORT2    KB4X4
 * auto-conflict-resolution
 */
#ifdef __USE_PORT1_KB1X8_
#define __USE_PORT1_KB_
#define __USE_KB1X8_
#endif

#ifdef __USE_PORT2_KB1X8_
#ifndef __USE_KB1X8_
#define __USE_PORT2_KB_
#define __USE_KB1X8_
#endif
#endif

#ifdef __USE_PORT1_KB4X4_
#ifndef __USE_PORT1_KB_
#define __USE_PORT1_KB_
#define __USE_KB4X4_
#endif
#endif

#ifdef __USE_PORT2_KB4X4_
#ifndef __USE_PORT2_KB_
#ifndef __USE_KB4X4_
#define __USE_PORT2_KB_
#define __USE_KB4X4_
#endif
#endif
#endif

#ifdef __USE_PORT1_KB_
#ifdef __USE_KB1X8_
#include "KeyBoard1X8.h"
#else
#ifdef __USE_KB4X4_
#include "KeyBoard4X4.h"
#endif
#endif
#endif

#ifdef __USE_PORT2_KB_
#ifdef __USE_KB1X8_
#include "KeyBoard1X8.h"
#else
#ifdef __USE_KB4X4_
#include "KeyBoard4X4.h"
#endif
#endif
#endif

#endif

//                            Hu Zhongsheng implemented, last update in 2013/7/19