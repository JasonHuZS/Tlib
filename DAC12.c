#include"DAC12.h"
/*
 * default:
 * DAC12_xCTL <- DAC12ENC
 * ref  -> reference voltage
 * refs -> reference voltage select
 * ir   -> input range
 * res  -> resolution
 * load -> load select
 * amp  -> amplifier setting
 */

//output on P6.6
void DAC12_0Init(DAC12_RefVol ref,DAC12_InputRange ir,
                 DAC12_Resolution res,DAC12_Load load,DAC12_Amplifier amp)
{
    DAC12_0INIT((ref),(ir),(res),(load),(amp));
}
void DAC12_0SimpleInit(void)
 {
   DAC12_0INIT(DAC12_VREF,DAC12_AV1,DAC12_12BIT,DAC12_WRITTEN,DAC12_IN_MED_OUT_MED);
 }

//output on P6.5
void DAC12_1Init(DAC12_RefVol ref,DAC12_InputRange ir,
                 DAC12_Resolution res,DAC12_Load load,DAC12_Amplifier amp)
{
    DAC12_1INIT((ref),(ir),(res),(load),(amp));
}
void DAC12_1SimpleInit(void)
 {
   DAC12_1INIT(DAC12_VREF,DAC12_AV1,DAC12_12BIT,DAC12_WRITTEN,DAC12_IN_MED_OUT_MED);
 }

void DAC12_0WriteData(uint data)
{
   DAC12_0DAT=data;
}


void DAC12_1WriteData(uint data)
{
  DAC12_1DAT=data;
}
      
#ifndef __Alternative_DAC12_
#pragma vector=DAC12_VECTOR
__interrupt void DAC12ISR(){
  _DINT();
  uchar result=0;
  if (DAC12_GET_IFG0){
    result=(*DAC12isr0Handler)();
    DAC12_CLR_IFG0;
  } else {
    result=(*DAC12isr1Handler)();
    DAC12_CLR_IFG1;
  }
  if (result) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}
#endif
