#ifndef _ADC12_H_
#define _ADC12_H_
#include "msp430.h"
#include "General.h"

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * handler may be not as efficient as required.
 * if efficiency is required, write a ISR function yourself.
 * before doing that, use macro directive to disinclude existed implementation:
 *     #define __Alternative_ADC12_
 *     //...statement...
 *     #pragma vector=ADC12_VECTOR
 *     __interrupt void MyDefinedADC12ISR(){}
 */

#ifndef __Alternative_ADC12_

uchar (*ADC12MemOverflowHandler)();
uchar (*ADC12CTOHandler)();         //Conversion Timer Overflow handler

#define ADC12_SET_MO_HANDLER(handler)     ADC12MemOverflowHandler=(handler)
#define ADC12_RESET_MO_HANDLER            ADC12MemOverflowHandler=ucblank
#define ADC12_SET_CTO_HANDLER(handler)    ADC12CTOHandler=(handler)
#define ADC12_RESET_CTO_HANDLER           ADC12CTOHandler=ucblank

uchar (*ADC12MemHandler)();

#define ADC12_SET_MEM_HANDLER(handler)   ADC12MemHandler=(handler)
#define ADC12_RESET_MEM_HANDLER          ADC12MemHandler=ucblank

#pragma vector=ADC12_VECTOR
__interrupt void ADC12ISR();
#endif

/*ADC12xCTL MACRO DEFINITIONS*/
#define ADC12_CTL0_SET(bitmap)        SET_BIT(ADC12CTL0,(bitmap))
#define ADC12_CTL0_CLR(bitmap)        CLR_BIT(ADC12CTL0,(bitmap))
#define ADC12_CTL0_ASN(bitmap)        ASN_BIT(ADC12CTL0,(bitmap))

#define ADC12_CTL1_SET(bitmap)        SET_BIT(ADC12CTL1,(bitmap))
#define ADC12_CTL1_CLR(bitmap)        CLR_BIT(ADC12CTL1,(bitmap))
#define ADC12_CTL1_ASN(bitmap)        ASN_BIT(ADC12CTL1,(bitmap))

#define ADC12_MCTL0_SET(bitmap)        SET_BIT(ADC12MCTL0,(bitmap))
#define ADC12_MCTL0_CLR(bitmap)        CLR_BIT(ADC12MCTL0,(bitmap))
#define ADC12_MCTL0_ASN(bitmap)        ASN_BIT(ADC12MCTL0,(bitmap))

#define ADC12_MCTL1_SET(bitmap)        SET_BIT(ADC12MCTL1,(bitmap))
#define ADC12_MCTL1_CLR(bitmap)        CLR_BIT(ADC12MCTL1,(bitmap))
#define ADC12_MCTL1_ASN(bitmap)        ASN_BIT(ADC12MCTL1,(bitmap))

#define ADC12_MCTL2_SET(bitmap)        SET_BIT(ADC12MCTL2,(bitmap))
#define ADC12_MCTL2_CLR(bitmap)        CLR_BIT(ADC12MCTL2,(bitmap))
#define ADC12_MCTL2_ASN(bitmap)        ASN_BIT(ADC12MCTL2,(bitmap))

#define ADC12_MCTL3_SET(bitmap)        SET_BIT(ADC12MCTL3,(bitmap))
#define ADC12_MCTL3_CLR(bitmap)        CLR_BIT(ADC12MCTL3,(bitmap))
#define ADC12_MCTL3_ASN(bitmap)        ASN_BIT(ADC12MCTL3,(bitmap))

#define ADC12_MCTL4_SET(bitmap)        SET_BIT(ADC12MCTL4,(bitmap))
#define ADC12_MCTL4_CLR(bitmap)        CLR_BIT(ADC12MCTL4,(bitmap))
#define ADC12_MCTL4_ASN(bitmap)        ASN_BIT(ADC12MCTL4,(bitmap))

#define ADC12_MCTL5_SET(bitmap)        SET_BIT(ADC12MCTL5,(bitmap))
#define ADC12_MCTL5_CLR(bitmap)        CLR_BIT(ADC12MCTL5,(bitmap))
#define ADC12_MCTL5_ASN(bitmap)        ASN_BIT(ADC12MCTL5,(bitmap))

#define ADC12_MCTL6_SET(bitmap)        SET_BIT(ADC12MCTL6,(bitmap))
#define ADC12_MCTL6_CLR(bitmap)        CLR_BIT(ADC12MCTL6,(bitmap))
#define ADC12_MCTL6_ASN(bitmap)        ASN_BIT(ADC12MCTL6,(bitmap))

#define ADC12_MCTL7_SET(bitmap)        SET_BIT(ADC12MCTL7,(bitmap))
#define ADC12_MCTL7_CLR(bitmap)        CLR_BIT(ADC12MCTL7,(bitmap))
#define ADC12_MCTL7_ASN(bitmap)        ASN_BIT(ADC12MCTL7,(bitmap))

#define ADC12_MCTL8_SET(bitmap)        SET_BIT(ADC12MCTL8,(bitmap))
#define ADC12_MCTL8_CLR(bitmap)        CLR_BIT(ADC12MCTL8,(bitmap))
#define ADC12_MCTL8_ASN(bitmap)        ASN_BIT(ADC12MCTL8,(bitmap))

#define ADC12_MCTL9_SET(bitmap)        SET_BIT(ADC12MCTL9,(bitmap))
#define ADC12_MCTL9_CLR(bitmap)        CLR_BIT(ADC12MCTL9,(bitmap))
#define ADC12_MCTL9_ASN(bitmap)        ASN_BIT(ADC12MCTL9,(bitmap))

#define ADC12_MCTL10_SET(bitmap)        SET_BIT(ADC12MCTL10,(bitmap))
#define ADC12_MCTL10_CLR(bitmap)        CLR_BIT(ADC12MCTL10,(bitmap))
#define ADC12_MCTL10_ASN(bitmap)        ASN_BIT(ADC12MCTL10,(bitmap))

#define ADC12_MCTL11_SET(bitmap)        SET_BIT(ADC12MCTL11,(bitmap))
#define ADC12_MCTL11_CLR(bitmap)        CLR_BIT(ADC12MCTL11,(bitmap))
#define ADC12_MCTL11_ASN(bitmap)        ASN_BIT(ADC12MCTL11,(bitmap))
  
#define ADC12_MCTL12_SET(bitmap)        SET_BIT(ADC12MCTL12,(bitmap))
#define ADC12_MCTL12_CLR(bitmap)        CLR_BIT(ADC12MCTL12,(bitmap))
#define ADC12_MCTL12_ASN(bitmap)        ASN_BIT(ADC12MCTL12,(bitmap))

#define ADC12_MCTL13_SET(bitmap)        SET_BIT(ADC12MCTL13,(bitmap))
#define ADC12_MCTL13_CLR(bitmap)        CLR_BIT(ADC12MCTL13,(bitmap))
#define ADC12_MCTL13_ASN(bitmap)        ASN_BIT(ADC12MCTL13,(bitmap))

#define ADC12_MCTL14_SET(bitmap)        SET_BIT(ADC12MCTL14,(bitmap))
#define ADC12_MCTL14_CLR(bitmap)        CLR_BIT(ADC12MCTL14,(bitmap))
#define ADC12_MCTL14_ASN(bitmap)        ASN_BIT(ADC12MCTL14,(bitmap))

#define ADC12_MCTL15_SET(bitmap)        SET_BIT(ADC12MCTL15,(bitmap))
#define ADC12_MCTL15_CLR(bitmap)        CLR_BIT(ADC12MCTL15,(bitmap))
#define ADC12_MCTL15_ASN(bitmap)        ASN_BIT(ADC12MCTL15,(bitmap))

#define ADC12_IE_SET(bitmap)            SET_BIT(ADC12IE,(bitmap))
#define ADC12_IE_CLR(bitmap)            CLR_BIT(ADC12IE,(bitmap))
#define ADC12_IE_ANS(bitmap)            ANS_BIT(ADC12IE,(bitmap))

#define ADC12_IFG_CLR(bitmap)           CLR_BIT(ADC12IFG,(bitmap))

#define ADC12_GET_IV                      (ADC12IV&(0x003e))

/*ADC12CTL0*/
//Sample-and-hold time for registers ADC12MEM8 to ADC12MEM15
//default: 4 ADC12CLK cycles.if you want to use others,you should set it yourself
#define ADC12_SHT1BIT          (0x0f000)
#define ADC12_SHT1_4CYCLE      ADC12_CTL0_SET(0x0000)
#define ADC12_SHT1_8CYCLE      ADC12_CTL0_SET(0x1000)
#define ADC12_SHT1_16CYCLE     ADC12_CTL0_SET(0x2000)
#define ADC12_SHT1_32CYCLE     ADC12_CTL0_SET(0x3000)
#define ADC12_SHT1_64CYCLE     ADC12_CTL0_SET(0x4000)
#define ADC12_SHT1_96CYCLE     ADC12_CTL0_SET(0x5000)
#define ADC12_SHT1_128CYCLE    ADC12_CTL0_SET(0x6000)
#define ADC12_SHT1_192CYCLE    ADC12_CTL0_SET(0x7000)
#define ADC12_SHT1_256CYCLE    ADC12_CTL0_SET(0x8000)
#define ADC12_SHT1_384CYCLE    ADC12_CTL0_SET(0x9000)
#define ADC12_SHT1_512CYCLE    ADC12_CTL0_SET(0xa000)
#define ADC12_SHT1_768CYCLE    ADC12_CTL0_SET(0xb000)
#define ADC12_SHT1_1024CYCLE   ADC12_CTL0_SET(0xc000)


//Sample-and-hold time for registers ADC12MEM0 to ADC12MEM7
//default: 4 ADC12CLK cycles.if you want to use others,you should set it yourself
#define ADC12_SHT0BIT          (0x0f00)
#define ADC12_SHT0_4CYCLE      ADC12_CTL0_SET(0x0000)
#define ADC12_SHT0_8CYCLE      ADC12_CTL0_SET(0x1000)
#define ADC12_SHT0_16CYCLE     ADC12_CTL0_SET(0x2000)
#define ADC12_SHT0_32CYCLE     ADC12_CTL0_SET(0x3000)
#define ADC12_SHT0_64CYCLE     ADC12_CTL0_SET(0x4000)
#define ADC12_SHT0_96CYCLE     ADC12_CTL0_SET(0x5000)
#define ADC12_SHT0_128CYCLE    ADC12_CTL0_SET(0x6000)
#define ADC12_SHT0_192CYCLE    ADC12_CTL0_SET(0x7000)
#define ADC12_SHT0_256CYCLE    ADC12_CTL0_SET(0x8000)
#define ADC12_SHT0_384CYCLE    ADC12_CTL0_SET(0x9000)
#define ADC12_SHT0_512CYCLE    ADC12_CTL0_SET(0xa000)
#define ADC12_SHT0_768CYCLE    ADC12_CTL0_SET(0xb000)
#define ADC12_SHT0_1024CYCLE   ADC12_CTL0_SET(0xc000)


//multiple sample and conversion
#define ADC12_MSCBIT           (0x0080)
#define ADC12_MSC_SHI_EACH     (0x0000)
#define ADC12_MSC_SHI_FIRS     (0x0080)


//interrupt enable
//overflow-interrupt enable
#define ADC12_SET_OVIE         ADC12_CTL0_SET(0x0008)
#define ADC12_CLR_OVIE         ADC12_CTL0_CLR(0x0008)

//conversion-time-overflow interrupt enable
#define ADC12_SET_TOVIE        ADC12_CTL0_SET(0x0004)
#define ADC12_CLR_TOVIE        ADC12_CTL0_CLR(0x0004)

//enable and start conversion
#define ADC12_START            ADC12_CTL0_SET(0x0003)
#define ADC12_STOP             ADC12_CTL0_CLR(0x0003)
#define ADC12_CLR_SC           ADC12_CTL0_CLR(0x0001)


/*ADC12CTL1*/
//conversion start address
#define ADC12_CHANNEL0_START         (0x0000)
#define ADC12_CHANNEL1_START         (0x1000)
#define ADC12_CHANNEL2_START         (0x2000)
#define ADC12_CHANNEL3_START         (0x3000)
#define ADC12_CHANNEL4_START         (0x4000)
#define ADC12_CHANNEL5_START         (0x5000)
#define ADC12_CHANNEL6_START         (0x6000)
#define ADC12_CHANNEL7_START         (0x7000)
#define ADC12_CHANNEL8_START         (0x8000)
#define ADC12_CHANNEL9_START         (0x9000)
#define ADC12_CHANNEL10_START        (0x0a000)
#define ADC12_CHANNEL11_START        (0x0b000)
#define ADC12_CHANNEL12_START        (0x0c000)
#define ADC12_CHANNEL13_START        (0x0d000)
#define ADC12_CHANNEL14_START        (0x0e000)
#define ADC12_CHANNEL15_START        (0x0f000)

//invert signal sample-and-hold
//default: not inwerted
#define ADC12_ISSHBIT          (0x0100)
#define ADC12_ISSH_NOT_INVERTED ADC12_CTL1_CLR(0x0100)
#define ADC12_ISSH_INVERTED     ADC12_CTL1_SET(0x0100)

//ADC12 clock divider
//default: /1
#define ADC12_DIV1              ADC12_CTL1_SET(0x0000)
#define ADC12_DIV2              ADC12_CTL1_SET(0x0020)
#define ADC12_DIV3              ADC12_CTL1_SET(0x0040)
#define ADC12_DIV4              ADC12_CTL1_SET(0x0060)
#define ADC12_DIV5              ADC12_CTL1_SET(0x0080)
#define ADC12_DIV6              ADC12_CTL1_SET(0x00a0)
#define ADC12_DIV7              ADC12_CTL1_SET(0x00c0)
#define ADC12_DIV8              ADC12_CTL1_SET(0x00e0)

//clock source select
#define ADC12_SSELBIT            (0x0018)
#define ADC12_SSEL_OSC           (0x0000)
#define ADC12_SSEL_ACLK          (0x0008)
#define ADC12_SSEL_MCLK          (0x0010)
#define ADC12_SSEL_SMCLK         (0x0018)

//sample-and-hold pulse-mode select
#define ADC12_SHPBIT           (0x0200)
#define ADC12_SHP_EXTENDED     (0x0000)
#define ADC12_SHP_PULSE        (0x0200)

//sample-and-hold source select
#define ADC12_SHSBIT           (0x0c00)
#define ADC12_SHS_SC           (0x0000)
#define ADC12_SHS_TIMERA_OUT1  (0x0400)
#define ADC12_SHS_TIMERB_OUT0  (0x0800)
#define ADC12_SHS_TIMERB_OUT1  (0x0c00)

//ADC12 BUSY
#define ADC12_GET_BUSYBIT        (ADC12CTL1&(0x0001))


/*ADC12MCTL*/
//conversion end address
#define ADC12_EOSBIT             (0x80)
#define ADC12_EOS_END            (0x80)
#define ADC12_CHANNEL0_END       (0x80)
#define ADC12_CHANNEL1_END       (0x81)
#define ADC12_CHANNEL2_END       (0x82)
#define ADC12_CHANNEL3_END       (0x83)
#define ADC12_CHANNEL4_END       (0x84)
#define ADC12_CHANNEL5_END       (0x85)
#define ADC12_CHANNEL6_END       (0x86)
#define ADC12_CHANNEL7_END       (0x87)
#define ADC12_CHANNEL8_END       (0x88)
#define ADC12_CHANNEL9_END       (0x89)
#define ADC12_CHANNEL10_END      (0x8a)
#define ADC12_CHANNEL11_END      (0x8b)
#define ADC12_CHANNEL12_END      (0x8c)
#define ADC12_CHANNEL13_END      (0x8d)
#define ADC12_CHANNEL14_END      (0x8e)
#define ADC12_CHANNEL15_END      (0x8f)

//select reference
#define ADC12_VREF_AVSS        (0x10)
#define ADC12_VEREF_AVSS       (0x20)
#define ADC12_VREF_VEREF       (0x50)
#define ADC12_VEREF_VEREF      (0x60)

/*renterrupt enable register*/
#define ADC12_CHANNEL0_IE     (0x0001)
#define ADC12_CHANNEL1_IE     (0x0002)
#define ADC12_CHANNEL2_IE     (0x0004)
#define ADC12_CHANNEL3_IE     (0x0008)
#define ADC12_CHANNEL4_IE     (0x0010)
#define ADC12_CHANNEL5_IE     (0x0020)
#define ADC12_CHANNEL6_IE     (0x0040)
#define ADC12_CHANNEL7_IE     (0x0080)
#define ADC12_CHANNEL8_IE     (0x0100)
#define ADC12_CHANNEL9_IE     (0x0200)
#define ADC12_CHANNEL10_IE    (0x0400)
#define ADC12_CHANNEL11_IE    (0x0800)
#define ADC12_CHANNEL12_IE    (0x1000)
#define ADC12_CHANNEL13_IE    (0x2000)
#define ADC12_CHANNEL14_IE    (0x4000)
#define ADC12_CHANNEL15_IE    (0x8000)

/*interrupt flag register*/
#define ADC12_CHANNEL0_IFG   (0x0001)
#define ADC12_CHANNEL1_IFG   (0x0002)
#define ADC12_CHANNEL2_IFG   (0x0004)
#define ADC12_CHANNEL3_IFG   (0x0008)
#define ADC12_CHANNEL4_IFG   (0x0010)
#define ADC12_CHANNEL5_IFG   (0x0020)
#define ADC12_CHANNEL6_IFG   (0x0040)
#define ADC12_CHANNEL7_IFG   (0x0080)
#define ADC12_CHANNEL8_IFG   (0x0100)
#define ADC12_CHANNEL9_IFG   (0x0200)
#define ADC12_CHANNEL10_IFG   (0x0400)
#define ADC12_CHANNEL11_IFG   (0x0800)
#define ADC12_CHANNEL12_IFG   (0x1000)
#define ADC12_CHANNEL13_IFG   (0x2000)
#define ADC12_CHANNEL14_IFG   (0x4000)
#define ADC12_CHANNEL15_IFG   (0x8000)




/***********************FUNCTION DECLARATIONS*************************/
void ADC12SingleChannelAndSingleConversion(uint csa,uint ssel,uint shp,uint shs,uint sref);
void ADC12SCSCSimple(uint csa);
void ADC12SequenceOfChannels(uint msc,uint csa,uint cea,uint ssel,uint shp,uint shs,uint sref);
void ADC12SCSimple(uint csa,uint cea);
void ADC12RepeatSingleChannel(uint msc,uint csa,uint ssel,uint shp,uint shs,uint sref);
void ADC12RSCSimple(uint csa);
void ADC12RepeatSequenceOfChannels(uint msc,uint csa,uint cea,uint ssel,uint shp,uint shs,uint sref);
void ADC12RSSimple(uint csa,uint cea);

#include "ADC12.c"
#endif


                                    //Ver. 0.75, all tested excepted VEREF
                                    //Yuanyuan Tan implemented, last update in 2013/8/2
