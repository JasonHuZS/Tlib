#include "USCISPI.h"

#ifdef __USE_A0_SPI_

void USCISPIA0MasterInit(USCISPI_SPIMode mode){
  USCI_A0CTL1_ASN(USCISPI_SMCLK+USCISPI_RESET);
  USCI_A0CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+USCISPI_MASTER+mode);
  USCI_A0BRW_ASN(2);
  SET_BIT(P3SEL,BIT0+BIT3+BIT4+BIT5);
  CLR_BIT(P3DIR,BIT3);
  #ifndef __Alternative_A0_SPI_
  USCISPI_A0_RESET_TX_HANDLER;
  USCISPI_A0_RESET_RX_HANDLER;
  #else
  USCI_0IE_SET(USCI_ATXIE+USCI_ARXIE);
  #endif
  USCI_A0CTL1_CLR(USCISPI_RESET);
}

void USCISPIA0SlaveInit(USCISPI_SPIMode mode){
  USCI_A0CTL1_ASN(USCISPI_RESET);
  USCI_A0CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+mode);
  SET_BIT(P3SEL,BIT0+BIT3+BIT4+BIT5);
  SET_BIT(P3DIR,BIT3);
  #ifndef __Alternative_A0_SPI_
  USCISPI_A0_RESET_TX_HANDLER;
  USCISPI_A0_RESET_RX_HANDLER;
  #else
  USCI_0IE_SET(USCI_ATXIE+USCI_ARXIE);
  #endif
  USCI_A0CTL1_CLR(USCISPI_RESET);
}

#ifndef __Alternative_A0_SPI_
static uchar* USCISPIA0sendFrom;
static uint USCISPIA0sendLength;
static uint USCISPIA0sendCount;

uint USCISPIA0GetActualSendedNum(){
  return USCISPIA0sendCount;
}

void USCISPIA0ResetSendCount(){
  USCISPIA0sendCount=0;
}

void USCISPIA0Send(uchar* dataAddr,uint length){
  int i;
  if (!(UCA0CTL0&USCISPI_MASTER))
    switch (UCA0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A0_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A0_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_ATXIE);
  USCISPIA0sendFrom=dataAddr;
  USCISPIA0sendLength=length;
  USCISPIA0sendCount=0;
  for(i=50;i>0;i--);
  if (USCISPIA0sendCount<USCISPIA0sendLength){
    USCI_A0TXBUF_ASN(USCISPIA0sendFrom[USCISPIA0sendCount]);
    USCISPIA0sendCount++;
  }
}

void USCISPIA0SendByte(uchar data){
  int i;
  if (!(UCA0CTL0&USCISPI_MASTER))
    switch (UCA0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A0_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A0_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_ATXIE);
  USCISPIA0sendFrom=NULL;
  USCISPIA0sendLength=1;
  USCISPIA0sendCount=1;
  for(i=50;i>0;i--);
  USCI_A0TXBUF_ASN(data);
}

static uchar* USCISPIA0recieveTo;
static uint USCISPIA0recieveLength;
static uint USCISPIA0recieveCount;

uint USCISPIA0GetActualRecievedNum(){
  return USCISPIA0recieveCount;
}

void USCISPIA0ResetRecieveCount(){
  USCISPIA0recieveCount=0;
}

void USCISPIA0Recieve(uchar* dataAddr,uint length){
  if (!(UCA0CTL0&USCISPI_MASTER))
    switch (UCA0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A0_SET_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A0_CLR_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_ARXIE);
  USCISPIA0recieveTo=dataAddr;
  USCISPIA0recieveLength=length;
  USCISPIA0recieveCount=0;
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCISPIA0TXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIA0sendCount<USCISPIA0sendLength){
    USCI_A0TXBUF_ASN(USCISPIA0sendFrom[USCISPIA0sendCount]);
    USCISPIA0sendCount++;
  }else {
    USCI_0IFG_CLR(USCI_ATXIFG);
  }
  res=(*USCISPIA0TXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCISPIA0RXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIA0recieveCount<USCISPIA0recieveLength){
    USCISPIA0recieveTo[USCISPIA0recieveCount]=USCI_A0RXBUF_GET;
    USCISPIA0recieveCount++;
  }else {
    USCI_0IFG_CLR(USCI_ARXIFG);
  }
  res=(*USCISPIA0RXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif
#endif

#ifdef __USE_A1_SPI_

void USCISPIA1MasterInit(USCISPI_SPIMode mode){
  USCI_A1CTL1_ASN(USCISPI_SMCLK+USCISPI_RESET);
  USCI_A1CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+USCISPI_MASTER+mode);
  USCI_A1BRW_ASN(2);
  SET_BIT(P3SEL,BIT6+BIT7);
  SET_BIT(P5SEL,BIT0+BIT3);
  CLR_BIT(P5DIR,BIT3);
  #ifndef __Alternative_A1_SPI_
  USCISPI_A1_RESET_TX_HANDLER;
  USCISPI_A1_RESET_RX_HANDLER;
  #else
  USCI_1IE_SET(USCI_ATXIE+USCI_ARXIE);
  #endif
  USCI_A1CTL1_CLR(USCISPI_RESET);
}

void USCISPIA1SlaveInit(USCISPI_SPIMode mode){
  USCI_A1CTL1_ASN(USCISPI_RESET);
  USCI_A1CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+mode);
  SET_BIT(P3SEL,BIT6+BIT7);
  SET_BIT(P5SEL,BIT0+BIT3);
  SET_BIT(P5DIR,BIT3);
  #ifndef __Alternative_A1_SPI_
  USCISPI_A1_RESET_TX_HANDLER;
  USCISPI_A1_RESET_RX_HANDLER;
  #else
  USCI_1IE_SET(USCI_ATXIE+USCI_ARXIE);
  #endif
  USCI_A1CTL1_CLR(USCISPI_RESET);
}

#ifndef __Alternative_A1_SPI_
static uchar* USCISPIA1sendFrom;
static uint USCISPIA1sendLength;
static uint USCISPIA1sendCount;

uint USCISPIA1GetActualSendedNum(){
  return USCISPIA1sendCount;
}

void USCISPIA1ResetSendCount(){
  USCISPIA1sendCount=0;
}

void USCISPIA1Send(uchar* dataAddr,uint length){
  int i;
  if (!(UCA1CTL0&USCISPI_MASTER))
    switch (UCA1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A1_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A1_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_ATXIE);
  USCISPIA1sendFrom=dataAddr;
  USCISPIA1sendLength=length;
  USCISPIA1sendCount=0;
  for(i=50;i>0;i--);
  if (USCISPIA1sendCount<USCISPIA1sendLength){
    USCI_A1TXBUF_ASN(USCISPIA1sendFrom[USCISPIA1sendCount]);
    USCISPIA1sendCount++;
  }
}

void USCISPIA1SendByte(uchar data){
  int i;
  if (!(UCA1CTL0&USCISPI_MASTER))
    switch (UCA1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A1_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A1_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_ATXIE);
  USCISPIA1sendFrom=NULL;
  USCISPIA1sendLength=1;
  USCISPIA1sendCount=1;
  for(i=50;i>0;i--);
  USCI_A1TXBUF_ASN(data);
}

static uchar* USCISPIA1recieveTo;
static uint USCISPIA1recieveLength;
static uint USCISPIA1recieveCount;

uint USCISPIA1GetActualRecievedNum(){
  return USCISPIA1recieveCount;
}

void USCISPIA1ResetRecieveCount(){
  USCISPIA1recieveCount=0;
}

void USCISPIA1Recieve(uchar* dataAddr,uint length){
  if (!(UCA1CTL0&USCISPI_MASTER))
    switch (UCA1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_A1_SET_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_A1_CLR_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_ARXIE);
  USCISPIA1recieveTo=dataAddr;
  USCISPIA1recieveLength=length;
  USCISPIA1recieveCount=0;
}

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCISPIA1TXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIA1sendCount<USCISPIA1sendLength){
    USCI_A1TXBUF_ASN(USCISPIA1sendFrom[USCISPIA1sendCount]);
    USCISPIA1sendCount++;
  }else {
    USCI_1IFG_CLR(USCI_ATXIFG);
  }
  res=(*USCISPIA1TXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCISPIA1RXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIA1recieveCount<USCISPIA1recieveLength){
    USCISPIA1recieveTo[USCISPIA1recieveCount]=USCI_A1RXBUF_GET;
    USCISPIA1recieveCount++;
  }else {
    USCI_1IFG_CLR(USCI_ARXIFG);
  }
  res=(*USCISPIA1RXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif
#endif

#ifdef __USE_B0_SPI_

void USCISPIB0MasterInit(USCISPI_SPIMode mode){
  USCI_B0CTL1_ASN(USCISPI_SMCLK+USCISPI_RESET);
  USCI_B0CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+USCISPI_MASTER+mode);
  USCI_B0BRW_ASN(2);
  SET_BIT(P3SEL,BIT0+BIT1+BIT2+BIT3);
  CLR_BIT(P3DIR,BIT0);
  #ifndef __Alternative_B0_SPI_
  USCISPI_B0_RESET_TX_HANDLER;
  USCISPI_B0_RESET_RX_HANDLER;
  #else
  USCI_0IE_SET(USCI_BTXIE+USCI_BRXIE);
  #endif
  USCI_B0CTL1_CLR(USCISPI_RESET);
}

void USCISPIB0SlaveInit(USCISPI_SPIMode mode){
  USCI_B0CTL1_ASN(USCISPI_RESET);
  USCI_B0CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+mode);
  SET_BIT(P3SEL,BIT0+BIT1+BIT2+BIT3);
  SET_BIT(P3DIR,BIT0);
  #ifndef __Alternative_B0_SPI_
  USCISPI_B0_RESET_TX_HANDLER;
  USCISPI_B0_RESET_RX_HANDLER;
  #else
  USCI_0IE_SET(USCI_BTXIE+USCI_BRXIE);
  #endif
  USCI_B0CTL1_CLR(USCISPI_RESET);
}

#ifndef __Alternative_B0_SPI_
static uchar* USCISPIB0sendFrom;
static uint USCISPIB0sendLength;
static uint USCISPIB0sendCount;

uint USCISPIB0GetActualSendedNum(){
  return USCISPIB0sendCount;
}

void USCISPIB0ResetSendCount(){
  USCISPIB0sendCount=0;
}

void USCISPIB0Send(uchar* dataAddr,uint length){
  int i;
  if (!(UCB0CTL0&USCISPI_MASTER))
    switch (UCB0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B0_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B0_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_BTXIE);
  USCISPIB0sendFrom=dataAddr;
  USCISPIB0sendLength=length;
  USCISPIB0sendCount=0;
  for(i=50;i>0;i--);
  if (USCISPIB0sendCount<USCISPIB0sendLength){
    USCI_B0TXBUF_ASN(USCISPIB0sendFrom[USCISPIB0sendCount]);
    USCISPIB0sendCount++;
  }
}

void USCISPIB0SendByte(uchar data){
  int i;
  if (!(UCB0CTL0&USCISPI_MASTER))
    switch (UCB0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B0_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B0_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_BTXIE);
  USCISPIB0sendFrom=NULL;
  USCISPIB0sendLength=1;
  USCISPIB0sendCount=1;
  for(i=50;i>0;i--);
  USCI_B0TXBUF_ASN(data);
}

static uchar* USCISPIB0recieveTo;
static uint USCISPIB0recieveLength;
static uint USCISPIB0recieveCount;

uint USCISPIB0GetActualRecievedNum(){
  return USCISPIB0recieveCount;
}

void USCISPIB0ResetRecieveCount(){
  USCISPIB0recieveCount=0;
}

void USCISPIB0Recieve(uchar* dataAddr,uint length){
  if (!(UCB0CTL0&USCISPI_MASTER))
    switch (UCB0CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B0_SET_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B0_CLR_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_0IE_SET(USCI_BRXIE);
  USCISPIB0recieveTo=dataAddr;
  USCISPIB0recieveLength=length;
  USCISPIB0recieveCount=0;
}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCISPIB0TXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIB0sendCount<USCISPIB0sendLength){
    USCI_B0TXBUF_ASN(USCISPIB0sendFrom[USCISPIB0sendCount]);
    USCISPIB0sendCount++;
  }else {
    USCI_0IFG_CLR(USCI_BTXIFG);
  }
  res=(*USCISPIB0TXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCISPIB0RXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIB0recieveCount<USCISPIB0recieveLength){
    USCISPIB0recieveTo[USCISPIB0recieveCount]=USCI_B0RXBUF_GET;
    USCISPIB0recieveCount++;
  }else {
    USCI_0IFG_CLR(USCI_BRXIFG);
  }
  res=(*USCISPIB0RXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif
#endif

#ifdef __USE_B1_SPI_

void USCISPIB1MasterInit(USCISPI_SPIMode mode){
  USCI_B1CTL1_ASN(USCISPI_SMCLK+USCISPI_RESET);
  USCI_B1CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+USCISPI_MASTER+mode);
  USCI_B1BRW_ASN(2);
  SET_BIT(P5SEL,BIT0+BIT1+BIT2+BIT3);
  CLR_BIT(P5DIR,BIT0);
  #ifndef __Alternative_B1_SPI_
  USCISPI_B1_RESET_TX_HANDLER;
  USCISPI_B1_RESET_RX_HANDLER;
  #else
  USCI_1IE_SET(USCI_BTXIE+USCI_BRXIE);
  #endif
  USCI_B1CTL1_CLR(USCISPI_RESET);
}

void USCISPIB1SlaveInit(USCISPI_SPIMode mode){
  USCI_B1CTL1_ASN(USCISPI_RESET);
  USCI_B1CTL0_ASN(USCISPI_CAPTURE_FIRST+USCISPI_MSB_FIRST+USCISPI_SYNC+mode);
  SET_BIT(P5SEL,BIT0+BIT1+BIT2+BIT3);
  SET_BIT(P5DIR,BIT0);
  #ifndef __Alternative_B1_SPI_
  USCISPI_B1_RESET_TX_HANDLER;
  USCISPI_B1_RESET_RX_HANDLER;
  #else
  USCI_1IE_SET(USCI_BTXIE+USCI_BRXIE);
  #endif
  USCI_B1CTL1_CLR(USCISPI_RESET);
}

#ifndef __Alternative_B1_SPI_
static uchar* USCISPIB1sendFrom;
static uint USCISPIB1sendLength;
static uint USCISPIB1sendCount;

uint USCISPIB1GetActualSendedNum(){
  return USCISPIB1sendCount;
}

void USCISPIB1ResetSendCount(){
  USCISPIB1sendCount=0;
}

void USCISPIB1Send(uchar* dataAddr,uint length){
  int i;
  if (!(UCB1CTL0&USCISPI_MASTER))
    switch (UCB1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B1_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B1_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_BTXIE);
  USCISPIB1sendFrom=dataAddr;
  USCISPIB1sendLength=length;
  USCISPIB1sendCount=0;
  for(i=50;i>0;i--);
  if (USCISPIB1sendCount<USCISPIB1sendLength){
    USCI_B1TXBUF_ASN(USCISPIB1sendFrom[USCISPIB1sendCount]);
    USCISPIB1sendCount++;
  }
}

void USCISPIB1SendByte(uchar data){
  int i;
  if (!(UCB1CTL0&USCISPI_MASTER))
    switch (UCB1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B1_CLR_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B1_SET_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_BTXIE);
  USCISPIB1sendFrom=NULL;
  USCISPIB1sendLength=1;
  USCISPIB1sendCount=1;
  for(i=50;i>0;i--);
  USCI_B1TXBUF_ASN(data);
}

static uchar* USCISPIB1recieveTo;
static uint USCISPIB1recieveLength;
static uint USCISPIB1recieveCount;

uint USCISPIB1GetActualRecievedNum(){
  return USCISPIB1recieveCount;
}

void USCISPIB1ResetRecieveCount(){
  USCISPIB1recieveCount=0;
}

void USCISPIB1Recieve(uchar* dataAddr,uint length){
  if (!(UCB1CTL0&USCISPI_MASTER))
    switch (UCB1CTL0&USCISPI_PIN_MODEBIT){
    case USCISPI_4PIN_STE0_ENABLE:
      USCISPI_B1_SET_STE;
      break;
    case USCISPI_4PIN_STE1_ENABLE:
      USCISPI_B1_CLR_STE;
      break;
    case USCISPI_3PIN:;
    }
  USCI_1IE_SET(USCI_BRXIE);
  USCISPIB1recieveTo=dataAddr;
  USCISPIB1recieveLength=length;
  USCISPIB1recieveCount=0;
}

#pragma vector=USCIAB1TX_VECTOR
__interrupt void USCISPIB1TXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIB1sendCount<USCISPIB1sendLength){
    USCI_B1TXBUF_ASN(USCISPIB1sendFrom[USCISPIB1sendCount]);
    USCISPIB1sendCount++;
  }else {
    USCI_1IFG_CLR(USCI_BTXIFG);
  }
  res=(*USCISPIB1TXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCISPIB1RXISR(){
  _DINT();
  uchar res=0;
  if (USCISPIB1recieveCount<USCISPIB1recieveLength){
    USCISPIB1recieveTo[USCISPIB1recieveCount]=USCI_B1RXBUF_GET;
    USCISPIB1recieveCount++;
  }else {
    USCI_1IFG_CLR(USCI_BRXIFG);
  }
  res=(*USCISPIB1RXHandler)();
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

#endif
#endif
