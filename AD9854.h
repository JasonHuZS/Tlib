#ifndef _AD9854_H_
#define _AD9854_H_

/*******************GENERAL INFOMATION DEFINITIONS************************/
#define AD9854_EXCLK                    (30000) //KHZ,external Clk
float AD9854sysClk=AD9854_EXCLK;

/********************CONTROL REGISTER BIT DEFINITIONS*********************/
//AD9854_PD 0x1D bits
#define AD9854_COMP                     (BIT4)
#define AD9854_QDAC                     (BIT2)
#define AD9854_DAC                      (BIT1)
#define AD9854_DIG                      (BIT0)

//AD9854_REFCLK 0x1E bits
#define AD9854_CLK_200MHZ               (BIT6)
#define AD9854_PLL_BYPASS               (BIT5)
#define AD9854_MUL_4                    (4)
#define AD9854_MUL_5                    (5)
#define AD9854_MUL_6                    (6)
#define AD9854_MUL_7                    (7)
#define AD9854_MUL_8                    (8)
#define AD9854_MUL_9                    (9)
#define AD9854_MUL_10                   (10)
#define AD9854_MUL_11                   (11)
#define AD9854_MUL_12                   (12)
#define AD9854_MUL_13                   (13)
#define AD9854_MUL_14                   (14)
#define AD9854_MUL_15                   (15)
#define AD9854_MUL_16                   (16)
#define AD9854_MUL_17                   (17)
#define AD9854_MUL_18                   (18)
#define AD9854_MUL_19                   (19)
#define AD9854_MUL_20                   (20)

//AD9854_MOD 0x1F bits
#define AD9854_CLRACC1                  (BIT7)
#define AD9854_CLRACC                   (BIT6)
#define AD9854_TRIANGLE                 (BIT5)
#define AD9854_QDACCH                   (BIT4)
#define AD9854_BPSK                     (0x08)
#define AD9854_CHIRP                    (0x06)
#define AD9854_RAMPED_FSK               (0x04)
#define AD9854_FSK                      (0x02)
#define AD9854_SINGLE_TONE              (0x00)
#define AD9854_INTERNEL_UP              (BIT0)

//AD9854_OSK_S 0x20 bits
#define AD9854_SINC_BYPASS              (BIT6)
#define AD9854_OSK_EN                   (BIT5)
#define AD9854_OSK_INT                  (BIT4)
#define AD9854_LSB_FIRST                (BIT1)
#define AD9854_SDO_ACT                  (BIT0)

#endif