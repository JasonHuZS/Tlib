#ifndef _Complex_H_
#define _Complex_H_

/*
 * this is a Complex head file.
 * this file implements complex number for common usage.
 * this is a pure software file.
 */

struct Complex_t{
  float real;
  float imag;
};
typedef struct Complex_t Complex;

/*
 * Special functions, don't use them without purposes
 */
Complex ComplexNew();
Complex ComplexGen(float realin,float imagin);

/*
 * general complex calculation function
 */
Complex ComplexMul(Complex a,Complex b);        // r=a*b
Complex ComplexAdd(Complex a,Complex b);        // r=a+b
Complex ComplexSub(Complex a,Complex b);        // r=a-b
Complex ComplexDiv(Complex a,Complex b);        // r=a/b
Complex ComplexConjugate(Complex a);
float ComplexSquare(Complex a);

/*
 * general complex macro
 */
/*
 * NOTICE:
 * try to use them in a absolutely safe way.
 */
#define COMPLEX_SET(rtn,re,im)    (rtn).real=(re),(rtn).imag=(im)
#define COMPLEX_MUL(rtn,a,b)      (rtn).real=(a).real*(b).real-(a).imag*(b).imag,\
                                  (rtn).imag=(a).real*(b).imag+(a).imag*(b).real
#define COMPLEX_ADD(rtn,a,b)      (rtn).real=(a).real+(b).real,\
                                  (rtn).imag=(a).imag+(b).imag
#define COMPLEX_SUB(rtn,a,b)      (rtn).real=(a).real-(b).real,\
                                  (rtn).imag=(a).imag-(b).imag
#define COMPLEX_CON(rtn,a)        (rtn).real=(a).real,\
                                  (rtn).imag=-(a).imag
#define COMPLEX_GEN(rtn,re,im)    (rtn).real=re,\
                                  (rtn).imag=im
#define COMPLEX_GET_REAL(a)        (a.real)
#define COMPLEX_GET_IMAG(a)        (a.imag)


#include "Complex.c"

#endif

//                            Ver. 1.0, all tested
//                            Hu Zhongsheng implemented, last update in 2013/7/18
