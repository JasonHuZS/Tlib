#include "KeyBoard4X4.h"

static int KB4X4Scan(int bitpos);

void KB4X4Init(){
  int i,j;
  for (i=0;i<3;i++)
    for (j=0;j<3;j++)
      KB4X4handler[i][j]=ucblank;
  KB4X4errorHandler=ucblank;
  ASN_BIT(KB4X4_DIR,0xf0);
  CLR_BIT(KB4X4_SEL,0xff);
  SET_BIT(KB4X4_IE,0x0f);
  SET_BIT(KB4X4_IES,0x0f);
  CLR_BIT(KB4X4_IFG,0x0f);
  CLR_BIT(KB4X4_OUT,0xf0);
}

void KB4X4ResetHandler(int col,int row){
  KB4X4handler[col][row]=ucblank;
}

void KB4X4SetHandler(int col,int row,uchar (*handler)()){
  KB4X4handler[col][row]=handler;
}

void KB4X4ResetErrorHandler(){
  KB4X4errorHandler=ucblank;
}

void KB4X4SetErrorHandler(uchar (*handler)()){
  KB4X4errorHandler=handler;
}

void KB4X4SetHandlerGroup(uchar (*handler00)(),uchar (*handler01)(),uchar (*handler02)(),uchar (*handler03)(),
                          uchar (*handler10)(),uchar (*handler11)(),uchar (*handler12)(),uchar (*handler13)(),
                          uchar (*handler20)(),uchar (*handler21)(),uchar (*handler22)(),uchar (*handler23)(),
                          uchar (*handler30)(),uchar (*handler31)(),uchar (*handler32)(),uchar (*handler33)()){
  KB4X4_SET_HANDLER(0,0,handler00);
  KB4X4_SET_HANDLER(0,1,handler01);
  KB4X4_SET_HANDLER(0,2,handler02);
  KB4X4_SET_HANDLER(0,3,handler03);
  KB4X4_SET_HANDLER(1,0,handler10);
  KB4X4_SET_HANDLER(1,1,handler11);
  KB4X4_SET_HANDLER(1,2,handler12);
  KB4X4_SET_HANDLER(1,3,handler13);
  KB4X4_SET_HANDLER(2,0,handler20);
  KB4X4_SET_HANDLER(2,1,handler21);
  KB4X4_SET_HANDLER(2,2,handler22);
  KB4X4_SET_HANDLER(2,3,handler23);
  KB4X4_SET_HANDLER(3,0,handler30);
  KB4X4_SET_HANDLER(3,1,handler31);
  KB4X4_SET_HANDLER(3,2,handler32);
  KB4X4_SET_HANDLER(3,3,handler33);
  }

#pragma vector= KB4X4_INT_VEC
__interrupt void KB4X4ISR(){
  _DINT();
  int scanResult=-1;
  uchar res=0,way;
  DELAY_1MS;
  DELAY_1MS;
  way=bit2index(KB4X4_IFG&0x0f);
  switch(way){
  case 0:
    if ((KB4X4_IN&BIT0)==0){
      scanResult=KB4X4Scan(BIT0);
      if (scanResult==-1)
        res=(*KB4X4errorHandler)();
      else
        res=(*KB4X4handler[0][scanResult])();
    }
    break;
    
  case 1:
    if ((KB4X4_IN&BIT1)==0){
      scanResult=KB4X4Scan(BIT1);
      if (scanResult==-1)
        res=(*KB4X4errorHandler)();
      else
        res=(*KB4X4handler[1][scanResult])();
    }
    break;
    
  case 2:
    if ((KB4X4_IN&BIT2)==0){
      scanResult=KB4X4Scan(BIT2);
      if (scanResult==-1)
        res=(*KB4X4errorHandler)();
      else
        res=(*KB4X4handler[2][scanResult])();
    }
    break;
    
  case 3:
    if ((KB4X4_IN&BIT3)==0){
      scanResult=KB4X4Scan(BIT3);
      if (scanResult==-1)
        res=(*KB4X4errorHandler)();
      else
        res=(*KB4X4handler[3][scanResult])();
    }
    break;
    
  default:
    res=(*KB4X4errorHandler)();
  }
  CLR_BIT(KB4X4_IFG,0x0f);
  if (res) __bic_SR_register_on_exit(CPUOFF);
  __bis_SR_register_on_exit(GIE);
}

static int KB4X4Scan(int bitpos){
  if ((KB4X4_IN&bitpos)!=0)
    return -1;
  
  SET_BIT(KB4X4_OUT,0x10);
  if ((KB4X4_IN&bitpos)!=0){
    CLR_BIT(KB4X4_OUT,0xf0);
    return 0;
  }

  SET_BIT(KB4X4_OUT,0x20);
  if ((KB4X4_IN&bitpos)!=0){
    CLR_BIT(KB4X4_OUT,0xf0);
    return 1;
  }

  SET_BIT(KB4X4_OUT,0x40);
  if ((KB4X4_IN&bitpos)!=0){
    CLR_BIT(KB4X4_OUT,0xf0);
    return 2;
  }

  SET_BIT(KB4X4_OUT,0x80);
  if ((KB4X4_IN&bitpos)!=0){
    CLR_BIT(KB4X4_OUT,0xf0);
    return 3;
  }
  
  return -1;
}
