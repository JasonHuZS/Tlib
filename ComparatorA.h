#ifndef _ComparatorA_H_
#define _ComparatorA_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a Comparator A head file.
 * this head file encapsulates all features of Comparator A in MSP430.
 * to understand how to use this lib, read the annotation in the context.
 *
 * User Defined Port Usage:
 * P1.0 -> OUT: CAOUT
 * P2.3 -> in: CA0
 * P2.4 -> in: CA1
 * P2.0 -> in: CA2
 * P2.1 -> in: CA3
 * P2.2 -> in: CA4  out: CAOUT
 * P2.5 -> in: CA5
 * P2.6 -> in: CA6
 * P2.7 -> in: CA7
 */

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * handler may be not as efficient as required.
 * if efficiency is required, write a ISR function yourself.
 * before doing that, use macro directive to disinclude existed implementation:
 *     #define __Alternative_ComparatorA_
 *     //...statement...
 *     #pragma vector=COMPARATORA_VECTOR
 *     __interrupt void MyDefinedCompAISR(){}
 */
/*
 * Introduction:
 * The interrupt flag CAIFG is set on either the rising or falling edge 
 *     of the comparator output, selected by the CAIES bit.
 * try to use:
 *     COMPA_FALLING_EDGE;
 * or:
 *     COMPA_RISING_EDGE;
 * to make things easier.
 */
#ifndef __Alternative_ComparatorA_

uchar (*CompAisrHandler)();

#define COMPA_SET_HANDLER(handler)  CompAisrHandler=(handler)
#define COMPA_RESET_HANDLER         CompAisrHandler=ucblank

#pragma vector=COMPARATORA_VECTOR
__interrupt void CompAISR();
#endif

/***********************GENERAL MACRO DEFINITIONS**************************/
#define COMPA_SET_CTL1(bitmap)        SET_BIT(CACTL1,(bitmap))
#define COMPA_CLR_CTL1(bitmap)        CLR_BIT(CACTL1,(bitmap))
#define COMPA_ASN_CTL1(bitmap)        ASN_BIT(CACTL1,(bitmap))

#define COMPA_SET_CTL2(bitmap)        SET_BIT(CACTL2,(bitmap))
#define COMPA_CLR_CTL2(bitmap)        CLR_BIT(CACTL2,(bitmap))
#define COMPA_ASN_CTL2(bitmap)        ASN_BIT(CACTL2,(bitmap))

#define COMPA_SET_PD(bitmap)          SET_BIT(CAPD,(bitmap))
#define COMPA_CLR_PD(bitmap)          CLR_BIT(CAPD,(bitmap))
#define COMPA_ASN_PD(bitmap)          ASN_BIT(CAPD,(bitmap))

/***************************CACTL1 MODIFIERS*******************************/
//Switch
#define COMPA_ON                      COMPA_SET_CTL1(CAON)
#define COMPA_OFF                     COMPA_CLR_CTL1(CAON)

//Reference Control
#define COMPA_EXCHANGE                COMPA_SET_CTL1(CAEX)
#define COMPA_NONEXCHANGE             COMPA_CLR_CTL1(CAEX)
/*
 * Introduction:
 * if ComparatorA is NONEXCHANGE:
 *     Vcaref <- the terminal applied
 * else:
 *     Vcaref <- the other terminal applied
 */
#define COMPA_IREF_PLUS               COMPA_CLR_CTL1(CARSEL)
#define COMPA_IREF_MINUS              COMPA_SET_CTL1(CARSEL)
#define COMPA_EXTERNAL                COMPA_CLR_CTL1(0x30)
#define COMPA_QUARTER_VCC             COMPA_CLR_CTL1(0x30);COMPA_SET_CTL1(0x10)
#define COMPA_HALF_VCC                COMPA_CLR_CTL1(0x30);COMPA_SET_CTL1(0x20)
#define COMPA_DIODE_REF               COMPA_SET_CTL1(0x30)

//Interrupt Control
#define COMPA_FALLING_EDGE            COMPA_SET_CTL1(CAIES)
#define COMPA_RISING_EDGE             COMPA_CLR_CTL1(CAIES)
#define COMPA_SET_CAIE                COMPA_SET_CTL1(CAIE)
#define COMPA_CLR_CAIE                COMPA_CLR_CTL1(CAIE)
#define COMPA_CLR_CAIFG               COMPA_CLR_CTL1(CAIFG)

/***************************CACTL2 MODIFIERS*******************************/
//Input Short
#define COMPA_SHORTED                 COMPA_SET_CTL2(CASHORT)
#define COMPA_UNSHORTED               COMPA_CLR_CTL2(CASHORT)

//Input Select
/*
 * NOTICE:
 * if comparatorA is EXCHANGE, consider the following inputs inversely.
 */
#define COMPA_PLUS_NO_CONNECT         COMPA_CLR_CTL2(0x44)
#define COMPA_PLUS_CA0                COMPA_CLR_CTL2(0x44);COMPA_SET_CTL2(0x04)
#define COMPA_PLUS_CA1                COMPA_CLR_CTL2(0x44);COMPA_SET_CTL2(0x40)
#define COMPA_PLUS_CA2                COMPA_SET_CTL2(0x44)

#define COMPA_MINUS_NO_CONNECT        COMPA_CLR_CTL2(0x38)
#define COMPA_MINUS_CA1               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x08)
#define COMPA_MINUS_CA2               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x10)
#define COMPA_MINUS_CA3               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x18)
#define COMPA_MINUS_CA4               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x20)
#define COMPA_MINUS_CA5               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x28)
#define COMPA_MINUS_CA6               COMPA_CLR_CTL2(0x38);COMPA_SET_CTL2(0x30)
#define COMPA_MINUS_CA7               COMPA_SET_CTL2(0x38)

//Others
#define COMPA_OUTPUT_FILTER_ON        COMPA_SET_CTL2(CAF)
#define COMPA_OUTPUT_FILTER_OFF       COMPA_CLR_CTL2(CAF)

#define COMPA_GET_CAOUT               (CACTL2&BIT0)

/**********************PORTS&CAPD MODIFIERS********************************/
#define COMPA_CAPD_DISABLE(ports)     COMPA_SET_PD((ports))
#define COMPA_CAPD_ENABLE(ports)      COMPA_CLR_PD((ports))
#define COMPA_CAPD_ALL_DISABLE        COMPA_CAPD_DISABLE(0xFF)
#define COMPA_CAPD_ALL_ENABLE         COMPA_CAPD_ENABLE(0xFF)

//Port Initialization
#define COMPA_IN_INIT(bitpos)         CLR_BIT(P2DIR,(bitpos));SET_BIT(P2SEL,(bitpos));\
                                      COMPA_CAPD_DISABLE((bitpos))
#define COMPA_P1_OUT_INIT             SET_BIT(P1DIR,BIT0);SET_BIT(P1SEL,BIT0)
#define COMPA_P2_OUT_INIT             SET_BIT(P2DIR,BIT2);SET_BIT(P2SEL,BIT2)

/**********************INITIALIZATION DEFINITIONS**************************/
/*
 * NOTICE:
 * even after you have initialized ComparatorA, this module still won't work!
 * turn ComparatorA on using:
 *     COMPA_ON;
 * when you are not using it for a certain long time, remember to turn it off:
 *     COMPA_OFF;
 * to reduce current consumption.
 *
 * and NO OUTPUT PORT INITIALIZED! if you do want, use macro:
 *     COMPA_P1_OUT_INIT;
 * or:
 *     COMPA_P2_OUT_INIT;
 *
 * last, if the initialization processes do not match your real requirement,
 *     you can use modifiers above or write your own one.
 * if you chose to write your own one, do NOT use processes below!
 */

/*
 * NOTICE:
 * caplus and caminus should be index of CA.
 * caplus should be 0~2 and caminus should be 1~7.
 */

/*
 * default:
 * CACTL1 <- External Reference, Rising Edge, CAIE
 * CACTL2 <- output filter on
 * CAPD   <- the input buffers of using ports disabled.
 */
void CompAExternalInit(uchar caplus,uchar caminus);
#define COMPA_EXTERNAL_INIT(caplus,caminus) CompAExternalInit((caplus),(caminus))

/*
 * default:
 * CACTL1 <- 0x25*Vcc, Vcaref to + terminal, Rising Edge, CAIE
 * CACTL2 <- output filter on
 * CAPD   <- the input buffers of using ports disabled.
 */
void CompAInternalInit(uchar caminus);
#define COMPA_INTERNAL_INIT(caminus) CompAInternalInit((caminus))

#include "ComparatorA.c"

#endif

//                            Ver. 0.2, all untested
//                            Hu Zhongsheng implemented, last update in 2013/7/26
