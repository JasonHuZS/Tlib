#ifndef _TimerA_H_
#define _TimerA_H_
#include "msp430.h"
#include "General.h"

/*
 * this is a TimerA head file
 * 
 * User-defined Port Usage:
 * P1.1 -> out: TA0 in: CCI0A
 * P1.2 -> out: TA1 in: CCI1A
 * P1.3 -> out: TA2 in: CCI2A
 * P1.5 -> out: TA0
 * P1.6 -> out: TA1
 * P1.7 -> out: TA2
 * P2.2 -> out: TA0 in: CCI0B
 * P2.3 -> out: TA1
 * P2.4 -> out: TA2
 * P2.7 -> out: TA0
 * P4.0 -> in: CCI0A/B
 * P4.1 -> in: CCI1A/B
 * P4.2 -> in: CCI2A/B
 * P1.0 -> in: TACLK
 * P2.1 -> in: TAINCLK
 */

/*
 * NOTICE:
 * once TIMERA_TACLR is used. TimerA needs to be initialized again!
 * modify proper system clock using SysClk.h before use this lib.
 * if precision is required, using XT2 is required.
 */

/************************ISR HANDLER DEFINITIONS**************************/
/*
 * handler may be not as efficient as required.
 * if efficiency is required, write a ISR function yourself.
 * before doing that, use macro directive to disinclude existed implementation:
 *     #define __Alternative_TimerA_0_
 *     //...statement...
 *     #pragma vector=TIMERA0_VECTOR
 *     __interrupt void MyDefinedTimerA0ISR(){}
 */

#ifndef __Alternative_TimerA_0_         //0xFFF2 Timer A CC0

uchar (*TimerAisr0Handler)();

#define TIMERA_SET_HANDLER0(handler) TimerAisr0Handler=(handler)
#define TIMERA_RESET_HANDLER0        TimerAisr0Handler=ucblank

#pragma vector=TIMERA0_VECTOR
__interrupt void TimerA0ISR();
#endif

#ifndef __Alternative_TimerA_1_         //0xFFF0 Timer A CC1-2, TA

uchar (*TimerACC1Handler)();
uchar (*TimerACC2Handler)();
uchar (*TimerATAHandler)();

#define TIMERA_SET_CC1_HANDLER(handler) TimerACC1Handler=(handler)
#define TIMERA_RESET_CC1_HANDLER        TimerACC1Handler=ucblank
#define TIMERA_SET_CC2_HANDLER(handler) TimerACC2Handler=(handler)
#define TIMERA_RESET_CC2_HANDLER        TimerACC2Handler=ucblank
#define TIMERA_SET_TA_HANDLER(handler)  TimerATAHandler=(handler)
#define TIMERA_RESET_TA_HANDLER         TimerATAHandler=ucblank

#pragma vector=TIMERA1_VECTOR
__interrupt void TimerA1ISR();
#endif

/***********************GENERAL MACRO DEFINITIONS**************************/
#define TIMERA_TA_INIT(port,bitpos)   SET_BIT((port##DIR),(bitpos));\
                                      SET_BIT((port##SEL),(bitpos))
#define TIMERA_CCI_INIT(port,bitpos)  CLR_BIT((port##DIR),(bitpos));\
                                      SET_BIT((port##SEL),(bitpos))
/*
 * Introduction:
 * this macro is a little bit tricky. use the above two macros as follow:
 *     TIMERA_TA_INIT(P1,BIT0)
 */

#define TIMERA_START                  TimerAStart()
#define TIMERA_PAUSE                  TimerAPause()

/***************************TACTL MODIFIER*********************************/
//CLK Control
#define TIMERA_TACLK                CLR_BIT(P1DIR,BIT0);SET_BIT(P1SEL,BIT0);\
                                    CLR_BIT(TACTL,0x0300)
#define TIMERA_SMCLK                CLR_BIT(TACTL,0x0300);SET_BIT(TACTL,0x0200)
#define TIMERA_INCLK                CLR_BIT(P2DIR,BIT1);SET_BIT(P2SEL,BIT1);\
                                    SET_BIT(TACTL,0x0300)
#define TIMERA_DIVISOR(div)         CLR_BIT(TACTL,0x00C0);\
                                    SET_BIT(TACTL,((bit2index((div)))<<6))
                                    //div should and only should be 1 2 4 8

//Interrupt Control
#define TIMERA_TACLR                SET_BIT(TACTL,TACLR)
#define TIMERA_SET_TAIE             SET_BIT(TACTL,TAIE)
#define TIMERA_CLR_TAIE             CLR_BIT(TACTL,TAIE)
#define TIMERA_CLR_TAIFG            CLR_BIT(TACTL,TAIFG)

//Mode Control
typedef uint TimerA_ModeCtrl;
#define TIMERA_MCBIT                (0x0030)
#define TIMERA_STOP                 (0x0000)
#define TIMERA_UP                   (0x0010)
#define TIMERA_CONTINUE             (0x0020)
#define TIMERA_UP_DOWN              (0x0030)
#define TIMERA_SET_STOP             CLR_BIT(TACTL,0x0030)
#define TIMERA_SET_UP               CLR_BIT(TACTL,0x0030);SET_BIT(TACTL,0x0010)
#define TIMERA_SET_CONTINUE         CLR_BIT(TACTL,0x0030);SET_BIT(TACTL,0x0020)
#define TIMERA_SET_UP_DOWN          SET_BIT(TACTL,0x0030)

//there are 3 TACCTLx, 0 1 2
/**************************TACCTLx MODIFIER********************************/
#define TIMERA_CCTL0_SET(bitmap)    SET_BIT(TACCTL0,(bitmap))
#define TIMERA_CCTL0_CLR(bitmap)    CLR_BIT(TACCTL0,(bitmap))

#define TIMERA_CCTL1_SET(bitmap)    SET_BIT(TACCTL1,(bitmap))
#define TIMERA_CCTL1_CLR(bitmap)    CLR_BIT(TACCTL1,(bitmap))

#define TIMERA_CCTL2_SET(bitmap)    SET_BIT(TACCTL2,(bitmap))
#define TIMERA_CCTL2_CLR(bitmap)    CLR_BIT(TACCTL2,(bitmap))

//Output Mode
/*
 * TR -> Toggle/Reset
 * SR -> Set/Reset
 * TS -> Toggle/Set
 * RS -> Reset/Set
 */
typedef uint TimerA_OutputM;
#define TIMERA_OMBIT                (0x00E0)
#define TIMERA_OUT                  (0x0000)
#define TIMERA_SET                  (0x0020)
#define TIMERA_TR                   (0x0040)
#define TIMERA_SR                   (0x0060)
#define TIMERA_TOGGLE               (0x0080)
#define TIMERA_RESET                (0x00A0)
#define TIMERA_TS                   (0x00C0)
#define TIMERA_RS                   (0x00E0)

//Capture Mode
typedef uint TimerA_CaptureM;
#define TIMERA_CMBIT                (0xC000)
#define TIMERA_NOCAP                (0x0000)
#define TIMERA_RISING               (0x4000)
#define TIMERA_FALLING              (0x8000)
#define TIMERA_BOTH                 (0xC000)

//Capture/Compare Input Select
typedef uint TimerA_CCInput;
#define TIMERA_CCISBIT              (0x3000)
#define TIMERA_CCIA                 (0x0000)
#define TIMERA_CCIB                 (0x1000)
#define TIMERA_GND                  (0x2000)
#define TIMERA_VCC                  (0x3000)

//Others
#define TIMERA_SCS                  (0x0800)
#define TIMERA_SCCI                 (0x0400)
#define TIMERA_CAP                  (0x0100)
#define TIMERA_CCI                  (0x0008)
#define TIMERA_OUTPUT               (0x0004)

//Interrupt&Overflow Control
#define TIMERA_SET_CC0IE            SET_BIT(TACCTL0,CCIE)
#define TIMERA_CLR_CC0IE            CLR_BIT(TACCTL0,CCIE)
#define TIMERA_CLR_CC0IFG           CLR_BIT(TACCTL0,CCIFG)
#define TIMERA_GET_CC0COV           (TACCTL0&COV)
#define TIMERA_CLR_CC0COV           CLR_BIT(TACCTL0,COV)

#define TIMERA_SET_CC1IE            SET_BIT(TACCTL1,CCIE)
#define TIMERA_CLR_CC1IE            CLR_BIT(TACCTL1,CCIE)
#define TIMERA_CLR_CC1IFG           CLR_BIT(TACCTL1,CCIFG)
#define TIMERA_GET_CC1COV           (TACCTL1&COV)
#define TIMERA_CLR_CC1COV           CLR_BIT(TACCTL1,COV)

#define TIMERA_SET_CC2IE            SET_BIT(TACCTL2,CCIE)
#define TIMERA_CLR_CC2IE            CLR_BIT(TACCTL2,CCIE)
#define TIMERA_CLR_CC2IFG           CLR_BIT(TACCTL2,CCIFG)
#define TIMERA_GET_CC2COV           (TACCTL2&COV)
#define TIMERA_CLR_CC2COV           CLR_BIT(TACCTL2,COV)

/***************************TACCRx MODIFIER********************************/
#define TIMERA_CCR0_ASN(value)      ASN_BIT(TACCR0,(value))
#define TIMERA_CCR1_ASN(value)      ASN_BIT(TACCR1,(value))
#define TIMERA_CCR2_ASN(value)      ASN_BIT(TACCR2,(value))

/*****************************TAIV MODIFIER********************************/
#define TIMERA_TAIV_1IFG            (0x0002)
#define TIMERA_TAIV_2IFG            (0x0004)
#define TIMERA_TAIV_TAIFG           (0x000A)


/*********************GENERAL FUNCTION DEFINITIONS*************************/
/*
 * TimerA is too difficult to encapsulate well, so the following initializing 
 *     func.s may or may not match your requirement.
 * if not, use modifier above additionally or write your own version.
 * if you choose to define your own version, do not use functions below.
 */
/*
 * NOTICE:
 * Port is not initialized. do port initialization yourself.
 * hint: try to use following macro:
 *     TIMERA_TA_INIT(port,bitpos)
 *     TIMERA_CCI_INIT(port,bitpos)
 * Introduction:
 * this macro is a little bit tricky. use the above two macros as follow:
 *     TIMERA_TA_INIT(P1,BIT0)
 */

//call this function when you want to start timing
void TimerAStart();
//call this function when you want to stop timing
void TimerAPause();

/*
 * default: 
 * TACTL <- SMCLK, /1
 * TACCTLx <- No Capture
 */
void TimerACompareInit(TimerA_ModeCtrl mcbit,       //TACTL mode control bits
                       uint tacctl_num,             //number of used TACCTLx
                       ...);                        //TACCTLx control bits, x <- 0,1,2
//control bits sequence: 
//    (uint)value of TACCRx, (TimerA_OutputM)output mode control bits

/*
 * default: 
 * TACTL <- SMCLK, /1
 * TACCTLx <- CCIA, SCS, CAP
 */
void TimerACaptureInit(TimerA_ModeCtrl mcbit,       //TACTL mode control bits
                       uint tacctl_num,             //number of used TACCTLx
                       ...);                        //TACCTLx control bits, x <- 0,1,2
//control bits sequence: 
//    (uint)value of TACCRx, (TimerA_CaptureM)capture mode control bits

/*
 * NOTICE AGAIN:
 * I found that when calling functions TimerACompareInit() and TimerACaptureInit(),
 *     if parameter(s) '(uint)value of TACCRx' is(are) larger than or equal to 32768,
 *     which is 0x8000 in hexadecimal, the compiler will recognise this number as a 
 *     32-bit long integer, instead of a 16-bit long one, since compiler presumes all
 *     integers are signed.
 * in this case, the stack is corrupted by compiler, so that the result is no longer 
 *     correct.
 * I tried to fix this problem but well, I didnot come up with a good way to handle this.
 * However, I do have a explicit way to avoid this:
 *     TimerACompareInit(TIMERA_UP,1,(uint)0x8000,TIMERA_OUT);
 * in this way, you tell the compiler that number is in uint type, without regarding 
 *     it as a signed integer.
 *
 *
 * variable length argument is introduced, call those functions as follows:
 *     TimerACompareInit(TIMERA_UP,1,
 *                       0x1000,TIMERA_TR);
 * or more complecated one:
 *     TimerACaptureInit(TIMER_UP_DOWN,3,
 *                       0x3000,TIMERA_RISING, //<- ch0 setting
 *                       0x2000,TIMER_FALLING, //<- ch1 setting
 *                       0x1000,TIMER_BOTH);   //<- ch2 setting
 * I try to maintain as many interfaces as it ought to be.
 * it should be robust enough. try and return feedback to me.
 * if default does not match your requirement slightly, use modifiers above.
 */

#include "TimerA.c"

#endif

//                            Ver. 0.4, all tested
//                            Hu Zhongsheng implemented, last update in 2013/9/6
